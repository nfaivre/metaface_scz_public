# I. Format dataframes
## 1) Raw data

# s2_by_indiv_FAvsCREJ = s2 %>% dplyr::filter(SDT %in% c('fa','crej')) %>% 
#   group_by(task, group, suj) %>% 
#   # summarise(m = mean(ac, na.rm = T), sigLevel = 0) 
#   summarise(m = (1- mean(ac, na.rm = T))*100, sigLevel = 0) 
# 
# binom_data0 = s2_by_indiv_FAvsCREJ %>% group_by(task, group) %>% summarise(avg = mean(m), sd = sd(m)) %>% 
#   mutate(n = ifelse(group == "c",spl[1],spl[2]),
#          se = sd/sqrt(n),
#          sig = -2.5,
#          type = ifelse(task == "Perception", "d",
#                        ifelse(task == "Familiarity", "f", "r"))) %>% 
#   ungroup() %>% select(-task)

binom_data0 = data0 %>% group_by(type, group, suj) %>% summarise(m = mean(fa,na.rm = T)*100, 
                                                                sd = sd(fa,na.rm = T)) %>% 
  group_by(type, group) %>% 
  summarise(avg = mean(m), sd = sd(m)) %>% 
  mutate(n = ifelse(group == "c",spl[1],spl[2]),
         se = sd/sqrt(n))


binom_data = data %>% group_by(type, sig, group, suj) %>% summarise(m = mean(ac,na.rm = T)*100, 
                                                                    sd = sd(ac,na.rm = T)) %>% 
  group_by(sig, type, group) %>% 
  summarise(avg = mean(m), sd = sd(m)) %>% 
  mutate(n = ifelse(group == "c",spl[1],spl[2]),
         se = sd/sqrt(n))

group.labs = c("Control", "Schizophrenia")
names(group.labs) <- c("c","p")

## 2) Model fits
### Model 1a (stim. strength >0)
pred.data = data %>% group_by(type, sig, group, suj) %>% summarise(m = mean(predb,na.rm = T)*100) %>% 
  group_by(sig, type, group) %>% 
  summarise(avg = mean(m))

model.fit = plot_model(bmod2_binom, type = "pred", terms = c("sig","type","group"), ci.lvl = 0.95) 
fit.data = as_tibble(model.fit$data) 
names(fit.data) <- c("sig", "dummy", "conf.low", "conf.high", "type", "group", "type_col")
fit.data$delta = (fit.data$conf.high - fit.data$conf.low)*100/2

binom_fit = full_join(pred.data, fit.data, by = c("sig", "type", "group")) %>% select(-conf.low, - conf.high, -type_col, - dummy) %>% rename(avg_fit = avg)
binom_1a = full_join(binom_data, binom_fit, by = c("sig", "type", "group"))

### Model 1b (stim. strength =0)
pred.data0 = data0 %>% group_by(type, group, suj) %>% summarise(m = 100*mean(predb,na.rm = T)) %>% 
  group_by(type, group) %>% 
  summarise(avg = mean(m))
model1b.fit = plot_model(bmod2_fa, type = "pred", terms = c("group", "type"), ci.lvl = 0.95) 
fit1b.data = as_tibble(model1b.fit$data) 
names(fit1b.data) <- c("group", "dummy", "conf.low", "conf.high", "type", "type_col")
fit1b.data <- fit1b.data %>% mutate(group = ifelse(group == 1, "c", "p"),
                                    delta = (conf.high - conf.low)*100/2)
binom_fit1b = full_join(pred.data0, fit1b.data, by = c("type", "group")) %>% select(-conf.low, - conf.high, -type_col, - dummy, avg) %>% rename(avg_fit = avg)
binom_1b = full_join(binom_data0, binom_fit1b, by = c("type", "group"))

# II. PLOTS
## 1) Plot_model + data
type1_A = binom_1a %>%
  ggplot()+
  geom_point(aes(x = sig, y = avg, group = type, col = type), 
             inherit.aes = F,
             position = position_dodge(width = .2))+
  facet_grid(.~group, labeller = labeller(group = group.labs))+
  geom_errorbar(aes(x = sig, y = avg, 
                    ymin = avg - se, ymax = avg + se,
                    group = type,
                    col = type), width=.1,
                position = position_dodge(width = .2),
                inherit.aes = F)+
  geom_line(aes(x = sig, y = avg_fit, col = type, group = type), data = binom_1a, position = position_dodge(width = .2)) +
  geom_ribbon(aes(x=sig, ymin = avg_fit - delta, ymax = avg_fit + delta,  fill = type), alpha = 0.4, data = binom_1a[binom_1a$type == "f",], position = position_dodge(width = .2))+
  geom_ribbon(aes(x=sig, ymin = avg_fit - delta, ymax = avg_fit + delta,  fill = type), alpha = 0.2, data = binom_1a[binom_1a$type != "f",], position = position_dodge(width = .2))+
  coord_cartesian(ylim=c(40,100))+
  scale_x_continuous(breaks = c(-1.5,-0.5,0.5,1.5),
                     labels = c(1,2,3,4))+
  scale_color_manual(name = "", labels = c("Visual detection", "Familiarity", "Recollection"),
                     values = c("darkorchid","gold","darkorange")) + #c("#E69F00","#56B4E9","#2C7FB8")) / c("#00BFC4","#E69F00","#F8766D"))
  scale_fill_manual(name = "", labels = c("Visual detection", "Familiarity", "Recollection"),
                    values = c("darkorchid","gold","darkorange")) + 
  labs(x = "Stimulus Strength", 
       y = "Hit rates (%)", 
       title = "A.")+
  theme(legend.position = "bottom",
        text = element_text(size = 16))

(type1_B = binom_1b %>%
  ggplot()+
  geom_point(aes(x = group, y = avg, group = type, col = type),
             inherit.aes = F,
             position = position_dodge(width = .2))+
  geom_errorbar(aes(x = group, y = avg,
                    ymin = avg - se, ymax = avg + se,
                    group = type,
                    col = type), width=.1,
                position = position_dodge(width = .2),
                inherit.aes = F)+
  geom_line(aes(x = group, y = avg, col = type, group = type), position = position_dodge(width = .2)) +
  # geom_point(aes(x = group, y = avg_fit, group = type, col = type),
  #              inherit.aes = F,
  #              position = position_dodge(width = .1),
  #            shape = 1,
  #            size = 3)+
  # geom_ribbon(aes(x= group, ymin = avg_fit - delta, ymax = avg_fit + delta,  group = type, fill = type), alpha = 0.4, position = position_dodge(width = .2))+
  coord_cartesian(ylim=c(0,60))+
  scale_x_discrete(breaks = c("c","p"),
                   labels = c("Control", "Scz"))+
  scale_color_manual(name = "", labels = c("Visual detection", "Familiarity", "Recollection"),
                     values = c("darkorchid","gold","darkorange")) + #c("#E69F00","#56B4E9","#2C7FB8")) / c("#00BFC4","#E69F00","#F8766D"))
  # scale_fill_manual(name = "", labels = c("Visual detection", "Familiarity", "Recollection"),
  #                     values = c("darkorchid","gold","darkorange")) + 
  labs(x = "Group", 
       # y = "Accuracy", 
       y = "False alarm rates (%)", 
       title = "B.")+
  theme(legend.position = "bottom",
        text = element_text(size = 16))
)

type1_plot = ggpubr::ggarrange(type1_A, type1_B,
                  ncol = 2, nrow = 1, 
                  common.legend = TRUE, legend = "bottom", 
                  widths = c(3,1))
ggsave(type1_plot, filename = here('..','figures', 'type1_plot.pdf'), device = "pdf", width = 8, height = 4)

###################################################################
# Plot FA  
  
  type1_fa = ggplot(s2fa, aes(x = type, y = avg, col = group, group = group))+
    geom_point(position = position_dodge(width = 0.1))+
    geom_line(position = position_dodge(width = 0.1))+
    geom_errorbar(aes(x = type, y = avg, 
                      ymin = avg - se, ymax = avg + se,
                      group = group,
                      col = group), 
                  width=.1,
                  position = position_dodge(width = 0.1))+
    scale_color_manual(name = "",
                       labels = c("Control", "Schizophrenia"),
                       values = c(brewer.pal(n=3,name="Set2")[1],brewer.pal(n=3,name="Set2")[2]))+
    scale_y_continuous(breaks = seq(0.4,1,0.1),
                       labels = seq(40,100,10))+
    labs(x = "",y = "% Correct \n(Stimulus Strength = 0)")+
    theme(legend.position = "bottom",
          text = element_text(size = 16))+
    coord_cartesian(ylim = c(0.4,1))
  
ggsave(type1_fa, file = here::here('..','figures','type1_fa.pdf'), device = 'pdf', width = 5, height = 4)

