# CONFIDENCE HISTORY

library(zoo)
a = data %>% group_by(suj) %>% mutate(n = n(),
                                  confh = rollmean(x = confidence, k = 5, fill = NA, align = 'right'),
                                  confh = c(NA,confh[1:(n-1)]))
a = a %>% mutate(zconf = scale(confidence),
                 zconfh = scale(confh),
                 zrt = scale(rt))

a$ac = factor(a$ac, levels = c("1", "0"))
priors = c(prior(prior = normal(-.1,1), class = "b", coef = "zrt"),
           prior(prior = normal(0.2,1), class = "b", coef = "zconfh"),
           prior(prior = normal(.1,1), class = "b", coef = "groupp"),
           prior(prior = normal(0,1), class = "b", coef = "groupp:zrt"),
           prior(prior = normal(0,1), class = "b", coef = "groupp:zconfh"))

warmup = 1500
iter = 6000
chains = 4

m6 = brm(zconf ~ group * type * (zrt + zconfh) + (type + zrt + zconfh| suj), 
         data =a[a$ac == 1,], sample_prior= T, save_all_pars = T,
         prior = priors,
         chains=chains, 
         iter=iter, 
         cores=chains, 
         warmup=warmup,
         file=here::here('..','data','models','lm_zconf-zconfh2')) # 
summary(m6)

plot_model(m6, type = "pred", terms = c("zrt", "group", "type"))
plot_model(m6, type = "pred", terms = c("zconfh", "group", "type")) 

h2 <- c("RT" = "zrt < 0",
        "RT * group" = "groupp:zrt > 0", 
        "RT * group * typef" = "groupp:typef:zrt > 0", 
        "RT * group * typer" = "groupp:typer:zrt > 0", 
        "history" = "zconfh = 0",
        "history * group" = "groupp:zconfh = 0",
        "history * group * typef" = "groupp:typef:zconfh = 0",
        "history * group * typer" = "groupp:typer:zconfh = 0"
)

(hyp=hypothesis(m6, h2))

## Plot confidence history
a$confh_round=round(a$zconfh)
roundconfh = a %>%  
  group_by(suj,group,confh_round,type) %>% summarise(conf=mean(zconf),length=n()) %>% na.omit() %>%
  group_by(group,confh_round,type)     %>% summarise_at(vars(conf,length),lst(mean,sd)) %>% 
  select(group,
         x = confh_round, 
         predicted = conf_mean,
         facet = type,
         conf_sd,
         length_mean,
         length_sd)

plot_confh = plot_model(m6,type='pred', terms=c('zconfh','group','type'))
tmp = as_tibble(plot_confh$data)

type.labs <- c("Perception", "Familiarity", "Recollection")
names(type.labs) <- c("d", "f", "r")

plot_confh = tmp %>% 
  ggplot(.,aes(x,predicted,ymin=conf.low,ymax=conf.high,fill=group,color=group)) + 
  geom_ribbon(color=NA,alpha=.2) +  
  geom_line() +
  geom_pointrange(data=roundconfh,inherit.aes = F, 
                  aes(x=x, y=predicted, 
                      ymin= predicted - conf_sd, 
                      ymax= predicted + conf_sd, 
                      size=length_mean, color=group,fill=group),
                  position=position_dodge(width=.5),shape=19,alpha=.3) + 
  scale_size_continuous(range=c(.1,1),name='# trials')+
  scale_color_manual(values = c(brewer.pal(n=3,name="Set2")[1],brewer.pal(n=3,name="Set2")[2]),
                     labels = c ("Control", "Patient"))+
  scale_fill_manual(values = c(brewer.pal(n=3,name="Set2")[1],brewer.pal(n=3,name="Set2")[2]),
                    labels = c ("Control", "Patient"))+
  coord_cartesian(ylim=c(-0.6,0.9))+
  theme(legend.position ='bottom') +
  facet_grid(.~facet, labeller = labeller(facet = type.labs))+
  labs(x= 'Standardized Confidence History', 
       y= 'Standardized Confidence', 
       color='Group:', fill ='Group:') + 
  scale_x_continuous(breaks=c(-2,0,2))

ggsave(plot_confh, file = here::here('..','figures','conf_confh.jpeg'), device = "jpeg", width = 6, height = 4)

## Plot response times
a$rt_round=round(a$zrt)
roundrt = a %>%  
  group_by(suj,group,rt_round,type) %>% summarise(conf=mean(zconf),length=n()) %>% na.omit() %>%
  group_by(group,rt_round,type)     %>% summarise_at(vars(conf,length),lst(mean,sd)) %>% 
  select(group,
         x = rt_round, 
         predicted = conf_mean,
         facet = type,
         conf_sd,
         length_mean,
         length_sd)

plot_rt = plot_model(m6,type='pred', terms=c('zrt','group','type'))
tmp = as_tibble(plot_rt$data)

type.labs <- c("Perception", "Familiarity", "Recollection")
names(type.labs) <- c("d", "f", "r")

plot_rt = tmp %>%
  ggplot(.,aes(x,predicted,ymin=conf.low,ymax=conf.high,fill=group,color=group)) + 
  geom_ribbon(color=NA,alpha=.2) +  
  geom_line() +
  geom_pointrange(data=roundrt,inherit.aes = F, 
                  aes(x=x, y=predicted, 
                      ymin= predicted - conf_sd, #conf_ci$CI_low,
                      ymax= predicted + conf_sd, #conf_ci$CI_high,
                      size=length_mean, color=group,fill=group),
                  position=position_dodge(width=.5),shape=19,alpha=.3) + 
  scale_size_continuous(range=c(.1,1),name='# trials')+
  scale_color_manual(values = c(brewer.pal(n=3,name="Set2")[1],brewer.pal(n=3,name="Set2")[2]),
                     labels = c ("Control", "Patient"))+
  scale_fill_manual(values = c(brewer.pal(n=3,name="Set2")[1],brewer.pal(n=3,name="Set2")[2]),
                    labels = c ("Control", "Patient"))+
  theme(legend.position ='bottom',
        text = element_text(size = 16)) +
  facet_grid(.~facet, labeller = labeller(facet = type.labs))+
  labs(x= 'Standardized response times', 
       y= 'Standardized Confidence', 
       color='Group:', fill ='Group:') + 
  scale_x_continuous(breaks=c(-2,0,2))

ggsave(plot_rt, file = here::here('..','figures','conf_rt.jpeg'), device = "jpeg", width = 6, height = 4)
