% re-analysis of Faivre et al. 2022
%
% 22-FEB-2022 - pascal mamassian
% 07-MAR-2022 - pm: added bootstrap within subject

clear all;
close all;

% -> ****************************
% stim_nb_bins = 2;
stim_nb_bins = 4;

conf_range = [0.0, 1.0];
% conf_resol = 2;
conf_resol = 4;
% conf_resol = 8;

% -> ****************************
% -> first visual stim in 'Exp1.res' = line 401
% -> first visual stim in 'Data_Faivre' = line 374

filename = 'data_metaface2.csv';

fileID = fopen(filename);
% C = textscan(fileID,'%s %s %f32 %d8 %u %f %f %s %f');
% data_cell = textscan(fileID, ...
%     '%d %s %s %s %d %d %f %d %s %s %d %s %f %f');
data_table = readtable(filename);
data_cell = table2cell(data_table);
data_struct = table2struct(data_table);

fclose(fileID);

% nb_lines = size(data_cell{1}, 1);
nb_lines = size(data_cell, 1);
col_line =  1;   % Line number
col_subj =  2;   % Subject id
col_grop =  3;   % group patient/control
col_tach =  4;   % tache familiarity/perception/recollection
col_tria =  5;   % trial number (1..100)
col_sigl =  6;   % sigma level [0,1,2,3,4]
col_samp =  7;   % stimulus amplitude
col_bloc =  8;   % block (6.14)
col_resp =  9;   % response (y/n/none)
col_expe = 10;   % expected response (y/n)
col_accu = 11;   % accuracy (0/1)
col_sdtc = 12;   % SDT category (hit/miss/fa/crej)
col_rtse = 13;   % reaction time (sec)
col_conf = 14;   % confidence continue (0..1)

line_num = 1;
% if (data_cell{col_subj}{line_num} == 'subjAB')
%     fprintf('OK\n');
% end
% if (data_cell{col_tria}(line_num) == 1)
%     fprintf('OK\n');
% end
if (data_cell{line_num, col_subj} == 'c3_v')
    fprintf('OK\n');
end
if (data_cell{line_num, col_tria} == 1)
    fprintf('OK\n');
end

% nb_groups = 1;
nb_groups = 2;

% do_bootstrap = 0;
do_bootstrap = 1;

if (do_bootstrap)
    nb_bootstrap = 100;
%     nb_bootstrap = 5;
%     nb_bootstrap = 10;
%     nb_bootstrap = 20;
%     nb_bootstrap = 50;
    nb_groups = nb_bootstrap;
end

% -> analyzed data per subject
subj_nb = 15;
% subj_nb = 3;
dprime_lst = NaN(3, subj_nb, nb_groups);
mratio_lst = NaN(3, subj_nb, nb_groups);
eff_lst    = NaN(3, subj_nb, nb_groups);
noise2_lst = NaN(3, subj_nb, nb_groups);
boost2_lst = NaN(3, subj_nb, nb_groups);

% mtlb_colors = [...
%     0.0000, 0.4470, 0.7410;
%     0.8500, 0.3250, 0.0980;
%     0.9290, 0.6940, 0.1250;
%     0.4940, 0.1840, 0.5560;
%     0.4660, 0.6740, 0.1880;
%     0.3010, 0.7450, 0.9330;
%     0.6350, 0.0780, 0.1840];
mtlb_colors = [...
    0.8500, 0.3250, 0.0980;
    0.4660, 0.6740, 0.1880;
    0.0000, 0.4470, 0.7410;
    0.9290, 0.6940, 0.1250;
    0.4940, 0.1840, 0.5560;
    0.3010, 0.7450, 0.9330;
    0.6350, 0.0780, 0.1840];


% -> select 'perception' task
task1 = 'Perception';
task_inds = (string({data_cell{:, col_tach}}') == task1);


return;
% ->  old stuff
% ************************************************************************


for modality = 1:3
    
    switch modality
        case 1
            % -> audition
            modal_inds = (data_cell{col_naud}(:) == 1);
            side_vals = double(data_cell{col_asid}(modal_inds) * 2 - 3);
        case 2
            % -> tactile
            modal_inds = (data_cell{col_ntac}(:) == 1);
            side_vals = double(randi(2, [sum(modal_inds), 1]) * 2 - 3);
        case 3
            % -> vision
            modal_inds = (data_cell{col_nvis}(:) == 1);
            side_vals = double(data_cell{col_vsid}(modal_inds) * 2 - 3);
    end
      
modal_nb_trials = sum(modal_inds);

% -> [subj#, stim, resp, conf]
modal_data = NaN(modal_nb_trials, 4);
cc_subj = 1;
cc_stim = 2;
cc_resp = 3;
cc_conf = 4;

% -> subject
subj_vals = data_cell{col_subj}(modal_inds);
[subj_lst, ~, subj_ic] = unique(subj_vals);
modal_data(:,cc_subj) = subj_ic;
% subj_nb = length(subj_lst);

% -> stimulus (side is probably wrong)
modal_data(:,cc_stim) = data_cell{col_vval}(modal_inds);
modal_data(:,cc_stim) = modal_data(:,cc_stim) .* side_vals;

% -> response
corr_vals = double(data_cell{col_t1ac}(modal_inds));
corr_side = side_vals .* (corr_vals * 2 - 1);
modal_data(:,cc_resp) = (corr_side + 1) / 2;

% -> confidence
modal_data(:,cc_conf) = data_cell{col_t2cf}(modal_inds);
    
my_col = mtlb_colors(modality, :);

for ss = 1:subj_nb
    figure(1);
    subplot(3, 5, ss);
    hold on;
    
    ss_inds = (modal_data(:,cc_subj) == ss);
    ss_data_all = modal_data(ss_inds, [cc_stim, cc_resp, cc_conf]);
    
    nb_trials_all = size(ss_data_all, 1);
    if (do_bootstrap)
        % -> resample with replacement
        inds_all = randi(nb_trials_all, nb_bootstrap, nb_trials_all);
    else
        % -> divide into 'nb_groups' groups of trials, randomly chosen
%         inds_all = randperm(nb_trials_all);
        inds_lim = round(linspace(1, nb_trials_all+1, nb_groups+1));
        inds_bnds = NaN(2, nb_groups);  % min and max indices in each group
        inds_bnds(1, :) = inds_lim(1:(end-1));
        inds_bnds(2, :) = inds_lim(2:end) - 1;
    end
    
for gr = 1:nb_groups
    
    if (do_bootstrap)
        ss_data = ss_data_all(inds_all(gr,:), :);
        fprintf('modality #%d, subject #%d, bootstrap #%d\n', modality, ss, gr);
    else
        ss_data = ss_data_all((inds_bnds(1,gr):inds_bnds(2,gr)), :);
    end
    
%     ss_corr = corr_vals(ss_inds);

% [stim_lst, ~, stim_ic] = unique(vis_data(:, [cc_subj, cc_stim]), 'rows');
    [stim_lst, ~, stim_ic] = unique(ss_data(:, 1), 'rows');
    stim_nb = length(stim_lst);
    
    resp_lst = NaN(1, stim_nb);

    for st = 1:stim_nb
        my_inds = (stim_ic == st);
        stim_val = stim_lst(st);
        st_nb = sum(my_inds);
%         resp_val = sum(ss_corr(my_inds));
        resp_val = sum(ss_data(my_inds, 2));
        resp_lst(st) = resp_val / st_nb;
    end
    if (gr == 1)
        plot(stim_lst, resp_lst, 'o-',  'Color', my_col);
    elseif (gr == 2)
        % -> plot only first 2 in case we are bootstrapping
        plot(stim_lst, resp_lst, '+-',  'Color', my_col);
    end
    
    % -> d-prime
    p_hit = sum((ss_data(:, 1) > 0.0) & (ss_data(:, 2) == 1)) / sum(ss_data(:, 1) > 0.0);
    p_fa  = sum((ss_data(:, 1) < 0.0) & (ss_data(:, 2) == 1)) / sum(ss_data(:, 1) < 0.0);
    dprime_lst(modality, ss, gr) = norminv(p_hit) - norminv(p_fa);

    % -> binarize stimuli
    ss_data_2 = ss_data;
    ss_data_2(:, 1) = (ss_data(:, 1) > 0.0) * 2 - 1;
    crs_data_SRC_2 = crs_group_02(ss_data_2, 'is_continuous', true, ...
        'confidence_range', conf_range, 'confidence_resolution', conf_resol);

    % -> discretize stimuli
    stim_vals = NaN(1, stim_nb_bins);
    ss_data_8 = ss_data;

    qq = quantile(ss_data(:, 1), stim_nb_bins - 1);
    [~, ~, stm_tsk_bin] = histcounts(ss_data(:, 1), [-inf, qq, inf]);
    for nn = 1:stim_nb_bins
        inds5 = (stm_tsk_bin == nn);
        stim_vals(nn) = mean(ss_data(inds5, 1));
        ss_data_8(inds5, 1) = stim_vals(nn);
    end

%     [stim_inds, stim_edges] = discretize(ss_data(:, 1), nb_bins);
%     stim_vals = (stim_edges(2:end) + stim_edges(1:(end-1)))/2;
%     ss_data_8(:, 1) = stim_vals(stim_inds);

    % -> meta-d-prime
    [nR_S1, nR_S2] = CRStoML12(crs_data_SRC_2);
    myfit = fit_meta_d_MLE(nR_S1, nR_S2);
    mratio_lst(modality, ss, gr) = myfit.M_ratio;
    
    % -> confidence efficiency
%     crs_data_SRC = crs_group_02(ss_data_2, 'is_continuous', true, ...
%         'confidence_range', conf_range, 'confidence_resolution', conf_resol);
    crs_data_SRC = crs_group_02(ss_data_8, 'is_continuous', true, ...
        'confidence_range', conf_range, 'confidence_resolution', conf_resol);
    crs_struct = crs_fit_02(crs_data_SRC, ...
        'is_continuous', true, 'confidence_range', conf_range, ...
        'confidence_resolution', conf_resol);
    eff_lst(modality, ss, gr) = crs_struct.efficiency;
    noise2_lst(modality, ss, gr) = crs_struct.conf_noise;
    boost2_lst(modality, ss, gr) = crs_struct.conf_boost;

    
end

    
nb_bins = 4;
figure(4);
set(gcf,'Position',[200 200 950 650]);
% for ss = 1:subj_nb
%     for modality = 1:3

    my_col = mtlb_colors(modality, :);

    subplot(3, 5, ss);
    hold on;
    hist_modal = mratio_lst(modality, ss, :);
    histogram(hist_modal, nb_bins, 'Normalization', 'pdf', ...
        'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
    set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
    if (ss == 1)
        xlabel('m-ratio');
        ylabel('Probability Density');
    end
    xlim([-0.5, 2.0]);
    ylim([0, 5]);
%     end
% end

figure(5);
set(gcf,'Position',[100 100 950 650]);
% for ss = 1:subj_nb
%     for modality = 1:3

    my_col = mtlb_colors(modality, :);

    subplot(3, 5, ss);
    hold on;
    hist_modal = squeeze(eff_lst(modality, ss, :));
    histogram(hist_modal, nb_bins, 'Normalization', 'pdf', ...
        'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
    set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
    if (ss == 1)
        xlabel('conf. efficiency');
        ylabel('Probability Density');
    end
    xlim([-0.1, 1.1]);
    ylim([0, 20]);
%     end
% end



end


figure(2);
subplot(1, 2, 1);
hold on;
xvals = squeeze(mratio_lst(modality, :, 1));
yvals = squeeze(mratio_lst(modality, :, 2));
xmin = min(min(xvals), min(yvals));
xmax = max(max(xvals), max(yvals));
axis([xmin, xmax, xmin, xmax]);
line([xmin, xmax], [xmin, xmax], 'Color', [0.5, 0.5, 0.5]);
plot(xvals, yvals, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
xlabel('m-ratio (set 1)');
ylabel('m-ratio (set 2)');
axis('square');


subplot(1, 2, 2);
hold on;
xvals = squeeze(eff_lst(modality, :, 1));
yvals = squeeze(eff_lst(modality, :, 2));
xmin = min(min(xvals), min(yvals));
xmax = max(max(xvals), max(yvals));
axis([xmin, xmax, xmin, xmax]);
line([xmin, xmax], [xmin, xmax], 'Color', [0.5, 0.5, 0.5]);
plot(xvals, yvals, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
xlabel('conf. efficiency (set 1)');
ylabel('conf. efficiency (set 2)');
axis('square');



% -> *******************************
% -> histogram
nb_bins = 10;
figure(3);
subplot(1,2,1);
hold on;
% line([myparams(1), myparams(1)], [0 60], 'LineStyle', '-', ...
%     'Color', [0.466, 0.674, 0.188], 'LineWidth', 3);
hist_modal = mratio_lst(modality, :, :);
histogram(hist_modal, nb_bins, 'Normalization', 'pdf', ...
    'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
% xlim([0.88, 1.12]);
% ylim([0 60]);
% yt2 = 0:20:60;
% set(gca, 'YTick', yt2, 'YTickLabel', yt2);
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
ylabel('Probability Density');
title('m-ratio');

subplot(1,2,2);
hold on;
hist_modal = eff_lst(modality, :, :);
histogram(hist_modal, nb_bins, 'Normalization', 'pdf', ...
    'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
ylabel('Probability Density');
title('conf. efficiency');



% -> *******************************
figure;
hold on;
xvals = mratio_lst(modality, :, :);
yvals = eff_lst(modality, :, :);
plot(xvals(:), yvals(:), ...
    'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
xlabel('m-ratio');
ylabel('conf. efficiency');
axis('equal');

end



% -> *******************************
% -> statistical test on the first 2 simulations

% xvals = squeeze(mratio_lst(:, :, 1));
% yvals = squeeze(mratio_lst(:, :, 2));
% [h,p,ci,stats] = ttest(xvals(:), yvals(:))
% [rr, pp] = corrcoef(xvals(:), yvals(:));
% [rr(1,2)^2, pp(1,2)]
% 
% 
% xvals = squeeze(eff_lst(:, :, 1));
% yvals = squeeze(eff_lst(:, :, 2));
% [h,p,ci,stats] = ttest(xvals(:), yvals(:))
% [rr, pp] = corrcoef(xvals(:), yvals(:));
% [rr(1,2)^2, pp(1,2)]


% -> *******************************

my_col = [0.2, 0.2, 0.2];

% -> visual vs. audio
figure;
% xvals = mratio_lst(1, :);
% yvals = mratio_lst(3, :);
xvals = median(squeeze(mratio_lst(1,:,:)), 2)';
yvals = median(squeeze(mratio_lst(3,:,:)), 2)';
plot(xvals, yvals, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
xlabel('audio m-ratio');
ylabel('visual m-ratio');
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
axis([0, 1.2, 0, 1.2]);
axis('square');

        
[rr, pp] = corrcoef(xvals, yvals);
fprintf('corr V-A (m-ratio): r^2 = %6.3f, p = %6.3f\n', rr(1,2)^2, pp(1,2));

figure;
% xvals = eff_lst(1, :);
% yvals = eff_lst(3, :);
xvals = median(squeeze(eff_lst(1,:,:)), 2)';
yvals = median(squeeze(eff_lst(3,:,:)), 2)';
plot(xvals, yvals, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
xlabel('audio conf. efficiency');
ylabel('visual conf. efficiency');
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
axis([0, 0.8, 0, 0.8]);
axis('square');
        
[rr, pp] = corrcoef(xvals, yvals);
fprintf('corr V-A (conf-ef): r^2 = %6.3f, p = %6.3f\n', rr(1,2)^2, pp(1,2));


% -> visual vs. tactile
figure;
% xvals = mratio_lst(2, :);
% yvals = mratio_lst(3, :);
xvals = median(squeeze(mratio_lst(2,:,:)), 2)';
yvals = median(squeeze(mratio_lst(3,:,:)), 2)';
plot(xvals, yvals, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
xlabel('tactile m-ratio');
ylabel('visual m-ratio');
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
axis([0, 1.2, 0, 1.2]);
axis('square');
        
[rr, pp] = corrcoef(xvals, yvals);
fprintf('corr V-T (m-ratio): r^2 = %6.3f, p = %6.3f\n', rr(1,2)^2, pp(1,2));

figure;
% xvals = eff_lst(2, :);
% yvals = eff_lst(3, :);
xvals = median(squeeze(eff_lst(2,:,:)), 2)';
yvals = median(squeeze(eff_lst(3,:,:)), 2)';
plot(xvals, yvals, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
xlabel('tactile conf. efficiency');
ylabel('visual conf. efficiency');
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
axis([0, 0.8, 0, 0.8]);
axis('square');
        
[rr, pp] = corrcoef(xvals, yvals);
fprintf('corr V-T (conf-ef): r^2 = %6.3f, p = %6.3f\n', rr(1,2)^2, pp(1,2));


% -> tactile vs. audio
figure;
% xvals = mratio_lst(1, :);
% yvals = mratio_lst(2, :);
xvals = median(squeeze(mratio_lst(1,:,:)), 2)';
yvals = median(squeeze(mratio_lst(2,:,:)), 2)';
plot(xvals, yvals, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
xlabel('audio m-ratio');
ylabel('tactile m-ratio');
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
axis([0, 1.2, 0, 1.2]);
axis('square');
        
[rr, pp] = corrcoef(xvals, yvals);
fprintf('corr T-A (m-ratio): r^2 = %6.3f, p = %6.3f\n', rr(1,2)^2, pp(1,2));

figure;
% xvals = eff_lst(1, :);
% yvals = eff_lst(2, :);
xvals = median(squeeze(eff_lst(1,:,:)), 2)';
yvals = median(squeeze(eff_lst(2,:,:)), 2)';
plot(xvals, yvals, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
xlabel('audio conf. efficiency');
ylabel('tactile conf. efficiency');
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
axis([0, 0.8, 0, 0.8]);
axis('square');
        
[rr, pp] = corrcoef(xvals, yvals);
fprintf('corr T-A (conf-ef): r^2 = %6.3f, p = %6.3f\n', rr(1,2)^2, pp(1,2));

% -> *******************************
% -> bootstrap analyses of the medians across subjects

nb_bins = 6;
figure;
set(gcf,'Position',[100 100 800 350]);
subplot(1, 2, 1);   % m-ratio
for modality = 1:3
    my_col = mtlb_colors(modality, :);

    hold on;
    hist_modal = median(squeeze(mratio_lst(modality,:,:)), 1);
    histogram(hist_modal, nb_bins, 'Normalization', 'pdf', ...
        'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
end
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
xlabel('m-ratio');
ylabel('Probability Density');
xlim([-0.05, 1.05]);
ylim([0, 22]);

subplot(1, 2, 2);   % conf. efficiency
for modality = 1:3
    my_col = mtlb_colors(modality, :);

    hold on;
    hist_modal = median(squeeze(eff_lst(modality,:,:)), 1);
    histogram(hist_modal, nb_bins, 'Normalization', 'pdf', ...
        'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
end
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
xlabel('conf. efficiency');
ylabel('Probability Density');
xlim([-0.05, 1.05]);
ylim([0, 22]);

% -> *******************************
% -> bootstrap analyses of the correlations across modalities
corr_mratio_mat = NaN(3, nb_bootstrap);
corr_confef_mat = NaN(3, nb_bootstrap);
my_col = [0.2, 0.2, 0.2];
quant = 0.95;
quant_val = (1-quant)/2;

% -> visual vs. audio
figure;
set(gcf,'Position',[100 100 800 350]);
subplot(1, 2, 1);   % m-ratio
hold on;
for nn = 1:nb_bootstrap
    xvals = mratio_lst(1,:,nn);
    yvals = mratio_lst(3,:,nn);
    [rr, pp] = corrcoef(xvals, yvals);
    corr_mratio_mat(1, nn) = rr(1,2)^2;
end
histogram(corr_mratio_mat(1, :), nb_bins, 'Normalization', 'pdf', ...
    'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
title('m-ratio');
xlabel('V-A corr.');
ylabel('Probability Density');
xlim([-0.05, 1.05]);
ylim([-2, 15]);
quant_min = quantile(corr_mratio_mat(1, :), quant_val);
quant_mid = quantile(corr_mratio_mat(1, :), 0.5);
quant_max = quantile(corr_mratio_mat(1, :), 1-quant_val);
plot([quant_min, quant_max], [-1, -1], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, -1, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);

subplot(1, 2, 2);   % conf. efficiency
hold on;
for nn = 1:nb_bootstrap
    xvals = eff_lst(1,:,nn);
    yvals = eff_lst(3,:,nn);
    [rr, pp] = corrcoef(xvals, yvals);
    corr_confef_mat(1, nn) = rr(1,2)^2;
end
histogram(corr_confef_mat(1, :), nb_bins, 'Normalization', 'pdf', ...
    'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
title('conf. efficiency');
xlabel('V-A corr.');
ylabel('Probability Density');
xlim([-0.05, 1.05]);
ylim([-2, 15]);
quant_min = quantile(corr_confef_mat(1, :), quant_val);
quant_mid = quantile(corr_confef_mat(1, :), 0.5);
quant_max = quantile(corr_confef_mat(1, :), 1-quant_val);
plot([quant_min, quant_max], [-1, -1], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, -1, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);


% -> visual vs. tactile
figure;
set(gcf,'Position',[100 100 800 350]);
subplot(1, 2, 1);   % m-ratio
hold on;
for nn = 1:nb_bootstrap
    xvals = mratio_lst(2,:,nn);
    yvals = mratio_lst(3,:,nn);
    [rr, pp] = corrcoef(xvals, yvals);
    corr_mratio_mat(2, nn) = rr(1,2)^2;
end
histogram(corr_mratio_mat(2, :), nb_bins, 'Normalization', 'pdf', ...
    'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
title('m-ratio');
xlabel('V-T corr.');
ylabel('Probability Density');
xlim([-0.05, 1.05]);
ylim([-2, 20]);
quant_min = quantile(corr_mratio_mat(2, :), quant_val);
quant_mid = quantile(corr_mratio_mat(2, :), 0.5);
quant_max = quantile(corr_mratio_mat(2, :), 1-quant_val);
plot([quant_min, quant_max], [-1, -1], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, -1, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);

subplot(1, 2, 2);   % conf. efficiency
hold on;
for nn = 1:nb_bootstrap
    xvals = eff_lst(2,:,nn);
    yvals = eff_lst(3,:,nn);
    [rr, pp] = corrcoef(xvals, yvals);
    corr_confef_mat(2, nn) = rr(1,2)^2;
end
histogram(corr_confef_mat(2, :), nb_bins, 'Normalization', 'pdf', ...
    'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
title('conf. efficiency');
xlabel('V-T corr.');
ylabel('Probability Density');
xlim([-0.05, 1.05]);
ylim([-2, 20]);
quant_min = quantile(corr_confef_mat(2, :), quant_val);
quant_mid = quantile(corr_confef_mat(2, :), 0.5);
quant_max = quantile(corr_confef_mat(2, :), 1-quant_val);
plot([quant_min, quant_max], [-1, -1], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, -1, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);


% -> tactile vs. audio
figure;
set(gcf,'Position',[100 100 800 350]);
xmin = -0.05;  xmax = 1.05;  ymin = -2;  ymax = 8;

subplot(1, 2, 1);   % m-ratio
hold on;
for nn = 1:nb_bootstrap
    xvals = mratio_lst(1,:,nn);
    yvals = mratio_lst(2,:,nn);
    [rr, pp] = corrcoef(xvals, yvals);
    corr_mratio_mat(3, nn) = rr(1,2)^2;
end
histogram(corr_mratio_mat(3, :), nb_bins, 'Normalization', 'pdf', ...
    'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
title('m-ratio');
xlabel('T-A corr.');
ylabel('Probability Density');
xlim([xmin, xmax]);  ylim([ymin, ymax]);
quant_min = quantile(corr_mratio_mat(3, :), quant_val);
quant_mid = quantile(corr_mratio_mat(3, :), 0.5);
quant_max = quantile(corr_mratio_mat(3, :), 1-quant_val);
plot([quant_min, quant_max], [-1, -1], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, -1, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);

subplot(1, 2, 2);   % conf. efficiency
hold on;
for nn = 1:nb_bootstrap
    xvals = eff_lst(1,:,nn);
    yvals = eff_lst(2,:,nn);
    [rr, pp] = corrcoef(xvals, yvals);
    corr_confef_mat(3, nn) = rr(1,2)^2;
end
histogram(corr_confef_mat(3, :), nb_bins, 'Normalization', 'pdf', ...
    'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
title('conf. efficiency');
xlabel('T-A corr.');
ylabel('Probability Density');
xlim([xmin, xmax]);  ylim([ymin, ymax]);
quant_min = quantile(corr_confef_mat(3, :), quant_val);
quant_mid = quantile(corr_confef_mat(3, :), 0.5);
quant_max = quantile(corr_confef_mat(3, :), 1-quant_val);
plot([quant_min, quant_max], [-1, -1], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, -1, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);

        
% -> ***************
        
alpha_val = 0.05;
t_crit = tinv(1 - alpha_val, subj_nb - 2);
% t = r * sqrt(n - 2) / sqrt(1 - r^2)
% pval = tcdf(t, (15 - 2))
% p_reported = (1 - pval)*2     % 2 sided
r_crit = t_crit^2 / (t_crit^2 + subj_nb - 2);   % 2 sided (around r=0)


alpha_col = 0.5;

xr_vals1 = [xmin, r_crit, r_crit, xmin, xmin];
yr_pred1 = [ymin, ymin, ymax, ymax, ymin];

fill(xr_vals1, yr_pred1, [0.5, 0.5, 0], 'FaceAlpha', alpha_col);
% plot(yr_pred1, xr_vals1, '-', 'Color', mtlb_colors(1, :), 'LineWidth', 3);

% save('faivre_220310.mat', 'mratio_lst', 'eff_lst')

% -> ***************
        
figure;
hold on;
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
line([0, 0], [-0.05, 0.15], 'LineStyle', '-', ...
    'Color', [0 0 0], 'LineWidth', 2);

ypos = 0.1;
quant_min = quantile(corr_mratio_mat(1, :), quant_val);
quant_mid = quantile(corr_mratio_mat(1, :), 0.5);
quant_max = quantile(corr_mratio_mat(1, :), 1-quant_val);
plot([quant_min, quant_max], [ypos, ypos], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, ypos, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
text(0.8, ypos, 'V-A', 'FontName', 'Arial', 'FontSize', 20);

ypos = 0.05;
quant_min = quantile(corr_mratio_mat(2, :), quant_val);
quant_mid = quantile(corr_mratio_mat(2, :), 0.5);
quant_max = quantile(corr_mratio_mat(2, :), 1-quant_val);
plot([quant_min, quant_max], [ypos, ypos], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, ypos, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
text(0.8, ypos, 'V-T', 'FontName', 'Arial', 'FontSize', 20);

ypos = 0.0;
quant_min = quantile(corr_mratio_mat(3, :), quant_val);
quant_mid = quantile(corr_mratio_mat(3, :), 0.5);
quant_max = quantile(corr_mratio_mat(3, :), 1-quant_val);
plot([quant_min, quant_max], [ypos, ypos], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, ypos, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
text(0.8, ypos, 'T-A', 'FontName', 'Arial', 'FontSize', 20);

title('m-ratio');
xlabel('correlation r^2');
xlim([-0.1, 1]);
ylim([-0.05, 0.15]);
yticks([]);

% -> ***************
        
figure;
hold on;
set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
line([0, 0], [-0.05, 0.15], 'LineStyle', '-', ...
    'Color', [0 0 0], 'LineWidth', 2);

ypos = 0.1;
quant_min = quantile(corr_confef_mat(1, :), quant_val);
quant_mid = quantile(corr_confef_mat(1, :), 0.5);
quant_max = quantile(corr_confef_mat(1, :), 1-quant_val);
plot([quant_min, quant_max], [ypos, ypos], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, ypos, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
text(0.8, ypos, 'V-A', 'FontName', 'Arial', 'FontSize', 20);

ypos = 0.05;
quant_min = quantile(corr_confef_mat(2, :), quant_val);
quant_mid = quantile(corr_confef_mat(2, :), 0.5);
quant_max = quantile(corr_confef_mat(2, :), 1-quant_val);
plot([quant_min, quant_max], [ypos, ypos], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, ypos, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
text(0.8, ypos, 'V-T', 'FontName', 'Arial', 'FontSize', 20);

ypos = 0.0;
quant_min = quantile(corr_confef_mat(3, :), quant_val);
quant_mid = quantile(corr_confef_mat(3, :), 0.5);
quant_max = quantile(corr_confef_mat(3, :), 1-quant_val);
plot([quant_min, quant_max], [ypos, ypos], ...
            'Color', [0 0 0], 'LineWidth', 3);
plot(quant_mid, ypos, 'o', 'MarkerSize', 16, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 3);
text(0.8, ypos, 'T-A', 'FontName', 'Arial', 'FontSize', 20);

title('confidence efficiency');
xlabel('correlation r^2');
xlim([-0.1, 1]);
ylim([-0.05, 0.15]);
yticks([]);

% -> THE END <-
