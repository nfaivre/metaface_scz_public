% fid = fopen('data_metaface2.csv')
% %d = textscan(fid,'%s %s %s %d %d %f %d %s %s %d %s %d %d','Delimiter',',');
% d = textscan(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s','Delimiter',',');
% fclose(fid)

% QUESTION:
% nb d'essais pas constant par sujet / tache

% TO DO:
% 
% caveats
% tache de detection: 
% - bruit non egal � travers les niveaux de stim
% - balance du design 80% de stimuli pr�sents (-> critere?)
% bcp de conditions avec aucune erreur (environ 30% du dataset en perception)


%%
clear all
close all

addpath(genpath('cfc_ratings\'))
% addpath('C:\Users\vdegardell\Dropbox\vincecode\type2sdt')
% addpath('C:\Users\vdegardell\Dropbox\vincecode\stats')

%%
% -> ****************************

% global parameters for the analysis
stim_nb_bins = 5; % number of bins for the stimulus axis
conf_range = [0.0, 1.0]; % range of confidence scale
conf_resol = 4; % number of confidence bins for M analysis
conf_resol_mratio = 4; % number of bins for mratio analysis

do_plots = true;
do_normalize = 0; % normalize individual data to performance
do_groupsubj = 0; % analyse all subjects as one, in each group
do_bootstrap = 0; % evaluate CI using resampling, in each group
% put these last 3 to 1 for combined subject with bootstrap 
% put 0 for standard analyses (each subject separately)
do_bootstrap_n = 10; % number of bootrasp samples

% load data and define columns
d = readtable('data_metaface_final.csv');
d = table2cell(d);
col_line =  1;   % Line number
col_subj =  2;   % Subject id
col_grop =  3;   % group patient/control
col_tach =  4;   % tache familiarity/perception/recollection
col_tria =  5;   % trial number (1..100)
col_sigl =  6;   % sigma level [0,1,2,3,4]
col_samp =  7;   % stimulus amplitude
col_bloc =  8;   % block (6.14)
col_resp =  9;   % response (y/n/none)
col_expe = 10;   % expected response (y/n)
col_accu = 11;   % accuracy (0/1)
col_sdtc = 12;   % SDT category (hit/miss/fa/crej)
col_rtse = 13;   % reaction time (sec)
col_conf = 14;   % confidence continue (0..1)

% reformat columns
for k = 1:14
    data_cell{k} = [d(:,k)];
end
data_cell_2{1} = cellfun(@(x) str2num(x),data_cell{1});
for k = [2:4 9 10 12]
    data_cell_2{k} = string(data_cell{k});
end
for k = [5:8 11]
    data_cell_2{k} = cell2mat(data_cell{k});
end
for k = [13 14]
    data_cell_2{k} = cell2mat(cellfun(@(x) str2num(strrep(num2str(x),'NA','NaN')),data_cell{k},'UniformOutput',false));
end
data_cell = data_cell_2;

% subject list
subj_vals = data_cell{col_subj}(:);
subj_list = unique(subj_vals);
subj_nb = length(subj_list);
% demographic variable: patients 
patients_list = nan(1,subj_nb);
for isuj=1:subj_nb
    if subj_list{isuj}(1)=='c'
        patients_list(isuj) = 0;
    else
        patients_list(isuj) = 1;
    end
end
% subject list: replace string with subject number
subj_valn = nan(size(subj_vals));
for isuj=1:subj_nb
    subj_valn(subj_vals==subj_list(isuj)) = isuj;
end
data_cell{col_subj} = subj_valn;

% bootstrap parameters
nb_bootstrap = 0; % by default no bootstrap
if do_bootstrap
    nb_bootstrap = do_bootstrap_n;
end

% output variables
% itask x subj x bootstrap
% e.g. for individual analysis: 3 x 29 x 1
% e.g. for groupsubj analysis: 3 x 2 x (1+n_bootsrap)
subj_nb_loop = subj_nb;
if do_groupsubj
    subj_nb_loop = 2;
end
dprime_lst = NaN(3, subj_nb_loop , 1+nb_bootstrap);
mratio_lst = NaN(3, subj_nb_loop , 1+nb_bootstrap);
eff_lst    = NaN(3, subj_nb_loop , 1+nb_bootstrap);
noise2_lst = NaN(3, subj_nb_loop , 1+nb_bootstrap);
boost2_lst = NaN(3, subj_nb_loop , 1+nb_bootstrap);

% colors for plots
mtlb_colors = [...
    0.8500, 0.3250, 0.0980;
    0.4660, 0.6740, 0.1880;
    0.0000, 0.4470, 0.7410;
    0.9290, 0.6940, 0.1250;
    0.4940, 0.1840, 0.5560;
    0.3010, 0.7450, 0.9330;
    0.6350, 0.0780, 0.1840];

%%
tasklist={'Perception','Familiarity','Recollection'};
for itask = 1:3    
    task = tasklist{itask};
    
    % get task data as [subj, stim, resp, conf]
    modal_inds = (data_cell{col_tach}(:) == task & (data_cell{col_resp}(:) ~= 'none'));
    modal_nb_trials = sum(modal_inds);
    modal_data = NaN(modal_nb_trials, 4);
    cc_subj = 1;
    cc_stim = 2;
    cc_resp = 3;
    cc_conf = 4;    
    modal_data(:,cc_subj) = data_cell{col_subj}(modal_inds);
    modal_data(:,cc_stim) = data_cell{col_sigl}(modal_inds);
    modal_data(:,cc_resp) = (data_cell{col_resp}(modal_inds)=='y') - (data_cell{col_resp}(modal_inds)=='n');
    modal_data(:,cc_conf) = data_cell{col_conf}(modal_inds);
    
    my_col = mtlb_colors(itask, :);
    
    %% psychometric functions 
    % for each subject and stimulus level
    % we quantify stimulus difficulty as a z_score  
    for ss = 1:subj_nb
        f_subj = modal_data(:,1)==ss;
        stim_list = unique(modal_data(f_subj,2));
        resp_list = nan(size(stim_list));
        for i_stim = 1:length(stim_list)
            f_stim = modal_data(:,2)==stim_list(i_stim);
            ff = f_subj & f_stim;
            resp_list(i_stim) = mean(modal_data(ff,3)==1);
            z_score = norminv(resp_list(i_stim));
            if abs(z_score)==Inf
                z_score = norminv(mean([modal_data(ff,3)==1; .5]));% adding .5 for padding
            end
            modal_data(ff, 5) = z_score;
        end
        if do_plots
            if exist('figureindiv')
               figure(figureindiv);
            else
                 figureindiv = figure;
            end            
            subplot(8, 9, ss);
            hold on
            plot(stim_list, resp_list, 'o-',  'Color', my_col);
            ylim([0 1])
        end
    end
    %% normalization
    % replace stimulus by zscored performance value in what follows
    if do_normalize
        modal_data(:,2) = modal_data(:,5);
    end
    % sum(modal_data(:,5)==Inf)
    % sum(modal_data(:,5)==-Inf)
    
    %% group subject
    % replace subject numbers (1 ... 29) with patients/control (1/2) in what follows
    % by doing so, we will compute only one efficiency value for each group
    % note that if this is done, we do it after normalization
    if do_groupsubj
        modal_data(:,cc_subj) = 1+(data_cell{3}(modal_inds)=='c');
    end
    
    %% subject loop
    disp('starting the loop')
    tic;
    for ss = 1:subj_nb_loop
       % > loop across subjects (groups if do_groupsubj = TRUE)
        
        % get subject data (remove trials without a response)
        ss_inds = (modal_data(:,cc_subj) == ss) & (modal_data(:,cc_resp) ~= 0);
        ss_data_all = modal_data(ss_inds, [cc_stim, cc_resp, cc_conf]);
        nb_trials_all = size(ss_data_all, 1);
     
        for gr = 1:(1+nb_bootstrap) 
            % > 1 pass for actual data, then more for bootstrap within this dataset
            
            if gr==1 % actual data
                inds_all = 1:nb_trials_all;
            else % boostrap samples
                inds_all = randi(nb_trials_all,1,nb_trials_all);
            end 
            ss_data = ss_data_all(inds_all, :);   
            
            % -> compute d-prime
            p_hit = sum((ss_data(:, 1) > 0.0) & (ss_data(:, 2) == 1)) / sum(ss_data(:, 1) > 0.0);
            p_fa  = sum((ss_data(:, 1) < 0.0) & (ss_data(:, 2) == 1)) / sum(ss_data(:, 1) < 0.0);
            dprime_lst(itask, ss, gr) = norminv(p_hit) - norminv(p_fa);
            
            % -> binarize stimuli 
            % note: this is necessary because meta-dprime requires 2 stimuli
            ss_data_2 = ss_data;
            ss_data_2(:, 1) = (ss_data(:, 1) > 0.0) * 2 - 1;
            crs_data_SRC_2 = crs_group_02(ss_data_2, 'is_continuous', true, ...
                'confidence_range', conf_range, 'confidence_resolution', conf_resol_mratio);
            
            % -> compute meta-d-prime
            [nR_S1, nR_S2] = CRStoML12(crs_data_SRC_2);
            myfit = fit_meta_d_MLE(nR_S1, nR_S2);
            mratio_lst(itask, ss, gr) = myfit.M_ratio;
            
            % -> discretize stimuli 
            % note: this is done to speed up confidence efficiency computation
            stim_vals = NaN(1, stim_nb_bins);
            ss_data_8 = ss_data;
            
            qq = quantile(ss_data(:, 1), stim_nb_bins - 1);
            [~, ~, stm_tsk_bin] = histcounts(ss_data(:, 1), [-inf, qq, inf]);
            for nn = 1:stim_nb_bins
                inds5 = (stm_tsk_bin == nn);
                stim_vals(nn) = mean(ss_data(inds5, 1));
                ss_data_8(inds5, 1) = stim_vals(nn);
            end
            
            % -> compute confidence efficiency
            %     crs_data_SRC = crs_group_02(ss_data_2, 'is_continuous', true, ...
            %         'confidence_range', conf_range, 'confidence_resolution', conf_resol);
            crs_data_SRC = crs_group_02(ss_data_8, 'is_continuous', true, ...
                'confidence_range', conf_range, 'confidence_resolution', conf_resol);
            crs_struct = crs_fit_02(crs_data_SRC, ...
                'is_continuous', true, 'confidence_range', conf_range, ...
                'confidence_resolution', conf_resol);
            eff_lst(itask, ss, gr) = crs_struct.efficiency;
            noise2_lst(itask, ss, gr) = crs_struct.conf_noise;
            boost2_lst(itask, ss, gr) = crs_struct.conf_boost;
            
            % -> keep track of time
            sprintf('step %i-%i out of %i-%i done ',ss,gr,subj_nb_loop,1+nb_bootstrap)
            toc;
        end        
    end
end

%%
return
        
%% figure across measures for indiv analyses
load('metaface_efficiency_indivresults_final.mat');
for itask = 1:3
    
    switch itask
        case 1
            task = "Perception"; %
        case 2
            task = "Familiarity"; %
        case 3
            task = "Recollection"; %
    end
 	my_col = mtlb_colors(itask, :);
 
    % -> *******************************
    % -> histogram
    nb_bins = 10;
    if itask==1
        newf = figure;
    else
        figure(newf);
    end
    subplot(1,2,1);
    hold on;
    % line([myparams(1), myparams(1)], [0 60], 'LineStyle', '-', ...
    %     'Color', [0.466, 0.674, 0.188], 'LineWidth', 3);
    hist_modal = mratio_lst(itask, :, :);
    histogram(hist_modal, nb_bins, 'Normalization', 'pdf', ...
        'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
    % xlim([0.88, 1.12]);
    % ylim([0 60]);
    % yt2 = 0:20:60;
    % set(gca, 'YTick', yt2, 'YTickLabel', yt2);
    set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
    ylabel('Probability Density');
    title('m-ratio');
    
    subplot(1,2,2);
    hold on;
    hist_modal = eff_lst(itask, :, :);
    histogram(hist_modal, nb_bins, 'Normalization', 'pdf', ...
        'DisplayStyle', 'stairs', 'LineWidth', 3, 'EdgeColor', my_col);
    set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
    ylabel('Probability Density');
    title('conf. efficiency');
    
    % -> *******************************
    if itask==1
        newf2 = figure;
    else
        figure(newf2);
    end
    subplot(1,3,itask)
    xvals = mratio_lst(itask, :, :);
    yvals = eff_lst(itask, :, :);
    plot(xvals(:), yvals(:), ...
        'o', 'MarkerSize', 6, 'MarkerFaceColor', my_col, ...
        'Color', [1 1 1], 'LineWidth', 1);
    xlabel('m-ratio');
    ylabel('conf. efficiency');
    axis('square');
    title(task)
end

%% figure across modalities for indiv analyses
my_col = [0.2, 0.2, 0.2];
figure;
for k=1:3
    for p=1:2
        if p==1
            metavar = mratio_lst; mlab = 'mratio';
        else
            metavar = eff_lst; mlab = 'eff';
        end
        switch k
            case 1
                xvals = squeeze(metavar(1,:)); xlab = 'P';
                yvals = squeeze(metavar(2,:)); ylab = 'F';
            case 2
                xvals = squeeze(metavar(1,:)); xlab = 'P';
                yvals = squeeze(metavar(3,:)); ylab = 'R';
            case 3
                xvals = squeeze(metavar(2,:)); xlab = 'F';
                yvals = squeeze(metavar(3,:)); ylab = 'R';
        end
        
        subplot(2,3,k+ (p-1)*3)
        plot(xvals, yvals, 'o', 'MarkerSize', 6, 'MarkerFaceColor', my_col, ...
            'Color', [1 1 1], 'LineWidth', 1);
        xlabel([mlab ' ' xlab]);
        ylabel([mlab ' ' ylab]);
        set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 16);
        axis([0, 1.2, 0, 1.2]);
        axis('square');
        [rr, pp] = corr(xvals', yvals','type','Spearman');
        fprintf('corr %s %s-%s : r^2 = %6.3f, p = %6.3f\n', mlab, xlab,ylab,rr^2, pp);
    end
end

%% figure patients vs. controls in individual analyses
load('metaface_efficiency_indivresults_final.mat')
figure
for m = 1:3
subplot(1,3,m)
hold on
for k=1:2
    hist_modal = eff_lst(m,patients_list==k-1);
    mycol = mtlb_colors(k,:);
    histogram(hist_modal, 0:.2:3, 'Normalization', 'pdf', ...
        'DisplayStyle', 'stairs', 'LineWidth', 2, 'EdgeColor', mycol);
end
title(tasklist{m})
axis('square')
end
legend('controls','patients')

%% figure patients vs. controls with bootstrap analysis
load('metaface_efficiency_bootstrapresults_final.mat')
% save('scripts\behav\schizo\eff_lst.mat','eff_lst')
figure
for m=1:3    
    subplot(1,3,m)
    hold on
    for k=1:2
        hist_modal = eff_lst(m,3-k,:);
        mycol = mtlb_colors(k,:);
        histogram(hist_modal, 0:.1:2, 'Normalization', 'pdf', ...
            'DisplayStyle', 'stairs', 'LineWidth', 2, 'EdgeColor', mycol);
    end
    title(tasklist{m})
    axis('square')
end
legend('controls','patients')



