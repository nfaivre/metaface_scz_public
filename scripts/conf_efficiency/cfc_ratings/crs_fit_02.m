% CONFIDENCE RATING SCALE TOOLBOX  v0.1
%
% crs_fit
%   This is the wrap function for confidence model fit.
%
% INPUT:
%   'crs_data_SRC': grouped data per (stim, resp, conf_id, conf_prob):
%       1st col: stimulus intensity
%       2nd col: perceptual decision
%       3rd col: confidence level (index 1, 2, ...)
%       4th col: confidence probability (for that conf_id | stim, resp)
% note: the 4th column can also just be the count of trials for this condition
%
%
% OPTIONAL PARAMETERS:
%
%   'model_parameters': model free parameters, as a structure
%                       increment numbers for desired parameters
%                       (use vectors if more than 1 task,
%                       use NaN for padding):
%       'sens_noise' : sensory (Type 1) sdtev of noise (0 = perfectly sensitive)
%       'sens_crit'  : sensory (Type 1) criterion
%       'conf_noise' : confidence (Type 2) sdtev of noise (0 = ideal)
%       'conf_boost' : fraction super-ideal (0 = ideal, 1 = super-ideal)
%       'conf_crit'  : confidence (Type 2) criterion
%
%   'model_fixed_values': model fixed values parameters, as a structure
%                       (use vectors if more than 1 task):
%       'sens_noise' : sensory (Type 1) sdtev of noise (0 = perfectly sensitive)
%       'sens_crit'  : sensory (Type 1) criterion
%       'conf_noise' : confidence (Type 2) sdtev of noise (0 = ideal)
%       'conf_boost' : fraction super-ideal (0 = ideal, 1 = super-ideal)
%       'conf_crit'  : confidence (Type 2) criterion
%
%   'boost_init': redo the fit from multiple boost starting points
%       (provide list here as a vector) to avoid local minima
%
%   'skip_efficiency': skip computing efficiency (for faster computations)
%
%   'verbose': verbose flag:
%       0: remove all online reports
%       1: print parameter estimates
%       2: print parameter estimates and fitting progress
%
%
%
% OUTPUT: crs_struct = struct
%   crs_struct.sens_noise        sdtev of sensory noise (0 = perfect sensitivity)
%   crs_struct.sens_crit         sensory criterion
%   crs_struct.conf_noise        sdtev of confidence noise (0 = ideal)
%   crs_struct.conf_boost        confidence boost (fraction super-ideal)
%   crs_struct.conf_crit         confidence criterion
%   crs_struct.intrvl_bias       intrvl_bias (bias in favour of interval 1)
%   crs_struct.conf_bias         confidence bias (relative to a task set to 1.0)
%   crs_struct.efficiency        efficiency
%   crs_struct.choice_prob_ideal            ideal choice probabilities
%   crs_struct.choice_prob_super_ideal      super-ideal choice probabilities
%   crs_struct.choice_prob_eff              efficiency choice probabilities
%   crs_struct.choice_prob_model            full-model choice probabilities
%   crs_struct.loglike                      log-likelihood of best fit
%
% 
% EXAMPLES OF USE:
%   crs_struct = crs_fit(crs_data_SRC)
%   crs_struct = crs_fit(crs_data_SRC, 'boost2_init', [0.2, 0.5, 0.8])
% 
%
% 23-AUG-2021 - pascal mamassian
% 16-FEB-2022 - pm: added continuous ratings


function crs_struct = crs_fit_02(crs_data_SRC, varargin)

    % -> default optional arguments
    dflt_model            = struct; % pass on specific model
    dflt_fixed            = struct; % pass on fixed values
    dflt_boost2_init      = [];     % initial value (or list of values) for boost2
    dflt_skip_efficiency  = false;  % skip computing efficiency
    dflt_verbose          = 1;      % verbose flag
    dflt_is_continuous    = false;  % continuous (T) or discrete (F) rating scale
    dflt_conf_range       = [0, 1];
    dflt_conf_resol       = 100;

    
    % -> parse all arguments
    ip = inputParser;
    ip.StructExpand = false;
    addRequired(ip, 'crs_data_SRC', @isnumeric);
    addParameter(ip, 'model_parameters', dflt_model, @isstruct);
    addParameter(ip, 'model_fixed_values', dflt_fixed, @isstruct);
    addParameter(ip, 'boost_init', dflt_boost2_init, @isnumeric);
    addParameter(ip, 'skip_efficiency', dflt_skip_efficiency, @islogical);
    addParameter(ip, 'verbose', dflt_verbose, @isnumeric);
    addParameter(ip, 'is_continuous', dflt_is_continuous, @islogical);
    addParameter(ip, 'confidence_range', dflt_conf_range, @isnumeric);
    addParameter(ip, 'confidence_resolution', dflt_conf_resol, @isnumeric);
    
    
    parse(ip, crs_data_SRC, varargin{:});
    my_model_params = ip.Results.model_parameters;
    my_fixed_params = ip.Results.model_fixed_values;
    boost_init_lst = ip.Results.boost_init;
    skip_efficiency = ip.Results.skip_efficiency;
    verbose_flag = ip.Results.verbose;
    conf_continuous = ip.Results.is_continuous;
    conf_range = ip.Results.confidence_range;
    conf_resol = ip.Results.confidence_resolution;
    
    
    compute_efficiency = ~skip_efficiency;	% compute efficiency


    options1 = optimset;
    if (verbose_flag >= 2)
        options2 = optimset('Display','iter', 'TolFun',1e-3, 'TolX',1e-3);
        tstart = tic;   % start timer
    else
        options2 = optimset('TolFun',1e-3, 'TolX',1e-3);
    end
    

% -> 0. extract proportion of choices for each confidence level
%    for the original human data

% -> clean up input data
% crs_data_SRC = crs_group_01(crs_data_SRC);
crs_data_SRC = crs_group_02(crs_data_SRC, ...
    'is_continuous', conf_continuous, ...
    'confidence_range', conf_range, ...
    'confidence_resolution', conf_resol);

col_stim = 1;
col_resp = 2;
col_conf_levl = 3;
col_conf_prob = 4;

stim_lst = unique(crs_data_SRC(:, col_stim));
stim_nb = length(stim_lst);

resp_lst = unique(crs_data_SRC(:, col_resp));
resp_nb = length(resp_lst);

rating_inds = unique(crs_data_SRC(:, col_conf_levl));
conf_levels_nb = length(rating_inds);
rating_bnds_nb = conf_levels_nb - 1;


% -> convert counts of confidence ratings into probabilities (conditional
%    on stimulus)
data_SRC_norm = NaN(size(crs_data_SRC,1),1);
for ss = 1:stim_nb
    sens_mean = stim_lst(ss);
    cond_inds = (crs_data_SRC(:, col_stim) == sens_mean);
    data_SRC_norm(cond_inds) = ...
        crs_data_SRC(cond_inds, col_conf_prob) ./ sum(crs_data_SRC(cond_inds, col_conf_prob));
end
crs_data_SRC(:, col_conf_prob) = data_SRC_norm;


% -> use cell padding technique advised in 'type2_SDT_MLE'
% cellpadding = 1 / (2*conf_levels_nb);
% cellpadding = 1 / (100*conf_levels_nb);   % <<
cellpadding = 1 / (1000*conf_levels_nb);
if any(crs_data_SRC(:,col_conf_prob) < 0.001)
%     crs_data_SRC(:,col_conf_prob) = crs_data_SRC(:,col_conf_prob) + cellpadding;
    crs_data_SRC(:,col_conf_prob) = crs_data_SRC(:,col_conf_prob) + ...
        cellpadding * rand(size(crs_data_SRC, 1), 1);
    
    % -> re-normalize to still get sum(prob)=1
    for ss = 1:stim_nb
        sens_mean = stim_lst(ss);
        cond_inds = (crs_data_SRC(:, col_stim) == sens_mean);
        data_SRC_norm(cond_inds) = ...
            crs_data_SRC(cond_inds, col_conf_prob) ./ sum(crs_data_SRC(cond_inds, col_conf_prob));
    end
    crs_data_SRC(:, col_conf_prob) = data_SRC_norm;
end


human_rating_tofit = [crs_data_SRC(:,col_conf_prob), 1 - crs_data_SRC(:,col_conf_prob)];


    
    % -> default values of the parameters
    default_sens_noise  = 1.0;
    default_sens_crit   = 0.0;
    default_conf_noise  = 0.0;
    default_conf_boost  = 0.0;
    default_conf_crit   = 0.0;
    default_llo_gamma   = 1.0;
    default_llo_p0      = 0.5;
    
    % -> initial values of the parameters
    initial_sens_noise  = 1.2;
    initial_sens_crit   = 0.1;
    initial_conf_noise  = 0.5;
%     initial_conf_boost  = 0.2;
    initial_conf_boost  = 0.5;
    initial_conf_crit   = 0.4;
%     initial_conf_bnds   = linspace(0.5, 2, rating_bnds_nb);
    initial_conf_bnds   = linspace(0.2, 0.8, rating_bnds_nb);
    
    % -> lower and upper bound values of the parameters
    lo_bnd_sens_noise   = 0.0;      hi_bnd_sens_noise   = Inf;
    lo_bnd_sens_crit    = -Inf;     hi_bnd_sens_crit    = Inf;
    lo_bnd_conf_noise   = 0.0;      hi_bnd_conf_noise   = Inf;
    lo_bnd_conf_boost   = 0.0;      hi_bnd_conf_boost   = 1.0;
    lo_bnd_conf_crit    = -Inf;     hi_bnd_conf_crit    = Inf;
%     lo_bnd_conf_bnds = -Inf * ones(1, rating_bnds_nb);
%     hi_bnd_conf_bnds = +Inf * ones(1, rating_bnds_nb);
    lo_bnd_conf_bnds = zeros(1, rating_bnds_nb);
    hi_bnd_conf_bnds =  ones(1, rating_bnds_nb);
    lo_llo_gamma        = 0.0;     hi_llo_gamma        = Inf;
    lo_llo_p0           = 0.0;      hi_llo_p0           = 1.0;
    


% -> 'params_set' is a struct that defines the free parameters, and 
%    their order. Put '0' if the variable is not a free parameter.
default_params_set = struct;
default_params_set.sens_noise = 0;
default_params_set.sens_crit  = 0;
default_params_set.conf_noise = 0;
default_params_set.conf_boost = 0;
default_params_set.conf_crit  = 0;


% -> params for ideal confidence observer
ideal_conf_boost = 0.0;
ideal_conf_noise = 0.001;

% -> params for super-ideal confidence observer
superideal_conf_boost = 1.0;


% ------------------------------------------------------------------------
% -> 1. 'type1': fit Type 1 model (cumulative Gaussian)
%       to extract the Type 1 parameters, i.e. (sens_noise) and (sens_crit)
resp_vec = NaN(stim_nb, 1);
for ss = 1:stim_nb
    stim_val = stim_lst(ss);
    lcl_righ_inds = ((crs_data_SRC(:,col_stim) == stim_val) & ...
                     (crs_data_SRC(:,col_resp) == 1));
    resp_vec(ss) = sum(crs_data_SRC(lcl_righ_inds, col_conf_prob));
end
nn1_resp_list = [resp_vec, 1-resp_vec];

params0_cumul = [initial_sens_noise, initial_sens_crit];
paramsLB_cumul = [lo_bnd_sens_noise, lo_bnd_sens_crit];
paramsUB_cumul = [hi_bnd_sens_noise, hi_bnd_sens_crit];

%my_fun_0 = @(pp) basic_normcdf(stim_lst, pp(2), pp(1));
my_fun_0 = @(pp) normcdf(stim_lst, pp(2), pp(1));
param_type1 = fitnll(my_fun_0, nn1_resp_list, ...
    params0_cumul, paramsLB_cumul, paramsUB_cumul, options1);

idl_sens_noise = param_type1(1);
idl_sens_crit  = param_type1(2);


% ------------------------------------------------------------------------
% -> 2. 'ideal': build the ideal observer that is constrained by Type 1 
%       performance (infered from data), 
%       that has (conf_noise = 0) & (conf_boost = 0)
%       and compute the confidence ratings for this ideal confidence observer
model_idl_params = struct;
model_idl_params.sens_noise = idl_sens_noise;
model_idl_params.sens_crit  = idl_sens_crit;
model_idl_params.conf_noise = ideal_conf_noise;
model_idl_params.conf_boost = ideal_conf_boost;
model_idl_params.conf_crit  = default_conf_crit;
model_idl_params.conf_continuous  = conf_continuous;

if (conf_continuous)
    
    llo_gamma_ideal = default_llo_gamma;
    llo_p0_ideal = default_llo_p0;
    
    % -> compute the confidence ratings for the ideal confidence observer
    model_idl_params.conf_range  = conf_range;
    model_idl_params.conf_resol = conf_resol;
    model_idl_params.llo_gamma = llo_gamma_ideal;
    model_idl_params.llo_p0 = llo_p0_ideal;

else
    params_set_idl = default_params_set;
    params_set_idl.conf_bnds  = (1:rating_bnds_nb);

    fixed_vals_idl = struct;
    fixed_vals_idl.sens_noise = idl_sens_noise;
    fixed_vals_idl.sens_crit  = idl_sens_crit;
    fixed_vals_idl.conf_crit  = default_conf_crit;
    fixed_vals_idl.conf_boost = ideal_conf_boost;
    fixed_vals_idl.conf_noise = ideal_conf_noise;
    fixed_vals_idl.conf_continuous = conf_continuous;
%     fixed_vals_idl.conf_bnds  = model_orig_params.conf_bnds;

    params0_idl = initial_conf_bnds;
    paramsLB_idl = lo_bnd_conf_bnds;
    paramsUB_idl = hi_bnd_conf_bnds;

    % -> extract the confidence boundaries for the ideal confidence observer
    my_fun_ideal = @(pp) lastcol(crs_core_wrap(crs_data_SRC, ...
        pp, params_set_idl, fixed_vals_idl));

    [best_params_ideal, ideal_human_loglike] = ...
        fitnll(my_fun_ideal, human_rating_tofit, ...
        params0_idl, paramsLB_idl, paramsUB_idl, options1);

    % -> boundaries are sometimes in random order
    bnd_lst_ideal = sort(best_params_ideal); 

    % -> compute the confidence ratings for the ideal confidence observer
    model_idl_params.conf_bnds  = bnd_lst_ideal;

end

ideal_rating_SRC = crs_core_02(crs_data_SRC, model_idl_params);
ideal_rating_tofit = [ideal_rating_SRC(:,4), 1 - ideal_rating_SRC(:,4)];


% ------------------------------------------------------------------------
% -> 3. 'super-human': build the super-ideal confidence observer
%    that has (conf_boost = 1) and free (conf_noise)
%    and extract equivalent confidence noise for this 'super-human'
params_set_super = default_params_set;
params_set_super.conf_noise = 1;

fixed_vals_super = struct;
fixed_vals_super.sens_noise = idl_sens_noise;
fixed_vals_super.sens_crit  = idl_sens_crit;
fixed_vals_super.conf_crit  = default_conf_crit;
fixed_vals_super.conf_boost = superideal_conf_boost;
fixed_vals_super.conf_continuous  = conf_continuous;

params0_super = initial_conf_noise;
paramsLB_super = lo_bnd_conf_noise;
paramsUB_super = hi_bnd_conf_noise;

if (conf_continuous)
    
    fixed_vals_super.conf_range  = conf_range;
    fixed_vals_super.conf_resol = conf_resol;
%     fixed_vals_super.llo_gamma = default_llo_gamma;
%     fixed_vals_super.llo_p0 = default_llo_p0;

    params_set_super.llo_gamma = 2;
    params_set_super.llo_p0 = 3;
    
    params0_super = [params0_super, default_llo_gamma, default_llo_p0];
    paramsLB_super = [paramsLB_super, lo_llo_gamma, lo_llo_p0];
    paramsUB_super = [paramsUB_super, hi_llo_gamma, hi_llo_p0];
    
else
    
    % -> extract the confidence boundaries for the 'super-human'
    params_set_super.conf_bnds = 1 + (1:rating_bnds_nb);

    params0_super = [params0_super, initial_conf_bnds];
    paramsLB_super = [paramsLB_super, lo_bnd_conf_bnds];
    paramsUB_super = [paramsUB_super, hi_bnd_conf_bnds];
    
end

% -> extract the confidence parameters for the 'super-human'
my_fun_super = @(pp) lastcol(crs_core_wrap(crs_data_SRC, ...
    pp, params_set_super, fixed_vals_super));

[super_human_params, super_human_loglike] = fitnll(my_fun_super, human_rating_tofit, ...
    params0_super, paramsLB_super, paramsUB_super, options2);
equiv_noise_hum = super_human_params(1);


% -> compute the confidence ratings for the 'super-human'
params_super_human = struct;
params_super_human.sens_noise = idl_sens_noise;
params_super_human.sens_crit  = idl_sens_crit;
params_super_human.conf_noise = equiv_noise_hum;
params_super_human.conf_boost = superideal_conf_boost;
params_super_human.conf_crit  = default_conf_crit;
params_super_human.conf_continuous  = conf_continuous;

if (conf_continuous)
    llo_gamma_super_human = super_human_params(2);
    llo_p0_super_human = super_human_params(3);
    
    params_super_human.conf_range  = conf_range;
    params_super_human.conf_resol = conf_resol;
    params_super_human.llo_gamma = llo_gamma_super_human;
    params_super_human.llo_p0 = llo_p0_super_human;

else
    % -> sort confidence boundaries in increasing order
    super_human_rtng_bnd_lst = sort(super_human_params(2:end));

    % -> compute the confidence ratings for the 'super-human'
    params_super_human.conf_bnds  = super_human_rtng_bnd_lst;

end

super_human_rating_mat = crs_core_02(crs_data_SRC, params_super_human);


% ------------------------------------------------------------------------
% -> 4. 'super-ideal': get the equivalent noise for the 'ideal' super-ideal 
%    confidence observer
[super_ideal_params, super_ideal_loglike] = fitnll(my_fun_super, ideal_rating_tofit, ...
    params0_super, paramsLB_super, paramsUB_super, options2);
equiv_noise_idl = super_ideal_params(1);


% -> compute the confidence ratings for the 'super-ideal'
params_super_ideal = struct;
params_super_ideal.sens_noise = idl_sens_noise;
params_super_ideal.sens_crit  = idl_sens_crit;
params_super_ideal.conf_noise = equiv_noise_idl;
params_super_ideal.conf_boost = superideal_conf_boost;
params_super_ideal.conf_crit  = default_conf_crit;
params_super_ideal.conf_continuous  = conf_continuous;

if (conf_continuous)
    llo_gamma_super_ideal = super_human_params(2);
    llo_p0_super_ideal = super_human_params(3);
    params_super_ideal.conf_range  = conf_range;
    params_super_ideal.conf_resol = conf_resol;
    params_super_ideal.llo_gamma = llo_gamma_super_ideal;
    params_super_ideal.llo_p0 = llo_p0_super_ideal;
else
    super_ideal_rtng_bnd_lst = sort(super_ideal_params(2:end));
    params_super_ideal.conf_bnds  = super_ideal_rtng_bnd_lst;
end

super_ideal_rating_mat = crs_core_02(crs_data_SRC, params_super_ideal);


% -> use ratio of variance (std dev squared) for definition of efficiency
efficiency = (equiv_noise_idl / equiv_noise_hum)^2;

% ------------------------------------------------------------------------
% -> 5. 'full-model'
%    keep the Type 1 parameters from the ideal fit

params_set_full = default_params_set;
params_set_full.conf_noise = 1;
params_set_full.conf_boost = 2;

fixed_vals_full = struct;
fixed_vals_full.sens_noise = idl_sens_noise;
fixed_vals_full.sens_crit  = idl_sens_crit;
fixed_vals_full.conf_crit  = default_conf_crit;
fixed_vals_full.conf_continuous  = conf_continuous;

params0_full = [initial_conf_noise, initial_conf_boost];
paramsLB_full = [lo_bnd_conf_noise, lo_bnd_conf_boost];
paramsUB_full = [hi_bnd_conf_noise, hi_bnd_conf_boost];


if (conf_continuous)
    
    fixed_vals_full.conf_range  = conf_range;
    fixed_vals_full.conf_resol = conf_resol;

    params_set_full.llo_gamma = 3;
    params_set_full.llo_p0 = 4;
    
    params0_full = [params0_full, default_llo_gamma, default_llo_p0];
    paramsLB_full = [paramsLB_full, lo_llo_gamma, lo_llo_p0];
    paramsUB_full = [paramsUB_full, hi_llo_gamma, hi_llo_p0];
    
else
    params_set_full.conf_bnds  = 2 + (1:rating_bnds_nb);

    params0_full = [params0_full, initial_conf_bnds];
    paramsLB_full = [paramsLB_full, lo_bnd_conf_bnds];
    paramsUB_full = [paramsUB_full, hi_bnd_conf_bnds];

end
param_free_nb2 = length(params0_full);

% -> extract the Type 2 parameters ('conf_noise' and 'conf_boost')
%    and confidence boundaries for the ideal confidence observer
my_fun_full = @(pp) lastcol(crs_core_wrap(crs_data_SRC, ...
    pp, params_set_full, fixed_vals_full));


% -> redo the fit from multiple boost2 starting points to avoid local minima
if (isempty(boost_init_lst))
    boost_init_lst = initial_conf_boost;
end
boost2_init_nb = length(boost_init_lst);
paramBest_mat = NaN(boost2_init_nb, param_free_nb2);
loglike_lst = NaN(boost2_init_nb, 1);

% -> do the fit for multiple starting values of 'boost2'
for bb = 1:boost2_init_nb
    boost2_init_val = boost_init_lst(bb);
    params0_full(2) = boost2_init_val;


[paramBest, loglike] = fitnll(my_fun_full, human_rating_tofit, ...
    params0_full, paramsLB_full, paramsUB_full, options1);


    paramBest_mat(bb, :) = paramBest;
    loglike_lst(bb) = loglike;

    if ((boost2_init_nb > 1) && (verbose_flag >= 1))
        fprintf('For boost2_init = %5.3f, loglike = %7.3f\n', ...
            boost2_init_val, loglike);
    end
end

% -> pick the initial boost value that led to maximum likelihood
[~, ii] = max(loglike_lst);
best_params_full = paramBest_mat(ii, :);
model_full_loglike = loglike_lst(ii);


full_conf_noise = best_params_full(1);
full_conf_boost = best_params_full(2);

% -> compute the confidence ratings for the 'full-model'
model_full_params = struct;
model_full_params.sens_noise = idl_sens_noise;
model_full_params.sens_crit  = idl_sens_crit;
model_full_params.conf_noise = full_conf_noise;
model_full_params.conf_boost = full_conf_boost;
model_full_params.conf_crit  = default_conf_crit;
model_full_params.conf_continuous  = conf_continuous;

if (conf_continuous)
    llo_gamma_full = best_params_full(3);
    llo_p0_full = best_params_full(4);
    
    model_full_params.conf_range  = conf_range;
    model_full_params.conf_resol = conf_resol;
    model_full_params.llo_gamma = llo_gamma_full;
    model_full_params.llo_p0 = llo_p0_full;
else
    bnd_lst_full = sort(best_params_full(3:end)); 
    model_full_params.conf_bnds  = bnd_lst_full;
end

full_rating_SRC = crs_core_02(crs_data_SRC, model_full_params);


% ------------------------------------------------------------------------
% -> 6. extra step: recompute confidence efficiency based on full model

fake_human_rating_tofit = [full_rating_SRC(:,col_conf_prob), 1 - full_rating_SRC(:,col_conf_prob)];

[fake_human_params, fake_human_loglike] = fitnll(my_fun_super, fake_human_rating_tofit, ...
    params0_super, paramsLB_super, paramsUB_super, options2);
equiv_noise_fake = fake_human_params(1);

fake_efficiency = (equiv_noise_idl / equiv_noise_fake)^2;



% % ------------------------------------------------------------------------
% % -> 6b. extra step: recompute confidence efficiency based on full model
% %        reduce data to most ambiguous situation
% 
% fixed_vals_super3 = fixed_vals_super;
% fixed_vals_super3.conf_resol = 2;
% my_fun_super3 = @(pp) lastcol(crs_core_wrap(crs_data_SRC, ...
%     pp, params_set_super, fixed_vals_super3));
% 
% model_idl3_params = model_idl_params;
% model_idl3_params.conf_resol = 2;
% ideal3_rating_SRC = crs_core_02(crs_data_SRC, model_idl3_params);
% ideal3_rating_tofit = [ideal3_rating_SRC(:,4), 1 - ideal3_rating_SRC(:,4)];
% 
% [super_ideal3_params, super3_ideal_loglike] = fitnll(my_fun_super3, ideal3_rating_tofit, ...
%     params0_super, paramsLB_super, paramsUB_super, options2);
% equiv_noise_idl3 = super_ideal3_params(1);
% 
% 
% model_full3_params = model_full_params;
% model_full3_params.conf_resol = 2;
% fake3_rating_SRC = crs_core_02(crs_data_SRC, model_full3_params);
% 
% 
% fake3_human_rating_tofit = [fake3_rating_SRC(:,col_conf_prob), 1 - fake3_rating_SRC(:,col_conf_prob)];
% 
% [fake3_human_params, fake3_human_loglike] = fitnll(my_fun_super3, fake3_human_rating_tofit, ...
%     params0_super, paramsLB_super, paramsUB_super, options2);
% equiv_noise_fake3 = fake3_human_params(1);
% 
% fake3_efficiency = (equiv_noise_idl3 / equiv_noise_fake3)^2;
% 
% 
% 
% % ------------------------------------------------------------------------
% % -> 7. extra step: recompute confidence efficiency based on fitted model
% %    that has (conf_boost = 0) and free (conf_noise)
% %    and extract equivalent confidence noise for this "zero"-boost
% 
% 
% % -> model with (conf_boost = 0) and free (conf_noise)
% params_set_zero = default_params_set;
% params_set_zero.conf_noise = 1;
% 
% fixed_vals_zero = struct;
% fixed_vals_zero.sens_noise = idl_sens_noise;
% fixed_vals_zero.sens_crit  = idl_sens_crit;
% fixed_vals_zero.conf_crit  = default_conf_crit;
% fixed_vals_zero.conf_boost = 0.0;
% fixed_vals_zero.conf_continuous  = conf_continuous;
% 
% params0_zero = initial_conf_noise;
% paramsLB_zero = lo_bnd_conf_noise;
% paramsUB_zero = hi_bnd_conf_noise;
% 
% if (conf_continuous)
%     fixed_vals_zero.conf_range  = conf_range;
%     fixed_vals_zero.conf_resol = conf_resol;
%     
%     params_set_zero.llo_gamma = 2;
%     params_set_zero.llo_p0 = 3;
%     
%     params0_zero = [params0_zero, default_llo_gamma, default_llo_p0];
%     paramsLB_zero = [paramsLB_zero, lo_llo_gamma, lo_llo_p0];
%     paramsUB_zero = [paramsUB_zero, hi_llo_gamma, hi_llo_p0];
% else
%     params_set_zero.conf_bnds = 1 + (1:rating_bnds_nb);
%     
%     params0_zero = [params0_zero, initial_conf_bnds];
%     paramsLB_zero = [paramsLB_zero, lo_bnd_conf_bnds];
%     paramsUB_zero = [paramsUB_zero, hi_bnd_conf_bnds];
% end
% 
% % -> extract the confidence parameters for the 'zero' confidence observer
% my_fun_zero = @(pp) lastcol(crs_core_wrap(crs_data_SRC, ...
%     pp, params_set_zero, fixed_vals_zero));
% 
% [zero_human_params, zero_human_loglike] = fitnll(my_fun_zero, human_rating_tofit, ...
%     params0_zero, paramsLB_zero, paramsUB_zero, options2);
% 
% 
% % -> model with free (conf_boost) and (conf_noise = 0)
% params_set_zero2 = default_params_set;
% params_set_zero2.conf_boost = 1;
% 
% fixed_vals_zero2 = struct;
% fixed_vals_zero2.sens_noise = idl_sens_noise;
% fixed_vals_zero2.sens_crit  = idl_sens_crit;
% fixed_vals_zero2.conf_crit  = default_conf_crit;
% fixed_vals_zero2.conf_noise = ideal_conf_noise;
% fixed_vals_zero2.conf_continuous  = conf_continuous;
% 
% params0_zero2 = initial_conf_boost;
% paramsLB_zero2 = lo_bnd_conf_boost;
% paramsUB_zero2 = hi_bnd_conf_boost;
% 
% if (conf_continuous)
%     fixed_vals_zero2.conf_range  = conf_range;
%     fixed_vals_zero2.conf_resol = conf_resol;
% 
%     params_set_zero2.llo_gamma = 2;
%     params_set_zero2.llo_p0 = 3;
%     
%     params0_zero2 = [params0_zero2, default_llo_gamma, default_llo_p0];
%     paramsLB_zero2 = [paramsLB_zero2, lo_llo_gamma, lo_llo_p0];
%     paramsUB_zero2 = [paramsUB_zero2, hi_llo_gamma, hi_llo_p0];
% else
%     params_set_zero2.conf_bnds = 1 + (1:rating_bnds_nb);
% 
%     params0_zero2 = [params0_zero2, initial_conf_bnds];
%     paramsLB_zero2 = [paramsLB_zero2, lo_bnd_conf_bnds];
%     paramsUB_zero2 = [paramsUB_zero2, hi_bnd_conf_bnds];
% end
% 
% % -> extract the confidence parameters for the 'zero' confidence observer
% my_fun_zero2 = @(pp) lastcol(crs_core_wrap(crs_data_SRC, ...
%     pp, params_set_zero2, fixed_vals_zero2));
% 
% [zero2_human_params, zero2_human_loglike] = fitnll(my_fun_zero2, human_rating_tofit, ...
%     params0_zero2, paramsLB_zero2, paramsUB_zero2, options2);
% 
% 
% % -> check which model is best between
% %    (conf_boost = 0) and free (conf_noise)
% %    and
% %    free (conf_boost) and (conf_noise = 0)
% 
% % if (zero_human_loglike > zero2_human_loglike)
% if (efficiency < 1.0)
%     % -> best is: (conf_boost = 0) and free (conf_noise)
%     equiv_noise_zero = zero_human_params(1);
%     equiv_boost_zero = 0.0;
%     conf_params_zero = zero_human_params;
% else
%     % -> best is: free (conf_boost) and (conf_noise = 0)
%     equiv_noise_zero = ideal_conf_noise;
%     equiv_boost_zero = zero2_human_params(1);
%     conf_params_zero = zero2_human_params;
% end
% 
% 
% % -> compute the confidence ratings for the 'zero' super ideal confidence observer
% params_zero_human = struct;
% params_zero_human.sens_noise = idl_sens_noise;
% params_zero_human.sens_crit  = idl_sens_crit;
% params_zero_human.conf_noise = equiv_noise_zero;
% params_zero_human.conf_boost = equiv_boost_zero;
% params_zero_human.conf_crit  = default_conf_crit;
% params_zero_human.conf_continuous  = conf_continuous;
% 
% if (conf_continuous)
%     llo_gamma_zero_human = conf_params_zero(2);
%     llo_p0_zero_human = conf_params_zero(3);
%     
%     params_zero_human.conf_range  = conf_range;
%     params_zero_human.conf_resol = conf_resol;
%     params_zero_human.llo_gamma = llo_gamma_zero_human;
%     params_zero_human.llo_p0 = llo_p0_zero_human;
% 
% else
% zero_human_rtng_bnd_lst = sort(conf_params_zero(2:end));  % sort in increasing order
% 
% % -> compute the confidence ratings for the 'zero' super ideal confidence observer
% params_zero_human.conf_bnds  = zero_human_rtng_bnd_lst;
% 
% end
% 
% zero_human_rating_mat = crs_core_02(crs_data_SRC, params_zero_human);
% 
% fake2_human_rating_tofit = [zero_human_rating_mat(:,col_conf_prob), ...
%     1 - zero_human_rating_mat(:,col_conf_prob)];
% 
% [fake2_human_params, zero2_human_loglike] = fitnll(my_fun_super, fake2_human_rating_tofit, ...
%     params0_super, paramsLB_super, paramsUB_super, options2);
% equiv_noise_fake2 = fake2_human_params(1);
% 
% fake2_efficiency = (equiv_noise_idl / equiv_noise_fake2)^2;
% 

% ------------------------------------------------------------------------
% -> fill in struct
crs_struct = struct;
crs_struct.sens_noise = idl_sens_noise;     % sensory noise
crs_struct.sens_crit  = idl_sens_crit;      % sensory criterion
crs_struct.conf_noise = full_conf_noise;    % sdtev of noise for Type 2 decision (0 = ideal)
crs_struct.conf_boost = full_conf_boost;    % fraction super-ideal (1 - fraction ideal)
crs_struct.conf_crit  = default_conf_crit;  % criterion for Type 2 decision (criterion)

crs_struct.efficiency = efficiency;                   % efficiency
crs_struct.equiv_conf_noise_ideal = equiv_noise_idl;  % equivalent noise 2 for ideal perf
crs_struct.equiv_conf_noise_human = equiv_noise_hum;  % equivalent noise 2 for human perf

crs_struct.fake_efficiency = fake_efficiency;         % efficiency based on full model
% crs_struct.fake2_efficiency = fake2_efficiency;       % efficiency based on zero model
% crs_struct.fake3_efficiency = fake3_efficiency;       % efficiency based on zero model

if (conf_continuous)
    crs_struct.llo_gamma_ideal      = llo_gamma_ideal;
    crs_struct.llo_p0_ideal         = llo_p0_ideal;
    crs_struct.llo_gamma_super_ideal = llo_gamma_super_ideal;
    crs_struct.llo_p0_super_ideal   = llo_p0_super_ideal;
    crs_struct.llo_gamma_eff        = llo_gamma_super_human;
    crs_struct.llo_p0_eff           = llo_p0_super_human;
    crs_struct.llo_gamma_full       = llo_gamma_full;
    crs_struct.llo_p0_full          = llo_p0_full;
else
    crs_struct.conf_bnd_ideal       = bnd_lst_ideal;
    crs_struct.conf_bnd_super_ideal = super_ideal_rtng_bnd_lst;
    crs_struct.conf_bnd_eff         = super_human_rtng_bnd_lst;
    crs_struct.conf_bnd_full        = bnd_lst_full;
end

crs_struct.conf_rating_ideal_SRC       = ideal_rating_SRC;
crs_struct.conf_rating_super_ideal_SRC = super_ideal_rating_mat;
crs_struct.conf_rating_eff_SRC         = super_human_rating_mat;
crs_struct.conf_rating_full_SRC        = full_rating_SRC;


crs_struct.loglike = model_full_loglike;               % log-likelihood of best fit






% ------------------------------------------------------------------------
% -> local functions

function my_vec = lastcol(my_mat)
    my_vec = my_mat(:,end);
end


% -> wrapper around cfc_core function to flexibly add parameters
function pred_rating_mat = crs_core_wrap(crs_data_SRC, variable_prms, params_set, fixed_vals)

    model_params = pack_params_in_struct(variable_prms, params_set, fixed_vals);
    pred_rating_mat = crs_core_02(crs_data_SRC, model_params);
end


% -> format parameters in a 'struct' variable
function params_struct = pack_params_in_struct(variable_prms, params_set, fixed_vals)

    params_struct = fixed_vals;
    params_cell = struct2cell(params_set);
    fld_nms = fieldnames(params_set);

    variable_nb = length(variable_prms);

    for kk = 1:variable_nb
        fld_ind = cellfun(@(xx) find(xx == kk), params_cell, 'UniformOutput', false);
        uu = find(~cellfun(@isempty, fld_ind));

        % -> allow for multiple use of a variable across model parameters
        for pp = 1:length(uu)
            vv = fld_ind{uu(pp)};

            % -> allow for multiple use of a variable within model parameters
            for qq = 1:length(vv)
                params_struct.(fld_nms{uu(pp)})(vv(qq)) = variable_prms(kk);
            end
        end
    end
end


% -> fit by maximizing the log-likelihood
function [params_best, loglike] = fitnll(fit_fcn, nn1_lst, ...
        params_0, params_LB, params_UB, fit_options)

    fun = @(xx) loglikefcn(xx, fit_fcn, nn1_lst);
    [params_best, nll_best] = fminsearchbnd(fun, ...
        params_0, params_LB, params_UB, fit_options);
    loglike = - nll_best;
end

% ->   negative summed log-likelihood
function nglglk = loglikefcn(pp, ff, nn)

    ypred = ff(pp);
    ypred(ypred == 0) = 1e-6;
    ypred(ypred == 1) = 1 - 1e-6;

    ll1 = log(ypred);
    ll0 = log(1.0 - ypred);

    ll_vect = nn(:,1) .* ll1 + nn(:,2) .* ll0;  % vector of log-likelihoods
    nglglk = - sum(ll_vect);      % minimize (-log) likelihood
end


% ------------------------------------------------------------------------


    
end