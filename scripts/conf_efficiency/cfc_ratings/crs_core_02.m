% CONFIDENCE RATING SCALE TOOLBOX  v0.1
%
% crs_core
%   This function generates simulation of data of confidence rating
%   for a Type 1 discrimination task. 
%   Its output data are in the format used by Maniscalco & Lau (2012).
%
%
% INPUT:
%   'model_params': model values parameters, as a structure
%       'sens_noise' : sensory (Type 1) sdtev of noise (0 = perfectly sensitive)
%       'sens_crit'  : sensory (Type 1) criterion
%       'conf_noise' : confidence (Type 2) sdtev of noise (0 = ideal)
%       'conf_boost' : fraction super-ideal (0 = ideal, 1 = super-ideal)
%       'conf_crit'  : confidence (Type 2) criterion
%
% OUTPUT: crs_rating_mat 
%    matrix where each line is [stim, resp, conf, prob(conf)]
%
%
% 19-AUG-2021 - pascal mamassian
% 16-FEB-2022 - pm: added continuous ratings

function crs_rating_mat = crs_core_02(crs_data_SRC, model_params)

% nb_trials = sum(sum(crs_data_SRC));
% nb_conf_levels = size(crs_data_SRC, 2) / 2;

% conf_levels = unique(crs_data_SRC(:, 3));
% nb_conf_levels = length(conf_levels);

stim_lst = unique(crs_data_SRC(:, 1));
stim_lst = sort(stim_lst);
stim_nb = length(stim_lst);


% -> PARAMETERS <-

% -> Type 1 parameters
sens_noise = model_params.sens_noise;  % sensory noise
sens_crit  = model_params.sens_crit;   % sensory criterion

% -> Type 2 parameters (compulsory)
conf_noise = model_params.conf_noise;  % confidence noise
conf_boost = model_params.conf_boost;  % confidence boost

% -> Type 2 parameters (optional)
% -> confidence criterion
if (any(strcmp(fieldnames(model_params), 'conf_crit')))
    conf_crit  = model_params.conf_crit;   
else
    conf_crit = 0.0;
end


% -> use discrete or continuous confidence judgments
if (any(strcmp(fieldnames(model_params), 'conf_continuous')))
    conf_continuous = model_params.conf_continuous;
else
    if (any(strcmp(fieldnames(model_params), 'conf_bnds')))
        % -> if only confidence bounds specified, assume discrete
        conf_continuous = 0;
    else
        % -> if nothing specified, assume continuous confidence on [0, 1]
        conf_continuous = 1;
        model_params.conf_range = [0, 1];
        model_params.conf_resol = 100;
        model_params.llo_gamma = 1.0;
        model_params.llo_p0 = 0.5;
    end
end


if (conf_continuous)
    % -> if continuous, the confidence label corresponds to the upper
    %    bound of the interval
    conf_range = model_params.conf_range;
    conf_bnd_min = conf_range(1);
    conf_bnd_max = conf_range(end);
    
    conf_levels_nb = model_params.conf_resol;
    
    % -> edges of confidence ratings
    conf_labels_step = (conf_bnd_max - conf_bnd_min) / conf_levels_nb;
    conf_bnd_edges = conf_bnd_min:conf_labels_step:conf_bnd_max;
    conf_bnd_labels = conf_bnd_edges(2:end);    
    
    % -> edges of confidence log-odds
%     lo_edges_step = 1.0 / conf_levels_nb;
%     lo_bnd_edges = 0:lo_edges_step:1;

    llo_gamma = model_params.llo_gamma;
    llo_p0 = model_params.llo_p0;

    % -> transformation rules
    %    1. log-odds <-> confidence ratings
    lo2conf = @(x_lo) conf_bnd_min + (conf_bnd_max - conf_bnd_min) .* x_lo;
    conf2lo = @(x_conf) (x_conf - conf_bnd_min) ./ (conf_bnd_max - conf_bnd_min);
    %    2. confidence prob <-> log-odds
    prob2lo = @(x_prob) crs_log_odds(x_prob, llo_gamma, llo_p0);
    lo2prob = @(x_lo) crs_log_odds(x_lo, 1/llo_gamma, llo_p0);
    %    3. confidence evidence <-> confidence prob
    evid2prob = @(x_evid) normcdf(x_evid);
    prob2evid = @(x_prob) norminv(x_prob);
    %    4. whole transformation: confidence evidence <-> confidence ratings
    evid2conf = @(x_evid) lo2conf(prob2lo(evid2prob(x_evid)));
    conf2evid = @(x_conf) prob2evid(lo2prob(conf2lo(x_conf)));
    
    evid_bnd_edges = conf2evid(conf_bnd_edges);
    
else
    prob_bnd_lst = model_params.conf_bnds;   % confidence boundaries
    prob_bnd_lst = sort(prob_bnd_lst);  % sort in increasing order
    conf_levels_nb = length(prob_bnd_lst) + 1;
%     conf_bnd_edges = [-Inf, conf_bnd_lst, +Inf];
    conf_bnd_labels = 1:conf_levels_nb;    

    % -> assume confidence boundaries are in probabilities
    if ((max(prob_bnd_lst) > 1.0) || (min(prob_bnd_lst) < 0.0))
        fprintf('Error: confidence boundaries are not probabilities\n');
        return;
    end
    
    % -> transformation: confidence evidence <-> confidence prob
%     evid2prob = @(x_evid) normcdf(x_evid);
    prob2evid = @(x_prob) norminv(x_prob);

    prob_bnd_edges = [0, prob_bnd_lst, 1];
    evid_bnd_edges = prob2evid(prob_bnd_edges);
end





sens_var_noise = sens_noise^2;
conf_var_noise = conf_noise^2;


rtg_resp0 = NaN(conf_levels_nb, 4);  % stim, resp=0, conf, prob(conf & resp | stim)
rtg_resp1 = NaN(conf_levels_nb, 4);  % stim, resp=1, conf, prob(conf & resp | stim)

nb_conds = stim_nb * 2 * conf_levels_nb;
nb_conds2 = 2 * conf_levels_nb;
crs_rating_mat = NaN(nb_conds, 4);

for ss = 1:stim_nb
    sens_mean = stim_lst(ss);

    lcl_distA_type1n2_mean = [sens_mean, ...
        (sens_mean - sens_crit - conf_crit)/sens_noise];
    lcl_distA_type1n2_covar = (1 - conf_boost) * sens_noise;
    lcl_distA_type1n2_var2 = (1 - conf_boost)^2 + conf_var_noise;
    lcl_distA_type1n2_var = [sens_var_noise,    lcl_distA_type1n2_covar; ...
                        lcl_distA_type1n2_covar, lcl_distA_type1n2_var2];

    % Make sure Sigma is a valid covariance matrix
%     [~,err] = cholcov(lcl_distA_type1n2_var,0);
%     if err ~= 0
%         fprintf('erreur\n');
%         lcl_distA_type1n2_var(2,2) = lcl_distA_type1n2_var(2,2) + 0.001;
%     end

%     % -> first do lowest confidence level
%     evid_upper_val = evid_bnd_edges(1);
%     pp_L_val = mvncdf([-Inf, -evid_upper_val], [sens_crit, +Inf], ...
%         lcl_distA_type1n2_mean, lcl_distA_type1n2_var);
%     pp_R_val = mvncdf([sens_crit, -Inf], [+Inf, evid_upper_val], ...
%         lcl_distA_type1n2_mean, lcl_distA_type1n2_var);
% 
%     rtg_resp0(1, :) = [sens_mean, 0, conf_bnd_labels(1), pp_L_val];
%     rtg_resp1(1, :) = [sens_mean, 1, conf_bnd_labels(1), pp_R_val];

    for rr = 1:conf_levels_nb
        % -> lower bound
        if (rr == 1)
            evid_lower_val = -Inf;
        else
            evid_lower_val = evid_bnd_edges(rr);
        end
        
        % -> upper bound
        if (rr == conf_levels_nb)  
            evid_upper_val = +Inf;
        else
            evid_upper_val = evid_bnd_edges(rr+1);
        end


        pp_L_val = mvncdf([-Inf, -evid_upper_val], [sens_crit, -evid_lower_val], ...
            lcl_distA_type1n2_mean, lcl_distA_type1n2_var);
        pp_R_val = mvncdf([sens_crit, evid_lower_val], [+Inf, evid_upper_val], ...
            lcl_distA_type1n2_mean, lcl_distA_type1n2_var);

        rtg_resp0(rr, :) = [sens_mean, 0, conf_bnd_labels(rr), pp_L_val];
        rtg_resp1(rr, :) = [sens_mean, 1, conf_bnd_labels(rr), pp_R_val];
    end

    rng = (ss - 1)*nb_conds2 + (1:nb_conds2);
    crs_rating_mat(rng, :) = [rtg_resp0; rtg_resp1];
end


end

% -> THE END