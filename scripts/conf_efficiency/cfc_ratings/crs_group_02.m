% CONFIDENCE RATING SCALE TOOLBOX  v0.1
%
% crs_group
%
% 26-AUG-2021 - pascal mamassian
% 16-FEB-2022 - pm: added continuous ratings

function grouped_data_SRC = crs_group_02(raw_data_SRC, varargin)

    % -> default optional arguments
    dflt_is_continuous    = false;  % continuous (T) or discrete (F) rating scale
    dflt_conf_range       = [0, 1];
    dflt_conf_resol       = 100;

    % -> parse all arguments
    ip = inputParser;
    ip.StructExpand = false;
    addRequired(ip, 'raw_data_SRC', @isnumeric);
    addParameter(ip, 'is_continuous', dflt_is_continuous, @islogical);
    addParameter(ip, 'confidence_range', dflt_conf_range, @isnumeric);
    addParameter(ip, 'confidence_resolution', dflt_conf_resol, @isnumeric);
    parse(ip, raw_data_SRC, varargin{:});
    conf_continuous = ip.Results.is_continuous;
    conf_range = ip.Results.confidence_range;
    conf_resol = ip.Results.confidence_resolution;

    col_stim = 1;
    col_resp = 2;
    col_conf_levl = 3;
    col_conf_prob = 4;  % prob(conf|stim) or count
    
    % -> if raw data have only 3 columns, add a 4th one
    if (size(raw_data_SRC, 2) < col_conf_prob)
        nb_lines = size(raw_data_SRC, 1);
        raw_data_SRC = [raw_data_SRC, ones(nb_lines, 1)];
    end

    [stim_lst, ~, stim_ic] = unique(raw_data_SRC(:, col_stim));
    stim_nb = length(stim_lst);

    [resp_lst, ~, resp_ic] = unique(raw_data_SRC(:, col_resp));
    resp_nb = length(resp_lst);

    if (conf_continuous)
        conf_bnd_min = conf_range(1);
        conf_bnd_max = conf_range(end);

%         conf_levels_nb = conf_resol;

        % -> edges of confidence ratings
        conf_labels_step = (conf_bnd_max - conf_bnd_min) / conf_resol;
        conf_bnd_edges = conf_bnd_min:conf_labels_step:conf_bnd_max;
        conf_bnd_labels = conf_bnd_edges(2:end);
    else
        [conf_bnd_labels, ~, rating_ic] = unique(raw_data_SRC(:, col_conf_levl));
    end
    conf_levels_nb = length(conf_bnd_labels);
%     rating_bnds_nb = conf_levels_nb - 1;

    nb_conds = stim_nb * resp_nb * conf_levels_nb;

    grouped_data_SRC = zeros(nb_conds, 4);
    
    kk = 0;
    for ss = 1:stim_nb
        stim_val = stim_lst(ss);
        
        for rr = 1:resp_nb
            resp_val = resp_lst(rr);

            for cc = 1:conf_levels_nb
                conf_val = conf_bnd_labels(cc);

                % -> search for number of confidence trials in this category
                if (conf_continuous)
                    conf_low = conf_bnd_edges(cc);
                    conf_hig = conf_bnd_edges(cc+1);
                    ratings_inds = ((raw_data_SRC(:, col_conf_levl) > conf_low) & ...
                        (raw_data_SRC(:, col_conf_levl) <= conf_hig));
                    if (cc == 1)
                        ratings_inds = (ratings_inds | ...
                            (raw_data_SRC(:, col_conf_levl) == conf_low));
                    end
                    conf_num = sum(raw_data_SRC((stim_ic==ss) & (resp_ic==rr) & ...
                        ratings_inds, col_conf_prob));
                else
                    conf_num = sum(raw_data_SRC((stim_ic==ss) & (resp_ic==rr) & ...
                        (rating_ic==cc), col_conf_prob));
                end

                kk = kk + 1;

                grouped_data_SRC(kk, col_stim) = stim_val;
                grouped_data_SRC(kk, col_resp) = resp_val;
                grouped_data_SRC(kk, col_conf_levl) = conf_val;
                grouped_data_SRC(kk, col_conf_prob) = conf_num;
            
            end
        end
        
    end
    
end