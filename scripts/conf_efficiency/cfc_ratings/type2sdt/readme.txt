http://www.columbia.edu/~bsm2105/typMATLAB files for conducting type 2 signal detection theory analysis

Last updated: 9/24/10

This analysis is intended to quantify metacognitive sensitivity (i.e. the efficacy with which confidence ratings discriminate between correct and incorrect judgments) in a signal detection theory framework. A central idea is that primary task performance can influence metacognitive sensitivity, and it is informative to take this influence into account.

Preliminary description of the methodology can be found here:

Rounis, E., Maniscalco, B., Rothwell, J. C., Passingham, R. E., & Lau, H. (2010). Theta-burst transcranial magnetic stimulation to the prefrontal cortex impairs metacognitive visual awareness. Cognitive Neuroscience, 1(3), 165. doi:10.1080/17588921003632529

A more comprehensive treatment of conceptual, computational, and empirical issues regarding the method is available as an unpublished manuscript here. Comments are welcome.

If you use the analysis files below, please reference the Cognitive Neuroscience paper and this website.

Brian Maniscalco
brian _at_ psych -dot- columbia -dot- edu

 

 

Below are two sets of functions for conducting type 2 SDT analysis. One set uses maximum likelihood estimation (MLE), and the other works by minimizing the sum of squared errors. MLE methods are desirable for estimating the SDT parameter s, since least-squares methods ignore the variance in FAR data. The MLE functions are also considerably faster. However, they require use of Matlab's optimization toolbox.
Maximum likelihood estimation (MLE) - requires optimization toolbox

fit_meta_d_MLE.m

This function estimates meta-da as well as basic type 1 SDT parameters. It takes as input a count of the number of times the subject used each available response for each stimulus type, as well as an estimate of the SDT parameter s. See the comments in the help section for full details.

type2_SDT_MLE.m (requires fit_meta_d_MLE.m and SDT_MLE_fit.m)
9/24/10 update: fixed input bug for (nR_S1, nR_S2) input

This is a wrap-around function you can use with fit_meta_d_MLE. This function handles different kinds of input formats, applies corrections to data to account for missing data cells, and calls the function SDT_MLE_fit to get an MLE estimate of the SDT parameter s for use with fit_meta_d_MLE.

SDT_MLE_fit.m

This function uses an MLE method to fit the parameters of an unequal variance SDT model to behavioral data.

Minimizing sum of squared errors
fit_meta_d_SSE.m

This function estimates meta-d' using response-conditional type 2 HRs and FARs and the empirical type 1 criterion c' as input. See the comments in the help section for full details.

type2_SDT_SSE.m (requires fit_meta_d_SSE.m)

This is a wrap-around function you can use to pass in raw behavioral data and have it processed appropriately for input into fit_meta_d_SSE. It also provides a basic type 1 SDT analysis and a comparison between type 1 d' and meta-d'. In total it's more convenient to use type2_SDT_SSE than to use fit_meta_d_SSE directly, but using fit_meta_d_SSE directly gives you more control over the fine details of how you estimate meta-d'. See comments in the help section for full details. e2sdt/