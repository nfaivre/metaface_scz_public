% CONFIDENCE RATING SCALE TOOLBOX  v0.1
%
% crs_plot
%   This is a collection of plot functions for confidence rating scale.
%
% INPUT:
%   'grouped_data': grouped data per (s1, s2, r1, r2):
%       1st col: stimulus intensity interval 1
%       2nd col: stimulus intensity interval 2
%       3rd col: perceptual decision interval 1
%       4th col: perceptual decision interval 2
%       5th col: nb of confidence choices for interval 1
%       6th col: nb of confidence choices for interval 2
%       7th col: stimulus task for interval 1
%       8th col: stimulus task for interval 2
%
% POSSIBLE PARAMETERS:
%   'human_model': plot fraction chosen for human against a model provided
%                  as argument
%
%   'choi_by_resp': plot matrix of choices for pairs of stimulus strengths
%
%   'zscore': plot z-scores instead of probabilities
%
%   'type1_psychometric': plot psychometric functions for percepts
%       if there is an argument 'arg', plot model data in 'arg'
%           format (n1, n0) for each 'knd'
%
%   'type1_psychometric_all': plot psychometric functions for percepts
%           for each interval and task
%
%   'type2_psychometric': plot psychometric functions for confidence choices
%       if there is an argument 'arg', plot model data in 'arg'
%           format 'choice for intrvl 1' for each (s1, s2, perc, tsk)
%
%   'type2_residuals': plot confidence choice residuals
%
%
% OUTPUT:
%   'plot_data' = struct
%        (vector of struct if number of unique tasks > 1)
%     plot_data(task_no).task               task number
%     plot_data(task_no).sensory_strength	list of stimuli
%     plot_data(task_no).unsorted_prob      Type1 responses (unsorted)
%     plot_data(task_no).chosen_prob        Type1 responses for confidence chosen
%     plot_data(task_no).declined_prob      Type1 responses for confidence declined
%     plot_data(task_no).unsorted_count     nb. of Type1 responses (unsorted)
%     plot_data(task_no).chosen_count       nb. of Type1 responses for confidence chosen
%     plot_data(task_no).declined_count     nb. of Type1 responses for confidence declined
% 
%
%
% 19-AUG-2021 - pascal mamassian
% 20-FEB-2022 - pm: added continuous ratings

function plot_data = crs_plot_02(crs_data_SRC, varargin)

    % -> default optional arguments
    dflt_human_model         = [];	   % human against model choice
    dflt_choi_by_resp        = false;  % choice by response
    dflt_zscore              = false;  % plot z-scores instead of probabilities
    dflt_t1_psychometric     = false;     % type-1 psychometric functions
    dflt_t2_roc     = [];     % type-2 ROC

    % -> parse all arguments
    ip = inputParser;
    addRequired(ip, 'crs_data_SRC', @isnumeric);
    addParameter(ip, 'human_model', dflt_human_model, @isnumeric);
    addParameter(ip, 'choi_by_resp', dflt_choi_by_resp, @islogical);
    addParameter(ip, 'zscore', dflt_zscore, @islogical);
    addParameter(ip, 'type1_psychometric', dflt_t1_psychometric);
    addParameter(ip, 'type2_roc', dflt_t2_roc);

    parse(ip, crs_data_SRC, varargin{:});
    plot_human_model = ip.Results.human_model;
    plot_choi_by_resp = ip.Results.choi_by_resp;
    plot_zscore = ip.Results.zscore;
    plot_t1_psychometric = ip.Results.type1_psychometric;
    plot_t2_roc = ip.Results.type2_roc;

    % -> prepare output variables
    plot_data = struct;

    
    col_stim = 1;
    col_resp = 2;
    col_conf_levl = 3;
    col_conf_prob = 4;

    stim_lst = unique(crs_data_SRC(:, col_stim));
    stim_nb = length(stim_lst);

    resp_lst = unique(crs_data_SRC(:, col_resp));
    resp_nb = length(resp_lst);

    rating_inds = unique(crs_data_SRC(:, col_conf_levl));
    conf_levels_nb = length(rating_inds);
    rating_bnds_nb = conf_levels_nb - 1;

    
    

% -> ********************************************** <-

mtlb_colors = [...
    0.0000, 0.4470, 0.7410;
    0.8500, 0.3250, 0.0980;
    0.9290, 0.6940, 0.1250;
    0.4940, 0.1840, 0.5560;
    0.4660, 0.6740, 0.1880;
    0.3010, 0.7450, 0.9330;
    0.6350, 0.0780, 0.1840];

% -> rating criteria for fits
% fit_rating_crit_lst = -4.0:0.1:4.0;
% fit_rating_crit_nb = length(fit_rating_crit_lst);

% -> data where conf_prob is per 'stim' & 'resp' (rather than just per 'stim')
% rtg_mat = NaN(resp_nb*conf_levels_nb, 4);
% rtg_mat(:, 1:3) = crs_data_SRC(1:(resp_nb*conf_levels_nb), 1:3);

hum_roc_lst = src_to_roc(crs_data_SRC);

% rtg_mat = crs_data_SRC;
% 
% rtg_roc_lst = NaN(stim_nb*rating_bnds_nb, 4);
% rng_all = 1:(stim_nb * conf_levels_nb * resp_nb);
% 
% for ss = 1:stim_nb
%     stim_val = stim_lst(ss);
% 
%     % -> prob of each response
%     lcl_left_inds = ((crs_data_SRC(:,col_stim) == stim_val) & (crs_data_SRC(:,col_resp) == 0));
%     lcl_righ_inds = ((crs_data_SRC(:,col_stim) == stim_val) & (crs_data_SRC(:,col_resp) == 1));
% 
%     lcl_resp_L = sum(crs_data_SRC(lcl_left_inds, col_conf_prob));
%     lcl_resp_R = sum(crs_data_SRC(lcl_righ_inds, col_conf_prob));
% 
%     rtg_mat(lcl_left_inds, col_conf_prob) = crs_data_SRC(lcl_left_inds, col_conf_prob) / lcl_resp_L;
%     rtg_mat(lcl_righ_inds, col_conf_prob) = crs_data_SRC(lcl_righ_inds, col_conf_prob) / lcl_resp_R;
% 
% 
% %             resp_cons = (stim_val > sens_crit);  % consistent response
% %             resp_inco = 1 - resp_cons;  % inconsistent response
%     resp_corr = (ss == stim_nb);  % correct response
%     resp_inco = 1 - resp_corr;    % incorrect response
% 
%     lcl_inco_inds = ((rtg_mat(:,col_stim) == stim_val) & (rtg_mat(:,col_resp) == resp_inco));
%     lcl_cons_inds = ((rtg_mat(:,col_stim) == stim_val) & (rtg_mat(:,col_resp) == resp_corr));
%     rng_inco = rng_all(lcl_inco_inds);
%     rng_cons = rng_all(lcl_cons_inds);
%     for rr = 1:rating_bnds_nb
%         lcl_type2_cr  = sum(rtg_mat(rng_inco(1:rr), col_conf_prob));
%         lcl_type2_miss = sum(rtg_mat(rng_cons(1:rr), col_conf_prob));
%         lcl_type2_fa  = 1 - lcl_type2_cr;
%         lcl_type2_hit = 1 - lcl_type2_miss;
% 
%         rtg_roc_lst((ss-1)*rating_bnds_nb + rr, :) = ...
%             [stim_val, rating_inds(rr), lcl_type2_hit, lcl_type2_fa];
%     end
% 
% end


% -> ********************************************** <-
% -> plot Type 2 ROC
goaheadandplot = 0;
if (islogical(plot_t2_roc))
    if (plot_t2_roc)
        goaheadandplot = 1;
    end
elseif (ismatrix(plot_t2_roc))
    
    % -> a struct is also an array, but of size 1
    if (size(plot_t2_roc, 1) == 1)
        goaheadandplot = 3;
        model_params = plot_t2_roc;
        rating_mat = crs_core_02(crs_data_SRC, model_params);
    else    
        goaheadandplot = 2;
        rating_mat = plot_t2_roc;
    end
        
end

if (goaheadandplot)
% if (plot_t2_roc)
    figure('Name', 'Type 2 ROC');
    axes('Position',[0.15 0.17 0.6 0.8]);
    set(gca, 'FontName', 'Arial'); set(gca, 'FontSize', 24);
    hold on;
    line([0, 1], [0, 1], 'LineStyle', '-', 'Color', [0.5 0.5 0.5], 'LineWidth', 2);

    leg_nb = NaN(1, stim_nb);
    leg_label = strings(1, stim_nb);

%     for mm = 1:stim_nb
    for mm = 1:1
        stim_val = stim_lst(mm);
        leg_label(mm) = sprintf('stim: %5.2f', stim_val);
%         my_col = mtlb_colors(mm, :);
        my_col = [0, 0, 0];
        
        human_rng_inds = (mm-1)*rating_bnds_nb + (1:rating_bnds_nb);

        human_hit2_lst = hum_roc_lst(human_rng_inds, 3);
        human_fa2_lst  = hum_roc_lst(human_rng_inds, 4);

        human_hit2_lst2 = [1.0; human_hit2_lst; 0.0];
        human_fa2_lst2 = [1.0; human_fa2_lst; 0.0];
        
        if (goaheadandplot == 1)
            plot(human_fa2_lst2, human_hit2_lst2, '-', ...
                'Color', my_col, 'LineWidth', 3);
        elseif ((goaheadandplot == 2) || (goaheadandplot == 3))
            mdl_conf_levels_nb = size(rating_mat,1) / stim_nb / resp_nb;
            mdl_rating_bnds_nb = mdl_conf_levels_nb - 1;
            model_rng_inds = (mm-1)*mdl_rating_bnds_nb + (1:mdl_rating_bnds_nb);
            
            mdl_roc_lst = src_to_roc(rating_mat);
            model_hit2_lst = mdl_roc_lst(model_rng_inds, 3);
            model_fa2_lst  = mdl_roc_lst(model_rng_inds, 4);

            model_hit2_lst2 = [1.0; model_hit2_lst; 0.0];
            model_fa2_lst2 = [1.0; model_fa2_lst; 0.0];
            
%             plot(human_fa2_lst2, human_hit2_lst2, '--', ...
%                 'Color', my_col, 'LineWidth', 2);
            plot(model_fa2_lst2, model_hit2_lst2, '-', ...
                'Color', my_col, 'LineWidth', 3);
        end
        
        leg_nb(mm) = plot(human_fa2_lst, human_hit2_lst, 'o', ...
            'MarkerSize', 16, 'MarkerFaceColor', [1 1 1], ...
            'Color', my_col, 'LineWidth', 3);
    end

%     legend(leg_nb, leg_label, 'Location', 'southeast');
    axis('square');
%     xlabel('p(high conf | incorrect)', 'FontSize', 26);
%     ylabel('p(high conf | correct)', 'FontSize', 26);
    xlabel('p(high conf | self-inconsistent)', 'FontSize', 26);
    ylabel('p(high conf | self-consistent)', 'FontSize', 26);
end

% -> ********************************************** <-
% -> nested functions

% -> convert SRC format to ROC format 
function data_ROC = src_to_roc(data_SRC)

    rtg_mat = data_SRC;
    
    nb_conds = size(data_SRC, 1);
    
    my_rating_inds = unique(data_SRC(:, col_conf_levl));
    my_conf_levels_nb = length(my_rating_inds);
    my_rating_bnds_nb = my_conf_levels_nb - 1;

    data_ROC = NaN(stim_nb*my_rating_bnds_nb, 4);
    rng_all = 1:nb_conds;

    for ss = 1:stim_nb
        lcl_stim_val = stim_lst(ss);

        % -> prob of each response
        lcl_left_inds = ((data_SRC(:,col_stim) == lcl_stim_val) & (data_SRC(:,col_resp) == 0));
        lcl_righ_inds = ((data_SRC(:,col_stim) == lcl_stim_val) & (data_SRC(:,col_resp) == 1));

        lcl_resp_L = sum(data_SRC(lcl_left_inds, col_conf_prob));
        lcl_resp_R = sum(data_SRC(lcl_righ_inds, col_conf_prob));

        rtg_mat(lcl_left_inds, col_conf_prob) = data_SRC(lcl_left_inds, col_conf_prob) / lcl_resp_L;
        rtg_mat(lcl_righ_inds, col_conf_prob) = data_SRC(lcl_righ_inds, col_conf_prob) / lcl_resp_R;


%         resp_cons = (stim_val > sens_crit);  % consistent response
%         resp_inco = 1 - resp_cons;  % inconsistent response
        resp_corr = (ss == stim_nb);  % correct response
        resp_inco = 1 - resp_corr;    % incorrect response

        lcl_inco_inds = ((rtg_mat(:,col_stim) == lcl_stim_val) & (rtg_mat(:,col_resp) == resp_inco));
        lcl_cons_inds = ((rtg_mat(:,col_stim) == lcl_stim_val) & (rtg_mat(:,col_resp) == resp_corr));
        rng_inco = rng_all(lcl_inco_inds);
        rng_cons = rng_all(lcl_cons_inds);
        for rr = 1:my_rating_bnds_nb
            lcl_type2_cr  = sum(rtg_mat(rng_inco(1:rr), col_conf_prob));
            lcl_type2_miss = sum(rtg_mat(rng_cons(1:rr), col_conf_prob));
            lcl_type2_fa  = 1 - lcl_type2_cr;
            lcl_type2_hit = 1 - lcl_type2_miss;

            data_ROC((ss-1)*my_rating_bnds_nb + rr, :) = ...
                [lcl_stim_val, my_rating_inds(rr), lcl_type2_hit, lcl_type2_fa];
        end

    end
end
    
end

