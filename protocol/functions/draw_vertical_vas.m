% draw the VAS in PTB
function drawVAS_ptb_TOJ(VASrange)

global win
global P

lineWidth = 2;
yup       = 10;
VASlength = max(VASrange) - min(VASrange);


stringTop  = 'S�r d''avoir bon';
stringBottom = 'S�r d''avoir faux';
stringMiddle = 'R�ponse au hasard';

vertLines_y = P.midY + VASlength/2 * [-1 -1 0 0 1 1];

vertLines_x = P.midX + yup * repmat([1 -1], 1, length(unique(vertLines_y)));
middleVerticals = find(vertLines_y == P.midY);
vertLines_x(middleVerticals(1)) = P.midX + yup * 2;
vertLines_x(middleVerticals(2)) = P.midX + yup * -2;                    %make middle line a bit longer
horizLine_y = VASrange;
horizLine_x = P.midX + [0          0];                                  %this is a little bit ridiculous but will depend on the size of the * used as a cursor

%     Screen('TextSize', win, 16);
Screen('DrawLines', win, [vertLines_x; vertLines_y], lineWidth);
Screen('DrawLines', win, [horizLine_x; horizLine_y], lineWidth);
%     DrawFormattedText(win, 'How confident?', P.midX - 60,'center');
DrawFormattedText(win, stringBottom, P.midX + 30, VASrange(2),[230 20 0]);
DrawFormattedText(win, stringTop,    P.midX + 30, VASrange(1),[20 230 0]);
DrawFormattedText(win, stringMiddle, P.midX + 30, P.midY +10, [0 0 0]);

end

