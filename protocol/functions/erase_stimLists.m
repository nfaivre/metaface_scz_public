savePath = 'data/test/';
filenames = struct2cell(dir(savePath));  

for i = 1:7
    if i ~= 5
        P.suj.number = ['000' num2str(i)];
        file_idx = contains(filenames(1,:),['suj' P.suj.number '_session1']);
        fileSession1 = char(filenames(1, file_idx));
        load([savePath, fileSession1]);
        P.stimList1 = [];
        P.stimList2 = [];
        save([savePath fileSession1], 'P');
    end
end



