% By Shuo Wang Oct 28 2013

fnSendTTL(isSendTrigger,pp_address,TTL.PAUSE);
myEyelink( 'Message', 'PAUSE', DUMMY_EYELINK);

Screen('FillRect',window,gray);

Screen('TextSize',window,35);
Screen('DrawText',window,'Pausing...',fixX-100,fixY-100,white);
Screen('DrawText',window,'Press Space Bar to Start',fixX-300,fixY,white);
Screen('Flip',window);
Screen('TextSize',window,50);

WaitSecs(2);

while 1
    [ keyIsDown2, timeSecs2, keyCode2 ] = KbCheck;
    
    if keyIsDown2 && keyCode2(KbName('SPACE'))
        break;
    end
end

fnSendTTL(isSendTrigger,pp_address,TTL.RESUME);
myEyelink( 'Message', 'RESUME', DUMMY_EYELINK);

iT(iTrial).isPaused = 1;