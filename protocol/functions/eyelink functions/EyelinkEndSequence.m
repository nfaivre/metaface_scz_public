% EyelinkEndSequence

function EyelinkEndSequence( edfName, sbjName, DUMMY_EYELINK )

if DUMMY_EYELINK
    return;
end

WaitSecs(0.1);
Eyelink('StopRecording');
Eyelink('Command', 'set_idle_mode');
WaitSecs(0.5);
Eyelink('CloseFile');

% transfer EyeLink result to same directory
try
    fprintf('Receiving data file ''%s''\n', edfName );
    status = myEyelink( 'ReceiveFile', edfName, ['../data/edf/' edfName], DUMMY_EYELINK);
    if status > 0
        fprintf('ReceiveFile status %d\n', status);
    end
    if 2==exist(edfName, 'file')
        fprintf('Data file ''%s'' can be found in ''%s''\n', edfName, pwd );
    end
catch %#ok<*CTCH>
    fprintf('Problem receiving data file ''%s''\n', edfName );
end




%if status~=0
%  error('File transfer from EL failed, check!');
%end

% shutdown EyeLink
myEyelink( 'ShutDown', DUMMY_EYELINK );
return
