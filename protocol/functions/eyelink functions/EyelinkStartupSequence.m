% EyelinkStartupSequence

function el = EyelinkStartupSequence( win, edfName, gray, white, DUMMY_EYELINK)
% Eyelink startup sequence, initialize through drift correction, hide cursor
HideCursor;

% Initialization of the connection with the Eyelink Gazetracker.
if myEyelink('Init', DUMMY_EYELINK) ~= 1;
    error( 'fail to init EyeLink' );
end;

% eye-tracking defaults
el = myEyelink('InitDefault', win,DUMMY_EYELINK);
el.backgroundcolour = gray;
el.foregroundcolour = white;

% make sure that we get gaze data from the Eyelink
myEyelink( 'Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA',DUMMY_EYELINK);

% start eyelink, set standard parameters
myEyelink( 'Openfile', edfName,DUMMY_EYELINK);

% do calibration
Screen('Flip', win);
myEyelink( 'DoTrackerSetup', el,DUMMY_EYELINK);

% do a final check of calibration using driftcorrection
myEyelink( 'DoDriftCorrection', el,DUMMY_EYELINK);