function [varargout] = myEyelink( varargin)

command = varargin{1};

% show message
if varargin{end}
%     fprintf( 'dummy function of Eyelink(%s)\n', varargin{1} );
    
    % dispatch by command
    switch ( lower(command) )
        case 'init'
            varargout{1} = 1;
        case 'initdefault'
            varargout{1} = [];
        otherwise
            varargout{1} = 0;
    end
    
    return;
end

fprintf( 'function of Eyelink(%s)\n', varargin{1} );
% dispatch by command
switch ( lower(command) )
    case 'init'
        %myEyelink( 'Init' );
        varargout{1} = EyelinkInit();
        
    case 'initdefault'
        % myEyelink( 'InitDefault', window);
        varargout{1} = EyelinkInitDefaults( varargin{2} );
        
    case 'dotrackersetup'
        % myEyelink( 'DoTrackerSetup', el);
        EyelinkDoTrackerSetup( varargin{2} );
        
    case 'dodriftcorrection'
        % myEyelink( 'DoDriftCorrection', el);
        EyelinkDoDriftCorrection( varargin{2} );
        
    case {'stoprecording', 'shutdown'}
        % functions which don't return output
        Eyelink( varargin{1} );
        
    otherwise
        if nargin == 2 % modified by Wang Shuo for DUMMY_EYELINK
            varargout{1} = Eyelink( varargin{1} );
        elseif nargin == 3
            varargout{1} = Eyelink( varargin{1}, varargin{2} );
        elseif nargin == 4
            varargout{1} = Eyelink( varargin{1}, varargin{2}, varargin{3});
        elseif nargin == 5
            varargout{1} = Eyelink( varargin{1}, varargin{2}, varargin{3}, varargin{4});
        else
            error('too many input for myEyelink\n');
        end
end
