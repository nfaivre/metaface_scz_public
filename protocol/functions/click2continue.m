function quit = click2continue(P,win)

quit = 0;
clicks = [];

while 1
    
    Screen('FrameOval',win,[0 0 0],P.design.clickReset, 2);
    [mouseX, mouseY, mouseButtons] = GetMouse;   
    
    Screen('DrawText', win, '+',  mouseX, mouseY, 0);
    Screen('Flip',win); % display
    
    % read the keyboard to quit if needed
    [keyIsDown, Secs, keyCode] = KbCheck;
    if keyCode(P.design.quitkey)
        quit=1;
        break   
    elseif mouseY > P.design.clickReset(2) && mouseY < P.design.clickReset(4) && ...
           mouseX > P.design.clickReset(1) && mouseX < P.design.clickReset(3)% No Response
        clicks = [clicks mouseButtons(1)];
        sumClicks = sum(clicks);
        if sumClicks > 1 && mouseButtons(1) == 0 % Released mouse click
           break
        end
    end
end

Screen('FrameOval',win,[0 P.depth 0],P.design.clickReset,2);
Screen('Flip', win);

end