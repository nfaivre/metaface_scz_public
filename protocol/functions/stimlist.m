function P = stimlist(P, Nstim1, Nstim2) 

Nstim = Nstim1 + Nstim2; %% total number of stimuli (session1 + session2) 

% Check that 'Nstim' is even
if mod(Nstim,2)==1
Nstim = Nstim+1;
end 

%% List of all stimuli needed for the experiment in a random order
rng shuffle
stimListh = randperm(Nstim/2); 
stimListf = randperm(Nstim/2); 

%% Split into 2 non-overlapping lists for each session
stimListh1 = stimListh(1 : Nstim1/2); %Random stim list session1
stimListf1 = stimListf(1 : Nstim1/2); %Random stim list session1

stimListh2 = stimListh(Nstim1/2+1 : Nstim/2); %Random stim list session2
stimListf2 = stimListf(Nstim1/2+1 : Nstim/2); %Random stim list session2

% saves lists in P struct
P.stimList1 = [strcat("h", string(stimListh1)); strcat("f", string(stimListf1))];
P.stimList2 = [strcat("h", string(stimListh2)); strcat("f", string(stimListf2))];

end
