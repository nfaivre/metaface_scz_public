function WaitForMouseRelease()

clicks = [];
while 1
    [mouseX,mouseY,mouseButtons] = GetMouse;
    clicks = [clicks mouseButtons(1)];
    sumClicks = sum(clicks);
    if sumClicks > 1 && mouseButtons(1) == 0  
        break
    end
end
end