function P = session2(P,screen,path_stimsM)
%UNTITLED3 Summary of this function goes here
%test

global win
global trigQ1 trigM1 trigR1
global trigQ2 trigM2 trigR2


%% Load data from session 1 
filenames = struct2cell(dir('../data'));  
file_idx = contains(filenames(1,:),['suj' num2str(P.suj.number) '_session1_all']);
fileSession1 = char(filenames(1,file_idx));

load([P.savepath fileSession1]);
     
%% Parameters settings %%
EL = 0;
quit = 0;
trialcount = 1;
dcount = 1;
idx = 1; % index of stimList2
P.suj.session = 2;
session = 2;
CrossWidth = Screen('TextBounds', win, '+');
%% RS Main eyelink startup sequence: from initialize to drift-correction
edfName  = ['m' num2str(P.suj.number) '.edf'];

if EL
    el = EyelinkStartupSequence(win, edfName, screen.colbackground, 255, DUMMY_EYELINK);
    status = myEyelink( 'StartRecording',DUMMY_EYELINK);
    if ~DUMMY_EYELINK
        eye_used = Eyelink('eyeavailable'); % get eye that's tracked
        % 0 for left, 1 for right and 2 for both
        if eye_used == 2 % when both eyes are tracked, use right eye
            eye_used = 1;
        end
    else
        eye_used = -1;
    end
end

% initialize P columns in the right order
P.session2(1,1).type = '';
P.session2(1,1).ac = 0;
P.session2(1,1).sigLevel = 0;
P.session2(1,1).stimamp = P.initialamp;
P.session2(1,1).paradigm = 'training';
   
%% Randomizing trials
Ntrials2 = P.design.n * P.design.nblocs; 
trialFeat = randtrials(session,Ntrials2);

colorComb  = nchoosek(1:4,2);% all combinations of indexes for the 2 red contexts among 4 contexts
color0     = repmat('b',1,4);
genderComb = nchoosek(1:4,2);% all combinations of indexes for the 2 women faces among 4 stims
gender0    = 1:4;

%% Welcome message
instruction = ['Bonjour et merci pour votre pers�v�rance! \n\nVous allez r�aliser les m�mes exercices que dans la session pr�c�dente, � un d�tail pr�s: \n\nD�sormais vous allez devoir estimer la confiance associ�e � chacune de vos r�ponses. \n\nCliquer pour une d�monstration.'];
DrawFormattedText(win, instruction, 'center', 'center', WhiteIndex(win));
Screen('Flip', win); % Update the display to show the instruction text
HideCursor();
WaitForMouseRelease();
Screen('Flip', win, screen.colbackground);  % Clear screen to background color 


%% Training with confidence scale
Screen('TextFont', win)
P.paradigm = 'training';
train = 1;
training(P, screen, train)
train = 0; 

filename = ['suj' num2str(P.suj.number) '_session2_all_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat'];

P = initexp(P);
%% START MAIN EXPERIMENT
P.paradigm = 'main';
for b = 1: P.design.nblocs
    fprintf(['Now running block ' num2str(b) '\n']);
    blocname = ['suj' num2str(P.suj.number) '_session2_bloc' int2str(b) '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat'];																															  

% run through the n trials
    for trial = 1:P.design.n  % loop through the n trials          
        P.session2(b,trial).blocNumber = b;
        P.session2(b,trial).bloc = b + P.design.nMemblocs;       
        P.session2(b,trial).session = session;
        P.session2(b,trial).type = char(trialFeat(1,trialcount)); % Determine the task
        P.session2(b,trial).pseudolag = NaN;
        P.session2(b,trial).perf2match = NaN;
        P.session2(b,trial).colors = 'none';
        P.session2(b,trial).faces = 'none';
        P.session2(b,trial).lag = NaN;
        
        type1 = P.session2(b,trial).type;       
        if ismember(type1,['r','f'])            
            P.session2(b,trial).paradigm = P.paradigm;
            P.session2(b,trial).paradigm_idx = 4;
            P.session2(b,trial).stimamp = -1; %this field stores only perceptual intensities. Put to -1 in memory trials, for matrix dimensions purpose
        end

        
        fprintf('bloc %i, trial %i, task = %s\n',b, trial, type1);

%         trigstart = 101 + (trial-1); % trigger index trial onset

%% Prepare encoding phase for familiarity and recollection
        if (type1 == 'r' || type1 == 'f')
            % Pick stim numbers           
            listFaces = reshape(transpose(P.stimList2(:,idx:idx+1)),1,[]);% Extract 4 stim indexes from stimList2 => Stims to encode
            if type1 == 'r'             
                idx = idx + 2;  
            elseif type1 == 'f'
                newFaces = reshape(P.stimList2(:,idx+2),1,[]);% Extract 2 stim indexes from stimList2 => New stims for the test phase
                idx = idx + 3;  % add a new face             
            end

            % Determine context colors
            listColors = color0; %initialize
            col_comb_idx = str2double(trialFeat(3, trialcount));
            listColors(colorComb(col_comb_idx,:)) = 'r';

            % Determine gender order 
            genders = 'ffff';
            gen_comb_idx = str2double(trialFeat(4, trialcount));
            h_idx = genderComb(gen_comb_idx,:);
            genders(h_idx) = 'h';
            f_idx = gender0;
            f_idx(h_idx)=[];
            listTmp = strings(1,4);
            listTmp(h_idx) = listFaces(1:2);
            listTmp(f_idx) = listFaces(3:4);
            listFaces = listTmp;
            
            P.session2(b,trial).genders = genders;
            P.session2(b,trial).colors = listColors;
            P.session2(b,trial).faces = convertContainedStringsToChars(listFaces);

    %% Encoding phase : Display a list of faces to memorize
    % loop through nfaces
        for stim = 1:P.design.nfaces % Load stimuli
            KbCheck;% initialize KbCheck and variables
            online =1;
            %Create masks from loaded templates
            template = strcat('template_',char(listFaces(stim)),'.tif');

            %load stimuli and color
            faceStim = strcat('SHINEd_',char(listFaces(stim)),'.tif');
            colorStim = char(listColors(stim));
            tex(stim) = makeTextures(path_stimsM,faceStim,colorStim,template,online);            
        end
        
        for stim = 1:P.design.nfaces % Display stimuli
            % Draw texture image to backbuffer.
            Screen('DrawTexture', win, tex(stim), [], P.design.centeredRect); % big rectangle 
            Screen('Flip', win);
            WaitSecs(P.design.stimDuration)

            Screen('FillRect', win, screen.colbackground);% Clear screen to background color
            Screen('Flip', win); % Flip to the screen
            WaitSecs(P.design.ISI);% Inter-stimulus Interval
        end
        
        Screen('Close');
     
%% Test phase: Display the face stimulus, depending on the task

            PsychHID('KbQueueStart',P.index);  
            lagLevel = str2double(trialFeat(2,trialcount)); % Determine the Lag
            idx_stim = (P.design.nfaces+1)- lagLevel;  % and corresponding stimulus index in the list of 4
            P.session2(b,trial).lag = lagLevel;
            P.session2(b,trial).sigLevel = idx_stim;

            if type1 == 'f' %Familiarity task
                taskInstruction = 'Déjà vu?';    
                if lagLevel == 0
                    % Pick and save new face
                    newFace = newFaces(randi(2)); % pick a random face between the 2 possibilites
                    faceStim = strcat('SHINEd_',char(newFace),'.tif');
                    P.session2(b,trial).faceStim = faceStim;
                    template = strcat('template_', newFace,'.tif');

                    % save context color
                    colorStim = 'none';
                    P.session2(b,trial).colorStim = colorStim;  

                    % Set parameters
                    P.session2(b,trial).correctAns = 'n';

                else
                    % Pick and save old face
                    faceStim = strcat('SHINEd_',char(listFaces(idx_stim)),'.tif');
                    P.session2(b,trial).faceStim = faceStim;
                    template = strcat('template_',listFaces(idx_stim),'.tif');

                    % Save context color
                    colorStim = listColors(idx_stim);
                    P.session2(b,trial).colorStim = colorStim;

                    % Set parameters
                    P.session2(b,trial).correctAns = 'y';
                end

            elseif type1 =='r'% Recollection Task  
                taskInstruction = 'Sur fond bleu ?';

                % Pick an already seen face index ...
                if lagLevel == 0
                    pseudolag = str2double(trialFeat(5,trialcount));
                    idx_stim = (P.design.nfaces + 1) - pseudolag;    
                    P.session2(b,trial).correctAns = 'n';
                    P.session2(b,trial).pseudolag = pseudolag; 
                else
                    P.session2(b,trial).correctAns = 'y';      
                    P.session2(b,trial).pseudolag = 0; 
                end

                faceStim = strcat('SHINEd_',char(listFaces(idx_stim)),'.tif');
                disp('Recollection trial. listFaces:');
                disp(listFaces);
                fprintf('idx_stim = %i, Displayed stim = %s\n',idx_stim, listFaces(idx_stim));
                
                P.session2(b,trial).faceStim = faceStim;
                template = char(strcat('template_',listFaces(idx_stim),'.tif')); 
                colorStim = listColors(idx_stim);       
                P.session2(b,trial).colorStim = colorStim;
            end
        end
%% Type 1 task
        WaitSecs(0.2); % wait 0.2 sec before type 1
        if (type1 == 'f' || type1 == 'r')
            online = 0;   
            tex = makeTextures(path_stimsM,faceStim,colorStim,template,online);
            P = type1Task_session2(P, screen, b, trial, tex, taskInstruction, trigQ1, trigM1, trigR1);
        else            
            
            % Init variables  
            sigLevel = str2double(trialFeat(2,trialcount));
            P.session2(b,trial).sigLevel = sigLevel;
            if sigLevel == 0
                P.session2(b,trial).stimamp = 0;                 
            else 
                P = inittrial(P,trial,dcount,b);
            end
            
            % draw fix cross
            Screen('DrawText', win, '+',  P.midX, P.midY, 0);
            Screen('Flip',win);
            WaitSecs(P.tITI);

            % draw stimulus
            [P, m, text_m] = drawstim(win,P,trial,b,session,dcount);
            P.session2(b,trial).colorStim = 'none';
            P.session2(b,trial).paradigm = P.paradigm;
            P.session2(b,trial).paradigm_idx = 4;

            % First-order response
            [P,quit] = firstorder_session2(win,P,trial,b,m,text_m);            
            dcount = dcount + 1;
            
            if quit
                break
            end
        end
        
        trialcount = trialcount + 1;
        
        % Flip the stimulus
        Screen('FillRect', win, screen.colbackground);  % Clear screen to background color
        Screen('Flip', win);% Flip to the screen
        WaitSecs(P.design.ISI);  
        
 %% Type 2 Task: confidence Judgment
        HideCursor();    
        if EL
            fnSendTTL(isSendTrigger,pp_address,trigQ2);
        end
        
        if P.session2(b,trial).resp ~= "none"
            [P, quit] = type2Task(P,b,trial,train,CrossWidth,trigQ2 ,trigM2,trigR2);
        else
            P.VAS(b,trial).onset         = NaN;
            P.VAS(b,trial).confidence    = NaN;
            P.VAS(b,trial).conf_absolute = NaN;
            P.VAS(b,trial).rt            = NaN;
            P.VAS(b,trial).firstRT       = NaN;
        end
        
        if EL
            fnSendTTL(isSendTrigger,pp_address,trigR2);
        end
    
        %% close all open textures to preserve memory
        Screen('Close');
        if quit
            break;
        end
        
        %% compute accuracy for familiarity and recollection
          if type1~='d' % correct or incorrect resp
                if ((P.session2(b,trial).resp == string(P.design.yesResp))&& P.session2(b,trial).correctAns == 'y' || ...
                    (P.session2(b,trial).resp == string(P.design.noResp)) && P.session2(b,trial).correctAns == 'n' )
                     P.session2(b,trial).ac = 1;
                else
                     P.session2(b,trial).ac = 0;
                end

                if  P.session2(b,trial).sigLevel == 5
                    P.session2(b,trial).sigLevel = 0;
                end

                % SDT data
                if P.session2(b,trial).ac == 1 && P.session2(b,trial).correctAns == 'y'
                    P.session2(b,trial).SDT = 'hit';
                elseif P.session2(b,trial).ac == 1 && P.session2(b,trial).correctAns == 'n'
                    P.session2(b,trial).SDT = 'crej';
                elseif P.session2(b,trial).ac == 0 && P.session2(b,trial).correctAns == 'y'
                    P.session2(b,trial).SDT = 'miss';
                elseif P.session2(b,trial).ac == 0 && P.session2(b,trial).correctAns == 'n'
                    P.session2(b,trial).SDT = 'fa';
                end            
          end
       
    end
	
    save([P.savebloc blocname], 'P');
    % inter-bloc break & end message
    pauseMessage = sprintf('Fin du bloc %i/%i\n\nCliquez pour reprendre l''expérience', b, P.design.nblocs);
    endMessage = 'C est terminé ! \n\nMerci pour votre participation.\n\nAppelez l''expérimentateur.';

    if b == P.design.nblocs
        DrawFormattedText(win, endMessage, 'center', 'center', WhiteIndex(win));
    else
        DrawFormattedText(win, pauseMessage, 'center', 'center', WhiteIndex(win));
    end

    Screen('Flip', win); % Update the display to show the instruction text
    HideCursor();
    while 1
        [mouseX, mouseY, mouseButtons] = GetMouse;
        if mouseButtons(1) > 0
            break
        end
    end
    Screen('Flip', win, screen.colbackground);  % Clear screen to background color  
end

%% SAVE FILE
P = plotperf(P);
P.T = [];
P.stimList1 = [];
P.stimList2 = [];
save([P.savepath filename], 'P');

print(['suj' num2str(P.suj.number) '_session2_avgPerf.png'],'-dpng');
[perfPlot, ~, alpha] = imread(['suj' num2str(P.suj.number) '_session2_avgPerf.png']);
close;
perfTex = Screen('MakeTexture', win, perfPlot);        
Instruction = strcat(['Pour quitter appuyez sur ESPACE']) ;
DrawFormattedText(win, Instruction, 'center', 100, WhiteIndex(win));
Screen('DrawTextures', win, perfTex, [], P.design.bigRect3);
Screen('Flip', win);

%check before continue
PsychHID('KbQueueFlush', P.index);
KbWait([],2);
movefile(['suj' num2str(P.suj.number) '_session2_avgPerf.png'],'../data/tmp/');

HideCursor();
% close all open textures to preserve memory
Screen('Close');
Screen('Flip', win);
PsychHID('KbQueueFlush', P.index);
PsychHID('KbQueueStop', P.index);
                     
end