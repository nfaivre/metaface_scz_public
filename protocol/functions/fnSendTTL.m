% By Shuo Wang Dec 11 2012
% send TTL

function fnSendTTL(isSendTrigger,pp_address,trigger)

if isSendTrigger
%     ioObj = io32;
%     status = io32(ioObj);
%     
%     io32(ioObj,pp_address,trigger);   %output command
%     WaitSecs(0.002);
%     io32(ioObj,pp_address,0);   %output command
    
%32-bit windows, windows XP
% writeIOw('3BC', TTLValue);

%64 bit windows, 32bit matlab or 64bit matlab (call correct input_io io32
%or io64)

outp(pp_address, trigger);
WaitSecs(0.002);
outp(pp_address, 0);

end