function [y, Fs] = buzz(T, env)
%% Create Sound
folder = pwd;
baseFileName = 'buzz_sound.wav';
fullFileName = fullfile(folder, baseFileName);

% Set up the time axis:
Fs = 10000;
duration = .3; % seconds.
t = 1 : (duration * Fs); % time axis

% Set up the period (pitch T, envelop env):
% Create the maximum amplitude:
Amplitude = 32767;
Amplitude = Amplitude .* sin(2.*pi.*t ./ env); % Pulsing sound.

% Construct the waveform:
y = int16(Amplitude .* sin(2.*pi.*t./T));

% Write the waveform to a file:
audiowrite(fullFileName, y, Fs);
end
% Plot the waveform:
% plot(t, y, 'b'); hold on;
% title('Waveform', 'FontSize', fontSize);
% xlabel('Time', 'FontSize', fontSize);
% ylabel('Y', 'FontSize', fontSize);
% grid on;
