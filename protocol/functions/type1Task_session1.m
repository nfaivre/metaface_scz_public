function P = type1Task_session1(P,screen, b, trial, tex, taskInstruction ,trigQ1 ,trigM1 ,trigR1)

global win
global isSendTrigger
global pp_address
global DUMMY_EYELINK EL
     
SetMouse(P.midX,P.midY,win);

if EL
    fnSendTTL(isSendTrigger,pp_address,trigQ1); % RS trigger stim onset to port
    myEyelink( 'Message', num2str(trigQ1), DUMMY_EYELINK); % RS trigger stim onset to eyetracker
end

% Show stimulus on screen at next possible display refresh cycle,
% and record stimulus onset time in 'startrt':
[VBLTimestamp, startrt]=Screen('Flip', win);

%% check bad key presses/releases during key press
PsychHID('KbQueueCheck', P.index);
PsychHID('KbQueueFlush', P.index);
PsychHID('KbQueueStop', P.index);

%% type 1 answer
%Intialize
counter = 1; 
clicks = [];
mousetrail_x = [];
mousetrail_y = [];
movementBegun   = 0;

P.session1(b,trial).onset = startrt;
P.session1(b,trial).rt = NaN;
P.session1(b,trial).resp = NaN;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
while 1
    
    drawresp_m(win,P,tex,taskInstruction);
    
    % ShowCursor;
    [mouseX, mouseY, mouseButtons] = GetMouse;   
    Screen('DrawText', win, '+',  mouseX, mouseY, 0);
   
    % set spatial limits to the rectangles
    if mouseX > P.design.clickRange(3,2)
        mouseX = P.design.clickRange(3,2);
        SetMouse(mouseX,mouseY,win);
    elseif mouseX < P.design.clickRange(1,1)
        mouseX = P.design.clickRange(1,1);
        SetMouse(mouseX,mouseY,win);
    end		   
    if mouseY < P.design.Ymin
        mouseY = P.design.Ymin;
        SetMouse(mouseX,mouseY,win);
    end

    % detects the first movement made by the mouse
    if mouseY ~= P.midY && ~movementBegun
        P.session1(b,trial).firstRT = GetSecs - P.session1(b,trial).onset;
        movementBegun = 1;
    end
    
    mousetrail_x(counter)   = mouseX; % save Y trajectory
    mousetrail_y(counter)   = mouseY; % save Y trajectory
    moveTime(counter)       = GetSecs - P.session1(b,trial).onset;
    counter                 = counter + 1;

    % detect clicks
    if mouseY > P.design.Ymin && mouseY < P.design.Ymax
        if mouseX > P.design.clickRange(1,1) && mouseX < P.design.clickRange(3,1)% Response "No"
            clicks = [clicks mouseButtons(1)];
            sumClicks = sum(clicks);
            if sumClicks > 1 && mouseButtons(1) == 0 % Released mouse click
               P.session1(b,trial).rt = GetSecs - P.session1(b,trial).onset;
               P.session1(b,trial).resp = P.design.noResp;
               break
            end
        elseif mouseX > P.design.clickRange(1,2) && mouseX < P.design.clickRange(3,2) % Response "Yes"
            clicks = [clicks mouseButtons(1)];
            sumClicks = sum(clicks);
            if sumClicks > 1 && mouseButtons(1) == 0 % Released mouse click
               P.session1(b,trial).rt = GetSecs - P.session1(b,trial).onset;
               P.session1(b,trial).resp = P.design.yesResp;
               break
            end
        end
    end
    
    if  (GetSecs - P.session1(b,trial).onset) > P.design.RTmax %Too long        
        
        Screen('Flip', win, screen.colbackground);  % Clear screen to background color 
        
        % Play buzz sound
        sound(P.buzz.y, P.buzz.Fs);
        
        % Display error message
        Screen('TextSize',win,40);
        warningMessage = ['TEMPS LIMITE ATTEINT\n\nRépondez plus vite.'];
        DrawFormattedText(win, warningMessage, 'center', 'center',[255,165,0], WhiteIndex(win));
        Screen('Flip', win); % Update the display to show the instruction text
        HideCursor();
        WaitSecs(2);
        Screen('Flip', win, screen.colbackground);  % Clear screen to background color 
        
        % Set response to 'none':
        P.session1(b,trial).resp = 'none'; 
        P.session1(b,trial).SDT = 'none';  
        Screen('TextSize', win, 26);
        
        break
    end

    Screen('Flip', win);
end
P.session1(b,trial).X = mousetrail_x;
P.session1(b,trial).Y = mousetrail_y;
P.session1(b,trial).moveTime = moveTime;
HideCursor();

WaitSecs(.001);

if P.session1(b,trial).resp ~= "none"
    %% display response
    % draw texture again 
    drawresp_m(win,P,tex,taskInstruction);
    
    % show response   
    if P.session1(b,trial).resp == P.design.yesResp
            SendTrigger(P.TRIGGER_YES,P.triggers);
            rect = P.design.clickRange(:,2).';
            fprintf('YES: %.2f, %.2f, %.2f, %.2f\n',rect);
            Screen('FrameOval',win,[0 P.depth 0],rect,2);
       else
            SendTrigger(P.TRIGGER_NO,P.triggers);
            rect = P.design.clickRange(:,1).';
            fprintf('NO: %.2f, %.2f, %.2f, %.2f\n',rect);
            Screen('FrameOval',win,[0 P.depth 0],rect,2);
    end
    Screen('Flip', win);
    WaitSecs(P.tfb);
end

end
  
