function tex = makeTextures(path_images,faceStim,colorStim,template,online)

global win

screen.colbackground = 128;
screen.colbackgroundrgb = [128 128 128];

% Create image matrix : 
    faceIm = imread(strcat(path_images,faceStim));  
    face   = imread(strcat(path_images,faceStim));
    contour = ones(size(faceIm));

% Create template matrix : 
    templateIm = imread(strcat(path_images,template));
    
% Adjust face matrix
    faceIm = imresize(faceIm,0.75);
    templateIm=imresize(templateIm,0.75);

% Create noise matrix : 
    Y = scramble(face,1);
    Y = 255*(Y - min(Y(:)))./(max(Y(:) - min(Y(:))));
    Y = Y.*contour;    
   
% Create color noise matrix :
if  colorStim ==  'b' % get a blue neige
    colorneige(:,:,1)= zeros(1024,1024);
    colorneige(:,:,2)= zeros(1024,1024);
    colorneige(:,:,3)= Y;
    colorneige(:,:,2)= 60 * ones (1024,1024); % balance luminosity with green channel
else 
    colorneige(:,:,1)= Y; % get a red neige
    colorneige(:,:,2)= zeros(1024,1024);
    colorneige(:,:,3)= zeros(1024,1024);
    colorneige(:,:,2) = 20 * ones (1024,1024); % balance luminosity with green channel
end
    
lum = 0.30*colorneige(:,:,1)+0.59*colorneige(:,:,2)+0.11*colorneige(:,:,3); % get luminosity
lum = mean(mean(lum)); 
disp(lum);

% create 3 color channels 
if online == 1
    color1 = colorneige(:,:,1);
    color2 = colorneige(:,:,2);
    color3 = colorneige(:,:,3);
end

% create masks to keep only the face and fill the neige matrix with it
bigmask=zeros(1024,1024);
smallface=zeros(1024,1024);
bigmask(129:896,129:896)= templateIm;
smallface(129:896,129:896)= faceIm;
visagemask=find(bigmask==255);

if online == 1
    color1(visagemask)=smallface(visagemask);
    color2(visagemask)=smallface(visagemask);
    color3(visagemask)=smallface(visagemask);

    colorneige(:,:,1) = color1;
    colorneige(:,:,2) = color2;
    colorneige(:,:,3) = color3;
    
    tex = Screen('MakeTexture', win, colorneige); % make texture
else
    Y(visagemask)=smallface(visagemask);
    tex = Screen('MakeTexture', win, Y); % make texture
end

end