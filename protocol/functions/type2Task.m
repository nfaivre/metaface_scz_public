function [P, quit] = type2Task(P, b, t, train, CrossWidth,trigQ2 ,trigM2,trigR2)

global win
global isSendTrigger
global pp_address
global DUMMY_EYELINK EL
global edfName

VASrange    = [P.midY-150, P.midY+150];

starty = (VASrange(1) + VASrange(2)) /2; % for start in the middle
initialy = round(starty);
SetMouse(0,initialy,win);
HideCursor;

%Intialize
P.VAS(b,t).onset = GetSecs;
counter = 1; 
clicks = [];
% mousetrail_y = [];
movementBegun   = 0;

tic;

%% RS trigger Q2 onset
if EL
    fnSendTTL(isSendTrigger,pp_address,trigQ2);
    myEyelink( 'Message', num2str(trigQ2), DUMMY_EYELINK);
end
quit=0;
while 1
    HideCursor;
    % read the keyboard to quit if needed
    [keyIsDown, Secs, keyCode] = KbCheck;
    if keyCode(P.design.quitkey)
        quit=1;
        break   
    end
    
    % draw scale
    [mouseX, mouseY, mouseButtons] = GetMouse;
    draw_vertical_vas(VASrange);
    
    % display instruction
    message = 'Estimez votre confiance dans votre r�ponse:';  
    DrawFormattedText(win, message, 'center', 120, WhiteIndex(win));
    
    % set spatial limits to the scale
    if mouseY > VASrange(2)
        mouseY = VASrange(2);
        SetMouse(mouseX,mouseY,win);       
    elseif mouseY < VASrange(1)
        mouseY = VASrange(1);
        SetMouse(mouseX,mouseY,win);
    end
    
    % draw cursor
    Screen('DrawText',win, '+', P.midX-CrossWidth(3)/2-3,mouseY-CrossWidth(4)/2-1,0);%(-(CrossWidth(4)/2)/2)-1 ; /2)
    conftmp=(VASrange(2) - mouseY)/(max(VASrange)-min(VASrange));
    Screen('DrawText',win,sprintf('%d%%',round(conftmp*100)), P.midX -120,P.midY-10,0);
    Screen('Flip',win); % display
    HideCursor;
    
    % detects the first movement made by the mouse
    if mouseY ~= initialy && ~movementBegun
        P.VAS(b,t).firstRT = GetSecs - P.VAS(b,t).onset;
        movementBegun = 1;
        
        if EL
            fnSendTTL(isSendTrigger,pp_address,trigM2); % RS trigger M2 mouse move to port
            myEyelink( 'Message', num2str(trigM2), DUMMY_EYELINK); % RS trigger M2 mouse move to eyetracker
        end 
    end
    
%     mousetrail_y(counter)   = mouseY; % save Y trajectory
%     moveTime(counter)       = GetSecs - P.VAS(b,t).onset;
    counter                 = counter + 1;
    
    %% detect clicks
    clicks = [clicks mouseButtons(1)];
    sumClicks = sum(clicks);
    if sumClicks > 1 && mouseButtons(1) == 0  
        if EL
            fnSendTTL(isSendTrigger,pp_address,trigR2); % RS trigger R2 to port
            myEyelink( 'Message', num2str(trigR2), DUMMY_EYELINK); % RS trigger R2 to eyetracker
        end 
        P.VAS(b,t).confidence        = conftmp;
        P.VAS(b,t).conf_absolute     = mouseY;
        P.VAS(b,t).rt             = GetSecs - P.VAS(b,t).onset;
        break
        
    elseif any(mouseButtons(2:length(mouseButtons))>0) % cancel trial if right click     
        break
    end
    
    
end

if ~movementBegun
    P.VAS(b,t).firstRT      = NaN;
end

Screen('Flip',win);
HideCursor;
WaitSecs(P.design.ISI);

end
