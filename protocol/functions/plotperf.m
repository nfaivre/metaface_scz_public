function P = plotperf(P)

if P.paradigm == "memoryPerf"
    ac = [P.session1.ac];
    lag = [P.session1.lag];
    task = [P.session1.type];
    disp('ac:')
    disp(ac)
    disp('lag:')
    disp(lag)
    disp('task:')
    disp(task)
       
    % Compute Recollection and Familiarity perf for each lag
    x = unique(lag);
    disp('x:')
    disp(x)
    [meanR, meanF] = deal(zeros(1,length(x)),zeros(1,length(x)));
    for i = 1:length(x)
        meanR(i) = mean(ac(task == 'r' & lag == x(i)));
        meanF(i) = mean(ac(task == 'f' & lag == x(i)));
    end

    figure(99); 
    hold on;
    plot(x,meanR,'g');
    plot(x,meanR,'ko','MarkerFaceColor','g');
    plot(x,meanF,'r');
    plot(x,meanF,'ko','MarkerFaceColor','r');
    xlabel('Lag');
    ylabel('Proportion Correct');
    legend({'','Recollection', '', 'Familiarity'},'Location','eastoutside');
    
elseif P.paradigm == "staircase"
    amp = [P.session1.stimamp];
    amp = amp([P.session1.paradigm_idx] == 2); %staircase
    len = length(amp);
    
    figure(99); 
    hold on;
    plot(amp,'ko-','MarkerSize',5,'MarkerFaceColor','k');
    xlabel('Trial');
    ylabel('Stimulus Amplitude');
    title('Staircase procedure');
    set(gcf,'Position',[100,700,400,150]);
    P.threshAmp = mean(amp(len-9:len));
    
elseif P.paradigm == "Psychometric"
%     t = toc;
%     
%     sdt = {P.session1.SDT};
%     sdt = sdt(idx);
%     hit = find(strcmp(sdt,'hit'));
%     miss = find(strcmp(sdt,'miss'));
%     crej = find(strcmp(sdt,'crej'));
%     fa = find(strcmp(sdt,'fa'));
    
    idx = ([P.session1.paradigm_idx] == 3); %Psychometric
    amp =  [P.session1.stimamp];%.*stim;
    amp = amp(idx);
    ac = [P.session1.ac];
    ac = ac(idx);
    x = unique(amp);
    len = length(x);
    y = zeros(1,len);
    for i=1:len
        if i == 1
            y(i) = 1 - mean(ac(amp==x(i)));
        else
            y(i) = mean(ac(amp==x(i)));
        end
    end
    
    P.amps_perf = y;
    
    x = x(2:end);
    y = y(2:end);
    
    figure(98); 
    hold on;
    plot(x,y,'ko-','MarkerSize',5,'MarkerFaceColor','k');
    xlabel('Stimulus amplitude')
    ylabel('Hit rate')

    % Fit psychometirc functions
    targets = [0.25, 0.5, 0.75]; % 25%, 50% and 75% performance
    weights = ones(1,length(x)); % No weighting
    P = FitPsycheCurveLogit(P, x, y, weights, targets);

    % Plot psychometic curves
    plot(P.fit.curve(:,1), P.fit.curve(:,2), 'LineStyle', '--');
    legend({'data', 'fit'},'Location','eastoutside');
    set(gcf,'Position',[100,700,400,150]);

elseif P.paradigm == "main"
    ac =       [P.session2.ac];
    sigLevel = [P.session2.sigLevel];
    task =     [P.session2.type];
    
    % Compute Memory perf for each signal level
    x = unique(sigLevel);
    avgFamil = zeros(1,length(x));
    avgRecol = zeros(1,length(x));
    avgPerception = zeros(1,length(x));
    
    for i = 1:length(x)
        avgFamil(i) = mean(ac(task == 'f' & sigLevel == x(i)));
        avgRecol(i) = mean(ac(task == 'r' & sigLevel == x(i)));
        avgPerception(i) = mean(ac(task == 'd' & sigLevel == x(i)));
    end
    
    figure(99); hold on;
    plot(x,avgRecol,'g');
    plot(x,avgRecol,'ko','MarkerFaceColor','g');
    plot(x,avgFamil,'r');
    plot(x,avgFamil,'ko','MarkerFaceColor','r');
    plot(x,avgPerception,'b');
    plot(x,avgPerception,'ko','MarkerFaceColor','b');
    xlabel('Signal Level');
    ylabel('Proportion Correct');
    legend({'','Recollection', '', 'Familiarity','','Perception'},'Location','eastoutside');
end