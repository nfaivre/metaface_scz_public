function P =  session1(P, screen, path_stimsM)
%  UNTITLED Summary of this function goes here :  test session 1 
%  Detailed explanation goes here

global win
global trigQ1 trigM1 trigR1

session = P.suj.session;
quit = 0;

%%  Total number of trials 
% Session 1
Ntrials1 = P.design.nMemtrials * P.design.nMemblocs;
% Session 2
Ntrials2 = P.design.n * P.design.nblocs;   
%%  Total number of stimuli
% Session 1
Nstim1 = Ntrials1/2 * ((P.design.nfaces + 2) + P.design.nfaces); % total number of face stimuli (session1)
% Session 2
Nstim2 = Ntrials2/3 * ((P.design.nfaces + 2) + P.design.nfaces); % total number of face stimuli (session2)

%% Create non-overlapping lists of stimuli for session 1 & 2
P = stimlist(P, Nstim1, Nstim2);

%% Parameters settings%%
P.T = [];
P.paradigm = 'memoryPerf';
trialcount = 1;
idx = 1; % index of stimList1
  
%% Randomizing trials
trialFeat = randtrials(session, Ntrials1);

colorComb  = nchoosek(1:4,2);% all combinations of indexes for the 2 red contexts among 4 contexts
color0     = repmat('b',1,4);
genderComb = nchoosek(1:4,2);% all combinations of indexes for the 2 women faces among 4 stims
gender0    = 1:4;

% Welcome message
Screen('TextFont', win)
welcomMessage = 'Bonjour et merci pour votre participation à cette exp�rience. \n\nDans cette exp�rience, des visages vont �tre present�s successivement.\n\nChaque visage est pr�sent� bri�vement, donc soyez attentif.ve! :)\n\n\n Vous utiliserez la souris pour r�pondre.\n\n\nCliquez pour continuer.';
DrawFormattedText(win, welcomMessage, 'center', 'center', WhiteIndex(win));
Screen('Flip', win); % Update the display to show the instruction text
HideCursor();
RestrictKeysForKbCheck([P.design.quitkey,P.design.pausekey,KbName('space'),[1]]);
PsychHID('KbQueueFlush', P.index);
PsychHID('KbQueueStop', P.index);
WaitForMouseRelease();
Screen('Flip', win, screen.colbackground);  % Clear screen to background color 

%% PART I: Memory trials, type 1 only

% Training 
Screen('TextFont', win)
train = 1;
training(P, screen, train)
train = 0; 


filename1 = ['suj' num2str(P.suj.number) '_session1_' convertStringsToChars(P.paradigm)  '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat'];
for b = 1: P.design.nMemblocs
    fprintf('\n-----------------------\n');
    fprintf(['Now running block ' num2str(b) '\n']);
	blocname = ['suj' P.suj.number '_session1_bloc' int2str(b) '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat'];																															  

% run through the n trials
    for trial = 1:P.design.nMemtrials  % loop through the n trials          
        P.session1(b,trial).session = session;
        P.session1(b,trial).blocNumber = b;
        P.session1(b,trial).bloc = b;       
        P.session1(b,trial).paradigm = P.paradigm;
        P.session1(b,trial).paradigm_idx = 1;
        P.session1(b,trial).type = char(trialFeat(1,trialcount)); % Determine the task
        P.session1(b,trial).stimamp = -1; %this field stores only perceptual intensities. Put to -1 in memory trials, for matrix dimensions purpose
        type1 = P.session1(b,trial).type;
        P.session1(b,trial).pseudolag = NaN;

        trigstart = 101 + (trial-1); % trigger index trial onset

        % Pick stim numbers           
        listFaces = reshape(transpose(P.stimList1(:,idx:idx+1)),1,[]);% Extract 4 stim indexes from stimList1 => Stims to encode
        if type1 == 'r'             
            idx = idx + 2;  
        elseif type1 == 'f'
            newFaces = reshape(P.stimList1(:,idx+2),1,[]);% Extract 2 stim indexes from stimList1 => New stims for the test phase
            idx = idx + 3;  % add a new face             
        end

        % Determine context colors
        listColors = color0; %initialize
        col_comb_idx = str2double(trialFeat(3, trialcount));
        listColors(colorComb(col_comb_idx,:)) = 'r';

        % Determine gender order
        genders = 'ffff';
        gen_comb_idx = str2double(trialFeat(4, trialcount));
        h_idx = genderComb(gen_comb_idx,:);
        genders(h_idx) = 'h';
        f_idx = gender0;
        f_idx(h_idx)=[];
        listTmp = strings(1,4);
        listTmp(h_idx) = listFaces(1:2);
        listTmp(f_idx) = listFaces(3:4);
        listFaces = listTmp;
        
        P.session1(b,trial).genders = genders;
        P.session1(b,trial).colors = listColors;
        P.session1(b,trial).faces = convertContainedStringsToChars(listFaces);

% Encoding phase : Display a list of 4 faces to memorize

        for stim = 1:P.design.nfaces % Load stimuli
            KbCheck;% initialize KbCheck and variables
            online =1;
            %Create masks from loaded templates
            template = strcat('template_',char(listFaces(stim)),'.tif');

            %load stimuli and color
            faceStim = strcat('SHINEd_',char(listFaces(stim)),'.tif');
            colorStim = char(listColors(stim));
            tex(stim) = makeTextures(path_stimsM,faceStim,colorStim,template,online);            
        end
        
        for stim = 1:P.design.nfaces % Display stimuli
            % Draw texture image to backbuffer.
            Screen('DrawTexture', win, tex(stim), [], P.design.centeredRect); % big rectangle 
            Screen('Flip', win);
            WaitSecs(P.design.stimDuration)

            Screen('FillRect', win, screen.colbackground);% Clear screen to background color
            Screen('Flip', win); % Flip to the screen
            WaitSecs(P.design.ISI);% Inter-stimulus Interval
        end
        
% for stim = 1:P.design.nfaces
%             KbCheck;% initialize KbCheck and variables
%             online =1;
%             %Create masks from loaded templates
%             template = strcat('template_',char(listFaces(stim)),'.tif');
% 
%             %load stimuli and color
%             faceStim = strcat('SHINEd_',char(listFaces(stim)),'.tif');
%             colorStim = char(listColors(stim));
%             tex = makeTextures(path_stimsM,faceStim,colorStim,template,online);
% 
%             % Draw texture image to backbuffer.
%             Screen('DrawTexture', win, tex, [], P.design.centeredRect); % big rectangle       %%tex <=> oltex(stim)             
%             Screen('Flip', win);
%             WaitSecs(P.design.stimDuration)
% 
%             Screen('FillRect', win, screen.colbackground);% Clear screen to background color
%             Screen('Flip', win); % Flip to the screen
%             WaitSecs(P.design.ISI);% Inter-stimulus Interval
%         end
        
        Screen('Close');
        
% Test phase: Display a stimulus, depending on the task

        PsychHID('KbQueueStart',P.index);  
        lagLevel = str2double(trialFeat(2,trialcount)); % Determine the Lag
        idx_stim = (P.design.nfaces+1)- lagLevel;  % and corresponding stimulus index in the list of 4
        P.session1(b,trial).lag = lagLevel;
        P.session1(b,trial).sigLevel = idx_stim;

        if type1 == 'f' %Familiarity task
            taskInstruction = 'Déjà vu?'; 
            if lagLevel == 0
                % Pick and save new face
                newFace = newFaces(randi(2)); % pick a random face between the 2 possibilites
                faceStim = strcat('SHINEd_', char(newFace),'.tif');
                P.session1(b,trial).faceStim = faceStim;
                template = strcat('template_', newFace,'.tif');

                % save context color
                colorStim = 'none';
                P.session1(b,trial).colorStim = colorStim;  

                % Set parameters
                P.session1(b,trial).correctAns = 'n';
            else
                % Pick and save new face
                faceStim = strcat('SHINEd_',char(listFaces(idx_stim)),'.tif');
                P.session1(b,trial).faceStim = faceStim;
                template = strcat('template_',listFaces(idx_stim),'.tif');

                % save context color
                colorStim = listColors(idx_stim);
                P.session1(b,trial).colorStim = colorStim;

                % Set parameters
                P.session1(b,trial).correctAns = 'y';
            end

        elseif type1 =='r'% Recollection Task  
            taskInstruction = 'Sur fond bleu ?';
            
            % Pick an already seen face index ...
            if lagLevel == 0
                pseudolag = str2double(trialFeat(5,trialcount));
                idx_stim = (P.design.nfaces + 1) - pseudolag;    
                P.session1(b,trial).correctAns = 'n';
                P.session1(b,trial).pseudolag = pseudolag; 
            else
                P.session1(b,trial).correctAns = 'y';      
                P.session1(b,trial).pseudolag = NaN; 
            end

            faceStim = strcat('SHINEd_',char(listFaces(idx_stim)),'.tif');
            P.session1(b,trial).faceStim = faceStim;
            template = char(strcat('template_',listFaces(idx_stim),'.tif')); 
            colorStim = listColors(idx_stim);       
            P.session1(b,trial).colorStim = colorStim;
        end
% Type 1 task
        WaitSecs(0.2); % wait 0.2 sec before type 1
        online = 0;   
        tex = makeTextures(path_stimsM,faceStim,colorStim,template,online);
        P = type1Task_session1(P, screen, b, trial, tex, taskInstruction, trigQ1, trigM1, trigR1);
        trialcount = trialcount+1;
        
        Screen('Flip', win, screen.colbackground);  % Clear screen to background color  
        WaitSecs(0.1);

        quit = click2continue(P,win);
        
        if quit == 1
            break
        end
        
% Compute accuracy for recollection and familiarity       
        if ((P.session1(b,trial).resp == string(P.design.yesResp)) && P.session1(b,trial).correctAns == 'y' || ...
            (P.session1(b,trial).resp == string(P.design.noResp)) && P.session1(b,trial).correctAns == 'n' )
            P.session1(b,trial).ac = 1;
        else
            P.session1(b,trial).ac = 0;
        end

        if  P.session1(b,trial).sigLevel == 5
            P.session1(b,trial).sigLevel = 0;
        end

        % SDT data
        if P.session1(b,trial).ac == 1 && P.session1(b,trial).correctAns == 'y'
            P.session1(b,trial).SDT = 'hit';
        elseif P.session1(b,trial).ac == 1 && P.session1(b,trial).correctAns == 'n'
            P.session1(b,trial).SDT = 'crej';
        elseif P.session1(b,trial).ac == 0 && P.session1(b,trial).correctAns == 'y'
            P.session1(b,trial).SDT = 'miss';
        elseif P.session1(b,trial).ac == 0 && P.session1(b,trial).correctAns == 'n'
            P.session1(b,trial).SDT = 'fa';
        end     
    end
    
	
	save([P.savebloc blocname], 'P');
	
    if quit == 1
        break
    end

% inter-bloc break & end message
    pauseMessage = sprintf('Fin du bloc %i/%i\n\nESPACE pour reprendre l''exp�rience', b, P.design.nMemblocs);
    endMessage = 'Fin de cet exercice ! \n\nPetite pause avant de passer à la suite.\n\nAppelez l exp�rimentateur.';

    if b == P.design.nMemblocs
        DrawFormattedText(win, endMessage, 'center', 'center', WhiteIndex(win));
    else
        DrawFormattedText(win, pauseMessage, 'center', 'center', WhiteIndex(win));
    end

    Screen('Flip', win); % Update the display to show the instruction text
    KbWait([],2);
    HideCursor();
    Screen('Flip', win, screen.colbackground);  % Clear screen to background color  
end

% SAVE MEMORY PERFS
P = plotperf(P);
save([P.savepath_tmp filename1], 'P');

% Print performance
print(['suj' num2str(P.suj.number) '_session1_memPerf.png'],'-dpng');
[memPlot, ~, alpha] = imread(['suj' num2str(P.suj.number) '_session1_memPerf.png']);
close;
memTex = Screen('MakeTexture', win, memPlot);        
Instruction = strcat(['Cliquez pour continuer. Pour quitter, appuyez sur ECHAP']) ;
DrawFormattedText(win, Instruction, 'center', 100, WhiteIndex(win));
Screen('DrawTextures', win, memTex, [], P.design.bigRect1);
Screen('Flip', win);

%check before continue
PsychHID('KbQueueFlush', P.index);
WaitForMouseRelease();
movefile(['suj' num2str(P.suj.number) '_session1_memPerf.png'],'../data/tmp/');

if quit == 1
    sca;
    ShowCursor;
    fclose('all');
    Priority(0);
    Screen('CloseAll');
    IOPort('CloseAll');
    return           
end
%% PART II: Detection trials, type 1 only

% 1) Psychometric staircase

P.paradigm = 'staircase';
b = P.design.nMemblocs + 1;
P = initexp(P);
edfName = instruction1(screen,P,trigQ1, trigM1, trigR1);
filename2 = ['suj' num2str(P.suj.number) '_session1_' convertStringsToChars(P.paradigm) '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat'];

for trial = 1 : P.design.nStaircase
    trialcount = trial;
    fprintf('\n-----------------------\n');
    P.session1(b,trial).type = 'd';
    P.session1(b,trial).paradigm = P.paradigm;
    P.session1(b,trial).paradigm_idx = 2;
    P.session1(b,trial).session = session;
    P.session1(b,trial).blocNumber = b;
    P.session1(b,trial).bloc = 0;
    P.session1(b,trial).sigLevel = -1; % arbitrary value, sigLevel is relevant only for "memoryPerf" & "main" paradigms
    P.session1(b,trial).pseudolag = NaN;
    P.session1(b,trial).genders = 'none';
    P.session1(b,trial).colors = 'none';
    P.session1(b,trial).faces = 'none';
    P.session1(b,trial).lag = NaN;
    
    % init variables
    P = inittrial(P,trial,trialcount,b);
    fprintf(['Now running trial ' num2str(trial) ' with amp: ' num2str(P.session1(b,trial).stimamp) '\n']);

    % draw fix cross
    Screen('DrawText', win, '+',  P.midX, P.midY, 0);
    Screen('Flip',win);
    WaitSecs(P.tITI);

    % draw stimulus
    [P, m, text_m] = drawstim(win,P,trial,b,session,trialcount);

    P.session1(b,trial).colorStim = 'none';

    % first-order response
    P = firstorder_session1(win,P,trial,b,m,text_m);
    quit = click2continue(P,win);
    
    if quit
        sca;
        ShowCursor;
        fclose('all');
        Priority(0);
        Screen('CloseAll');
        IOPort('CloseAll');
        return           
    end
end
   
    endStc = sprintf('Entraînement terminé! \n\nVous allez maintenant réaliser %i blocs de %i essais.\n\nCliquez pour démarrer..', P.design.nPerblocs, P.design.nPertrials);
    DrawFormattedText(win, endStc, 'center', 'center', WhiteIndex(win));

    Screen('Flip', win); % Update the display to show the instruction text
    HideCursor();
    WaitForMouseRelease();
    Screen('Flip', win, screen.colbackground);  % Clear screen to background color  
   
%% Save File & Print Perf
P = plotperf(P);
save([P.savepath_tmp, filename2], 'P');

print(['suj' num2str(P.suj.number) '_session1_staircase.png'],'-dpng');
[stcPlot, ~, alpha] = imread(['suj' num2str(P.suj.number) '_session1_staircase.png']);
close;
stcTex = Screen('MakeTexture', win, stcPlot);        
Instruction = strcat(['Cliquez pour continuer.']) ;
DrawFormattedText(win, Instruction, 'center', 100, WhiteIndex(win));
Screen('DrawTextures', win, stcTex, [], P.design.bigRect2);
Screen('Flip', win);

%check before continue
PsychHID('KbQueueFlush', P.index);
WaitForMouseRelease();
movefile(['suj' num2str(P.suj.number) '_session1_staircase.png'],'../data/tmp/');

% 2) Psychometric curve: Data & Fit
   
P.paradigm = 'Psychometric';
trialcount = trialcount + 1;
P = initexp(P);
filename3 = ['suj' num2str(P.suj.number) '_session1_' convertStringsToChars(P.paradigm) '_' [datestr(now,'hh.MM')] '.mat'];
 
for b = P.design.nMemblocs + 2: P.design.nMemblocs + P.design.nPerblocs + 1
    for trial = 1 : P.design.nPertrials
        P.session1(b,trial).type = 'd';
        P.session1(b,trial).paradigm = P.paradigm;
        P.session1(b,trial).paradigm_idx = 3;
        P.session1(b,trial).session = session;
        P.session1(b,trial).blocNumber = b;
        P.session1(b,trial).bloc = 0;
        P.session1(b,trial).sigLevel = -1; % arbitrary value, sigLevel is relevant only for "memoryPerf" & "main" paradigms
        P.session1(b,trial).pseudolag = NaN;
        P.session1(b,trial).genders = 'none';
        P.session1(b,trial).colors = 'none';
        P.session1(b,trial).faces = 'none';
        P.session1(b,trial).lag = NaN;
        
        % init variables
        P = inittrial(P,trial,trialcount,b);
        fprintf(['Now running trial ' num2str(trial) ' with amp: ' num2str(P.session1(b,trial).stimamp) '\n']);

       % draw fix cross
        Screen('DrawText', win, '+',  P.midX, P.midY, 0);
        Screen('Flip',win);
        WaitSecs(P.tITI);

        % draw stimulus
        [P, m, text_m] = drawstim(win,P,trial,b,session,trialcount);

        P.session1(b,trial).colorStim = 'none';

        % first-order response
        P = firstorder_session1(win,P,trial,b,m,text_m);
        quit = click2continue(P,win);
        
        if quit
            break
        end
        trialcount = trialcount + 1;  % get perception trials index
    end

    % inter-bloc break & end message
    pauseMessage = sprintf('Fin du bloc %i/%i\n\nESPACE pour reprendre l''experience', b-(P.design.nMemblocs+1), P.design.nPerblocs);
    endMessage = 'Session terminée! \n\nOn se revoit pour la deuxième session. :)\n\nAppelez l''expérimentateur.';

    if b == P.design.nPerblocs + P.design.nMemblocs + 1
        DrawFormattedText(win, endMessage, 'center', 'center', WhiteIndex(win));
    else
        DrawFormattedText(win, pauseMessage, 'center', 'center', WhiteIndex(win));
    end

    Screen('Flip', win); % Update the display to show the instruction text
    KbWait([],2);
    HideCursor();
    Screen('Flip', win, screen.colbackground);  % Clear screen to background color  

end

%% Save File & Print Perf
save([P.savepath_tmp filename3], 'P');

filename4 = ['suj' num2str(P.suj.number) '_session1_all_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat'];
P = plotperf(P);
save([P.savepath filename4], 'P');

print(['suj' num2str(P.suj.number) '_session1_psycho.png'],'-dpng');
[psychoPlot, ~, alpha] = imread(['suj' num2str(P.suj.number) '_session1_psycho.png']);
close;
psychoTex = Screen('MakeTexture', win, psychoPlot);        
Instruction = strcat(['Pour quitter, appuyez sur ESPACE']) ;
DrawFormattedText(win, Instruction, 'center', 100, WhiteIndex(win));
Screen('DrawTextures', win, psychoTex, [], P.design.bigRect2);
Screen('Flip', win);

%check before continue
PsychHID('KbQueueFlush', P.index);
KbWait([],2);
movefile(['suj' num2str(P.suj.number) '_session1_psycho.png'],'../data/tmp/');

HideCursor();
% close all open textures to preserve memory
Screen('Close');
Screen('Flip', win);
PsychHID('KbQueueFlush', P.index);
PsychHID('KbQueueStop', P.index);
    
end

