function outp(address,byte)

global cogent;

%test for correct number of input arguments
if(nargin ~= 2)
    error('usage: outp(address,data)');
end

io64(cogent.io.ioObj,address,byte);

% calling io64() with three input parameters allows the user to output data
% to the specified I/O port adress. 
% adress specifies the physical adress of the destination I/O port (<64K)
% data (here byte 0-255) represents the value being output
