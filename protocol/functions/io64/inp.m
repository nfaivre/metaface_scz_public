function [byte] = inp(address)

global cogent;

byte = io64(cogent.io.ioObj,address);

% Calling io64() using 2 inputs arguments and one return variable allows
% the user to read the contents of specified I/O port.
% adress specifies location of the I/O port
% data (here byte) specifies value returned after reading the I/O port
