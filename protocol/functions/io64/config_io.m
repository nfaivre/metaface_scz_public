function config_io

%create IO64 interface object
clear io64;
io.ioObj = io64();

%install the inpoutx64.dll driver
%status = 0 if installation successful
io.status = io64(io.ioObj);
if(io.status ~= 0)
    disp('inp/outp installation failed!')
else
    disp('inpout64 (re)installation successful.')
end

% il faut installer io64/mexw64 dans le bon path
% il faut installer inpoutx64.dll dans C:\windows\system32
% nécessite Microsoft Visual C++ 2005 SP1 Redistributable (x64) Package
