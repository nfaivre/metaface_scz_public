function training(P, screen, train)

global win
global trigQ1 trigM1 trigR1
global trigQ2 trigM2 trigR2

CrossWidth   = Screen('TextBounds', win, '+');
Screen('TextSize', win, 24);% Set text size
path_images = '../stims/stimsTraining/';

% Set parameters
session = P.suj.session;
faceN = 1;
Nfaces = 6;
if session == 1
    idx_start = 1;
elseif session == 2
    idx_start = 1 + Nfaces;
end
faceIndex = idx_start:(idx_start + Nfaces - 1);
faceList = cell(Nfaces,1);
i=1;
for index = faceIndex
    faceList{i,1}=  [strcat('f',num2str(index))];
    faceList{i+1,1} = [strcat('h',num2str(index))];
    i=i+2;
end

colorList = ["r" "r" "b" "b"];
if session == 1
    type1=['f','r'];
elseif session == 2
    type1=['f','r','d'];
end
ntrials = length(type1);
b=1;

for trial = 1:ntrials
    if type1(trial) == 'f'
        listFaces = faceList(faceN: faceN + (P.design.nfaces+1),:);% Extract nfaces+2 of face stimuli from the randomPairs array
        faceN= faceN+P.design.nfaces+2;
        tmp = listFaces(1:4);
        tmp = tmp(randperm(4));
        listFaces(1:4) = tmp;
        if session == 1
            instruction = ['TACHE 1:\n\nQuatre visages vont etre pr�sent�s successivement, sur fond rouge ou bleu. \n\nPuis, un visage sera pr�sent� sur fond gris.\n\n Votre tache consiste à d�terminer si ce visage a d�j� �t� vu, oui ou non, peu importe la couleur du fond.\n\nCliquez pour un essai de d�monstration.'];
        else
            instruction = ['TACHE 1 - RAPPEL\n\nQuatre visages vont �tre pr�sent�s successivement, sur fond rouge ou bleu. \n\nPuis, un visage sera pr�sent� sur fond gris.\n\n Votre t�che consiste � d�terminer si ce visage a d�j� �t� vu, oui ou non, peu importe la couleur du fond.\n\nCliquez pour un essai de d�monstration.'];
        end
    elseif type1(trial) == 'r'
        listFaces = faceList(faceN: faceN + (P.design.nfaces-1),:);% Extract nfaces of face stimuli from the randomPairs array
        faceN= faceN+P.design.nfaces;
        listFaces = listFaces(randperm(4));
        if session == 1
            instruction = ['TACHE 2:\n\nQuatre visages vont être présentés successivement, sur fond rouge ou bleu. \n\nPuis, un des visages vous sera présenté à nouveau.\n\n Votre tâche consiste à déterminer si ce visage était sur fond bleu, oui ou non.\n\nCliquez pour un essai de démonstration.'];
        else 
            instruction = ['TACHE 2 - RAPPEL\n\nQuatre visages vont �tre pr�sent�s successivement, sur fond rouge ou bleu. \n\nPuis, un des visages vous sera pr�sent� �nouveau.\n\n Votre t�che consiste � d�terminer si ce visage �tait sur fond bleu, oui ou non.\n\nCliquez pour un essai de d�monstration.'];
        end
    else
        instruction = ['TACHE 3 - RAPPEL\n\nCet exercice consiste � d�tecter des visages dans du brouillard.\n\n Cliquez pour lancer un essai de d�monstration.'];
    end
    
    listColors = colorList(randperm(4));    
    DrawFormattedText(win, instruction, 'center', 'center', WhiteIndex(win));
    Screen('Flip', win); % Update the display to show the instruction text
    HideCursor();
    WaitForMouseRelease();
    Screen('Flip', win, screen.colbackground);  % Clear screen to background color 
    
    if type1(trial)~='d'
        for stim = 1:P.design.nfaces
            %Create masks from loaded templates
            template = char(strcat('template_',listFaces(stim),'.tif'));

            %load stimuli
            faceStim = char(strcat('SHINEd_',listFaces(stim),'.tif'));
            colorStim = char(listColors(stim));
            % make texture image out of image matrix faceIm
            tex = makeTextures(path_images,faceStim,colorStim,template,1);      
                
            % Draw texture image to backbuffer.
            Screen('DrawTexture', win, tex, [], P.design.centeredRect);
            Screen('Flip', win);                
            WaitSecs(P.design.stimDuration);
            Screen('FillRect', win, screen.colbackground);% Clear screen to background color 
            Screen('Flip', win); % Flip to the screen
            WaitSecs(P.design.ISI);% Inter-stimulus Interval
        end
    end

    %% Type 1 tasks :
    if type1(trial) =='f'
        % Familiarity task
        taskInstruction = 'D�j� vu?';
        faceStim = char(strcat('SHINEd_',listFaces(stim+1,1),'.tif'));
        P.session1(trial,b).faceStim = faceStim;
        template = char(strcat('template_',listFaces(stim+1,1),'.tif'));
        colorStim = "none";

        % Create Texture
        tex = makeTextures(path_images,faceStim,colorStim,template,0);               
        P = type1Task_session1(P,screen, 1, 1, tex, taskInstruction, trigQ1 ,trigM1 ,trigR1);

        % Flip the stimulus       
        Screen('FillRect', win, screen.colbackground);  % Clear screen to background color 
        Screen('Flip', win);% Flip to the screen
        WaitSecs(P.design.ISI);

    elseif type1(trial)=='r'
        %% Recollection Task
        taskInstruction = 'Sur fond bleu ?';
        
        faceStim = char(strcat('SHINEd_',listFaces(stim,1),'.tif'));
        P.session1(trial,b).faceStim = faceStim;
        template = char(strcat('template_',listFaces(stim,1),'.tif'));
        colorStim =  char(listColors(stim));
        P.session1(trial,b).colorStim = colorStim;

        % Create Texture
        tex = makeTextures(path_images,faceStim,colorStim,template,0);               

        P = type1Task_session1(P,screen, 1, 1, tex, taskInstruction, trigQ1 ,trigM1 ,trigR1);

        % Flip the stimulus       
        Screen('FillRect', win, screen.colbackground);  % Clear screen to background color 
        Screen('Flip', win);% Flip to the screen
        WaitSecs(P.design.ISI); 

    elseif session == 2
        %% Detection Task : 
        P = initexp(P);
        i = 1; trialcount = 1; b = 1;
        fprintf('\n-----------------------\n');
        % init variables
        P = inittrial(P,i,trialcount,b);

        % draw fix cross
        Screen('DrawText', win, '+',  P.midX, P.midY, 0);
        Screen('Flip',win);
        WaitSecs(P.tITI);

        % draw stimulus
        [P, m, text_m] = drawstim(win,P,i,b,session,trialcount);

        % first-order response
        [P, quit] = firstorder_session1(win,P,trial,b,m,text_m);
        
        if quit
            break
        end
    end

    %% type 2 task : show the confidence scale
    if session == 2
        HideCursor();
        [P quit] = type2Task(P,1,1,train,CrossWidth,trigQ2 ,trigM2,trigR2); 
    end
    % close all open textures to preserve memory
    Screen('Close',tex);
end
%% Conclusion of training instructions
if session == 1
    message = sprintf('Fin de la d�monstration!\n\nVous allez maintenant réaliser %i bloc(s) de %i essais.\n\nAttention, soyez bien attentif.ve à la question posée: \n\nDans les essais qui suivent, les tâches sont présentées aléatoirement!\n\nCliquez pour commencer l''expérience.', P.design.nMemblocs, P.design.nMemtrials);
elseif session == 2
    message = sprintf('Fin de la d�monstration!\n\nVous allez maintenant r�aliser %i bloc(s) de %i essais.\n\nAttention, soyez bien attentif.ve � la question pos�e: \n\nDans les essais qui suivent, les t�ches sont pr�sent�es al�atoirement!\n\nCliquez pour commencer l''exp�rience.', P.design.nblocs, P.design.n);
end
DrawFormattedText(win, message, 'center', 'center', WhiteIndex(win));
Screen('Flip',win);
WaitForMouseRelease();
HideCursor();
Screen('Close');
end
