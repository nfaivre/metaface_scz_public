function trialFeat = randtrials(session, ntrials)
    
    if session == 1
        tasks = ["r","f"];
    else 
        tasks = ["r","f","d"];
    end
    signalLevels = [0,1,2,3,4]; % number of conditions / sigArrayR
    
    trialTL0 = CombVec(tasks,signalLevels); % task and level
    l = length(trialTL0);
    repetition = ntrials/l;
    trialTL = repmat(trialTL0,1,repetition);
    trialCG = CombVec(trialTL,CombVec(1:6,1:6));  % indexes for color and gender
    
    lag_blueCol = [1 2 4; 1 3 5; 2 3 6; 4 5 6]; % row index corresponds to lag, figures indicates the color_comb_idx which return a blue color for this particular lag.
    lag_redCol  = [3 5 6; 2 4 6; 1 4 5; 1 2 3];
    
    [m,n] = size(trialCG);
    idx = randperm(n);
    tmp = trialCG;
        
    for i = 1:m
        trialCG(i,:) = tmp(i,idx);
    end
    
    trialTL0(3,:) = "0";
    trialFeat = strings(5,ntrials); % row 1 : task, "r" or "f" or "d"
                                    % row 2 : lag, from "0" to "4"
                                    % row 3 : color comb index,  1 to 6
                                    % row 4 : gender comb index, 1 to 6                                
                                    % row 5 : random lag (1 to 4) for lag 0
                                                                
    for r = 1:repetition 
        for i = 1:length(trialTL0)
            iteration = str2double(trialTL0(3,i));
            level  =  str2double(trialTL0(2,i));
            
            if level == 0 % lag 0
                level = randi(4);
            end
            
            j = 1;
            while any(trialCG(1:2,j) ~= trialTL0(1:2,i)) || (iteration > repetition-1) || ~ismember(str2num(trialCG(3,j)),lag_blueCol(level,:))
                j=j+1;
            end
            iteration = iteration + 1;
            trialTL0(3,i) = num2str(iteration);
            
            if str2double(trialTL0(2,i)) == 0 %lag 0
                idx_comb = find(lag_blueCol(level,:) == str2double(trialCG(3,j)));
                trialCG(3,j) = lag_redCol(level,idx_comb);
                trialFeat(5,(r-1)*l + i) = level;
            end
            trialFeat(1:4,(r-1)*l + i) = trialCG(:,j);
            trialCG(:,j)=[];
        end 
    end
    
    [m,n] = size(trialFeat);
    rng shuffle
    idx = randperm(n);
    tmp = trialFeat;
        
    for i = 1:m
        trialFeat(i,:) = tmp(i,idx);
    end
    %trialFeat(:,trialFeat(1,:) == "r" & trialFeat(2,:) == "0")
end