function [im] = scramble(im,noise)
%SCRAMBLE Summary of this function goes here
%   Detailed explanation goes here

%imagesc(f)
%%

fim = fft2(im);
magim = abs(fim);
phim = angle(fim);

phmask = phim(randperm(numel(phim)));% + noise*randn(size(phim));
phmask = reshape(phmask,size(magim));
phsum = noise*phmask + (1-noise)*phim;
fscr = magim.*exp(1i*phsum);
im = real(ifft2(fscr));


end

