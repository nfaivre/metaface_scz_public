function [P,quit] = firstorder_session2(win,P,trial,b,m,text_m)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
quit = 0;

SetMouse(P.midX,P.midY,win);

% Initialize
counter = 1; 
clicks = [];
mousetrail_x = [];
mousetrail_y = [];
movementBegun   = 0;
% Draw response buttons
[VBLTimestamp, startrt] = Screen('Flip', win); % show response & stim from drawstim and get onset with startrt
drawresp_p(win,P);

P.session2(b,trial).onset = startrt;
P.session2(b,trial).rt = NaN;
P.session2(b,trial).resp = NaN;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
while 1
    
	drawresp_p(win,P);
    Screen('DrawTexture',win,text_m(m), [],P.design.centeredRect);
	
	% read the keyboard to quit if needed
    [keyIsDown, Secs, keyCode] = KbCheck;
    if keyCode(P.design.quitkey)
        quit=1;
        break   
    end

    % ShowCursor;
    [mouseX, mouseY, mouseButtons] = GetMouse;
    Screen('DrawText', win, '+',  mouseX, mouseY, 0);
   
    % set spatial limits to the rectangles
    if mouseX > P.design.clickRange(3,2)
        mouseX = P.design.clickRange(3,2);
        SetMouse(mouseX,mouseY,win);
    elseif mouseX < P.design.clickRange(1,1)
        mouseX = P.design.clickRange(1,1);
        SetMouse(mouseX,mouseY,win);
    end		   
    if mouseY < P.design.Ymin
        mouseY = P.design.Ymin;
        SetMouse(mouseX,mouseY,win);
    end

    % detects the first movement made by the mouse
    if mouseY ~= P.midY && ~movementBegun
        P.session2(b,trial).firstRT = GetSecs - P.session2(b,trial).onset;
        movementBegun = 1;
    end
    
    mousetrail_x(counter)   = mouseX; % save Y trajectory
    mousetrail_y(counter)   = mouseY; % save Y trajectory
    moveTime(counter)       = GetSecs - P.session2(b,trial).onset;
    counter                 = counter + 1;

    % detect clicks
    if mouseY > P.design.Ymin && mouseY < P.design.Ymax
        if mouseX > P.design.clickRange(1,1) && mouseX < P.design.clickRange(3,1)% Response "No"
            clicks = [clicks mouseButtons(1)];
            sumClicks = sum(clicks);
            if sumClicks > 1 && mouseButtons(1) == 0 % Released mouse click
               P.session2(b,trial).rt = GetSecs - P.session2(b,trial).onset;
               P.session2(b,trial).resp = P.design.noResp;
               break
            end
        elseif mouseX > P.design.clickRange(1,2) && mouseX < P.design.clickRange(3,2) % Response "Yes"
            clicks = [clicks mouseButtons(1)];
            sumClicks = sum(clicks);
            if sumClicks > 1 && mouseButtons(1) == 0 % Released mouse click
               P.session2(b,trial).rt = GetSecs - P.session2(b,trial).onset;
               P.session2(b,trial).resp = P.design.yesResp;
               break
            end
        end
    end
    
    if  (GetSecs - P.session2(b,trial).onset) > P.design.RTmax %Too long        
        
        Screen('Flip', win, 128);  % Clear screen to background color 
        
        % Play buzz sound
        sound(P.buzz.y, P.buzz.Fs);
        
        % Display error message
        Screen('TextSize',win,40);
        warningMessage = ['TEMPS LIMITE ATTEINT\n\nRépondez plus vite.'];
        DrawFormattedText(win, warningMessage, 'center', 'center',[255,165,0], WhiteIndex(win));
        Screen('Flip', win); % Update the display to show the instruction text
        HideCursor();
        WaitSecs(2);
        Screen('Flip', win, 128);  % Clear screen to background color 
        
        % Set response to 'none':
        P.session2(b,trial).resp = 'none';  
        Screen('TextSize', win, 26);
        
        break
    end

    Screen('Flip', win);
end
P.session2(b,trial).X = mousetrail_x;
P.session2(b,trial).Y = mousetrail_y;
P.session2(b,trial).moveTime = moveTime;
HideCursor();

WaitSecs(.001);
if quit == 1
    sca;
    ShowCursor;
    fclose('all');
    Priority(0);
    Screen('CloseAll');
    IOPort('CloseAll');
    return           
end
if P.session2(b,trial).resp ~= "none"
    %% display response
    % draw texture again 
    drawresp_p(win,P);
    Screen('DrawTexture',win,text_m(m), [],P.design.centeredRect);
	
    % show response   
    if P.session2(b,trial).resp == P.design.yesResp
            SendTrigger(P.TRIGGER_YES,P.triggers);
            rect = P.design.clickRange(:,2);
            fprintf('YES: %.2f, %.2f, %.2f, %.2f\n',rect);
            Screen('FrameOval',win,[0 P.depth 0],rect,2);
       else
            SendTrigger(P.TRIGGER_NO,P.triggers);
            rect = P.design.clickRange(:,1);
            fprintf('NO: %.2f, %.2f, %.2f, %.2f\n',rect);
            Screen('FrameOval',win,[0 P.depth 0],rect,2);
    end
    Screen('Flip', win);
    WaitSecs(P.tfb);
else
    P.session2(b,trial).ac = 0;
    P.session2(b,trial).SDT = 'none';
    if P.session2(b,trial).stimamp > 0
        P.session2(b,trial).correctAns = 'y';
    else
        P.session2(b,trial).correctAns = 'n';
    end	   
end



% SDT DATA
if P.session2(b,trial).resp == string(P.design.yesResp) && P.session2(b,trial).stimamp > 0 % HIT
    P.session2(b,trial).SDT = 'hit';
    P.session2(b,trial).correctAns = 'y';
    P.session2(b,trial).ac = 1;
    
elseif P.session2(b,trial).resp == string(P.design.yesResp) && P.session2(b,trial).stimamp == 0 % FALSE ALARM
    P.session2(b,trial).SDT = 'fa';
    P.session2(b,trial).correctAns  = 'n';
    P.session2(b,trial).ac = 0; 

elseif P.session2(b,trial).resp == string(P.design.noResp) && P.session2(b,trial).stimamp > 0 % MISS
    P.session2(b,trial).SDT = 'miss';
    P.session2(b,trial).correctAns = 'y';
    P.session2(b,trial).ac = 0;
    
elseif P.session2(b,trial).resp == string(P.design.noResp) &&  P.session2(b,trial).stimamp == 0 % CORRECT REJECTION
    P.session2(b,trial).SDT = 'crej';
    P.session2(b,trial).correctAns = 'n';
    P.session2(b,trial).ac = 1;
   
elseif any(strcmp(KbName(keyCode),KbName(P.design.quitkey)))
    quit = 1;
    return
end

sprintf('ac = %i',P.session2(b,trial).ac)
Screen('Flip',win);
WaitSecs(P.tfb);
end

