function [P] = inittrial(P,i,trialcount,b)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if P.paradigm == "staircase"  
    if trialcount > 1  % 1-up/1-down staircase
        if P.session1(b,i-1).ac == 0            
           P.lastamp = P.lastamp + P.stepsize;
           P.session1(b,i).stimamp = P.lastamp;
           P.count = 0;
           disp(P.lastamp);
        elseif P.session1(b,i-1).ac == 1
            if P.count == 1           
               P.lastamp = P.lastamp - P.stepsize;
               P.session1(b,i).stimamp = P.lastamp;
               if P.lastamp <= 0
                   P.lastamp = P.stepsize;
                   P.session1(b,i).stimamp = P.lastamp;
               end
               P.count = 0; %Reinitialize staircae
            elseif P.count == 0
               P.count = P.count + 1;
               P.session1(b,i).stimamp = P.lastamp;
            end
        end
    else
        P.count = 0;  % staircase
        P.lastamp = P.initialamp;
        P.session1(b,i).stimamp = P.initialamp;
    end

elseif P.paradigm == "Psychometric"
        P.session1(b,i).stimamp = P.stimamp(trialcount-P.design.nStaircase);

elseif P.paradigm == "training"
        P.session2(b,i).stimamp = P.initialamp;
    
elseif P.paradigm == "main"
        % Update Memory Perf         
        current_sigLevel = P.session2(b,i).sigLevel;
        current_task =     P.session2(b,i).type;
        current_bloc =     P.session2(b,i).bloc;
        
        P.session2(b,i).sigLevel = []; % for matrix dimension agreement
        P.session2(b,i).type =     []; % for matrix dimension agreement
        P.session2(b,i).ac =       []; % for matrix dimension agreement
        P.session2(b,i).bloc =     [];
        
        bloc = [P.session1.bloc P.session2.bloc];
        task = [P.session1.type P.session2.type];
        sigLevels = [P.session1.sigLevel  P.session2.sigLevel];  
        ac =   [P.session1.ac   P.session2.ac];

        x = unique(sigLevels); % Modalities for sigLevel
        x = x(x > 0);
        [meanR, meanF] = deal(zeros(1,length(x)),zeros(1,length(x)));
        sprintf('trial %i : length(ac) = %i, length(task) = %i, length(sigLevels) = %i, length(bloc) = %i', i, length(ac),length(task),length(sigLevels), length(bloc)) 

        for j = 1:length(x)
            meanR(j) = mean(ac(task == 'r' & sigLevels == x(j) & bloc > 2));
            meanF(j) = mean(ac(task == 'f' & sigLevels == x(j) & bloc > 2));
        end
        
        tmp = [meanR meanF];
        disp('Memory perfs:')
        disp(tmp)
        minVal = min(tmp);
        rangeVal = range(tmp);
        len = length(x)-1;
        perVals = deal(zeros(1,length(x)));
        for k = 1:length(x)
            perVals(k) = minVal + (k-1)/len * rangeVal;
        end
        
        P.session2(b,i).sigLevel = current_sigLevel;
        P.session2(b,i).type = current_task;
        P.session2(b,i).bloc = current_bloc;
        
        P.session2(b,i).perf2match = perVals(current_sigLevel);
        sprintf('Signal Level = %i, Perf to match = %.3f',current_sigLevel, P.session2(b,i).perf2match) 
        
        % Compute corresponding signal for detection in order to match with memory perf
        x = P.amps;
        x = x(2:end);
        y = P.amps_perf;
        y = y(2:end);
        target = P.session2(b,i).perf2match;
        if target == 1
            target = 0.95;
        end
        weights = ones(1,length(x))'; % No weighting       
        x = x'; y = y';
        P.fit.coeffs = glmfit(x, [y, weights], 'binomial','link','logit');
        P.threshold = (log(target./(1-target))- P.fit.coeffs(1)) / P.fit.coeffs(2);
        
        if P.threshold < 0
            P.session2(b,i).stimamp  = 0;
        else
            P.session2(b,i).stimamp  = P.threshold;
        end
        sprintf('stimamp = %.3f', P.threshold)
end

P.T(trialcount).stimonsetid = P.nmasksinit + randi(P.nmasksstim);
PsychHID('KbQueueStart',P.index);
end

