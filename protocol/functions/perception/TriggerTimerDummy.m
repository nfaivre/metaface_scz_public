
function TriggerTimerDummy(mTimer,mEvent,trigger)
%TriggerTimerDummy is used to test triggers without connecting to a serial
%port. It simply outputs some text. 
date = datestr(mEvent.Data.time,'dd-mmm-yyyy HH:MM:SS.FFF');
fprintf('\n ** %s: fake trigger %d \n',date,trigger);
stop(mTimer);
end