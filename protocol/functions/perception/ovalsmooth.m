function im = ovalsmooth(im,c)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
if nargin == 1
    c = [180 300];
end
%%
sz = size(im);
vals = -round(sz./2):round(sz./2);
[x,y] = meshgrid(vals, vals);

f=exp(-(((x)./c(1)).^2+((y)./c(2)).^2));
f=f(1:sz,1:sz);
%%
im = im.*f;
end

