function [P, m, text_m] = drawstim(win,P,i,b,session,trialcount)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

photodpos = P.photodpos;
if photodpos(1) < 0; photodpos(1) = P.midX*2 + photodpos(1) - photodpos(3); end
if photodpos(2) < 0; photodpos(2) = P.midY*2 + photodpos(2) - photodpos(4); end
photodpos(3:4) = photodpos(1:2) + photodpos(3:4);

%% prepare masks and stim
if P.paradigm == "Psychometric"
    iface = P.face(trialcount - P.design.nStaircase);
else
    iface = P.face(trialcount);
end
face = double(imread([P.facelist(iface).folder '/' P.facelist(iface).name]));

if session == 1
    P.session1(b,i).faceStim = [P.facelist(iface).name];
else
    P.session2(b,i).faceStim = [P.facelist(iface).name];
end

if strcmp(P.masking,'contour')
    stim = face;
    contour = double(imread([contourlist(iface).folder filesep contourlist(iface).name])>0);
    
elseif strcmp(P.masking,'blur')
    stim = ovalsmooth(face,P.gauss_std);
    contour = ones(size(stim));
else
    stim = face;
    contour = ones(size(stim));
end

if session == 1
    stimamp = P.session1(b,i).stimamp;
else
    stimamp = P.session2(b,i).stimamp;
end

type_m = zeros(1,P.nmaskstot);
for m = 1:P.nmaskstot
    % scramble picture
    Y = scramble(face,1);
    if (m == 1) %P.T(trialcount).stimonsetid
        sprintf('stimamp = %.2f', stimamp)
        Y = (1-stimamp)*Y + stimamp*stim;
        type_m(m) = 1;
    end
    Y = 255*(Y - min(Y(:)))./(max(Y(:) - min(Y(:))));
    Y = Y.*contour;
    text_m(m) = Screen('MakeTexture', win, Y);
end

SendTrigger(P.TRIGGER_TRIALSTART,P.triggers);
P.T(trialcount).ttrialstart = GetSecs();

%% actually present masks and stim
for m = 1:P.nmaskstot
   for flip=1:P.nframestot
        if P.photod
            % set P.photod to 1 to have a small circle blinking if there is
            % a photodiode available
            if flip < P.nframesmask/2
                Screen('FillOval',win,[],photodpos);
            else
                Screen('FillOval',win,[0 0 0],photodpos);
            end
        end

        % draw the texture
        Screen('DrawTexture',win,text_m(m), [],P.design.centeredRect);
        if type_m(m) == 1
            % if it's a face
            if flip==1
                % send a trigger on the first screen flip (i.e. only once)
                SendTrigger(P.TRIGGER_STIMSTART,P.triggers);
                P.T(trialcount).flip(m) = GetSecs() - P.T(trialcount).ttrialstart;
            end
        else
            % or if it isnt a face
            if flip==1
                if P.trigframe
                    % P.trigframe is an option that we set to 0. 
                    % Indeed, for STN-LFP recordings, we
                    % don't send one trigger per frame because we dont
                    % trust the amplifier to be able to catch triggers
                    % every 200 ms.
                    SendTrigger(P.TRIGGER_FRAME,P.triggers);
                end
                % Instead we save the timing
                P.T(trialcount).flip(m) = GetSecs() - P.T(trialcount).ttrialstart;
            end
        end
    end
end

SendTrigger(P.TRIGGER_TRIALEND,P.triggers);
end

