
function SendTrigger(trigger,triggertype)
t = timer;
t.StartDelay = 0;
if strcmp(triggertype,'serial')
    t.TimerFcn = {@TriggerTimer,trigger};
elseif strcmp(triggertype,'dummy')
    t.TimerFcn = {@TriggerTimerDummy,trigger};
end
t.ExecutionMode = 'singleShot';
start(t);
end