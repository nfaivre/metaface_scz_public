function P = FitPsycheCurveLogit(P, xAxis, yData, weights, targets)

% Transpose if necessary
if size(xAxis,1)<size(xAxis,2)
    xAxis=xAxis';
end
if size(yData,1)<size(yData,2)
    yData=yData';
end
if size(weights,1)<size(weights,2)
    weights=weights';
end

% Check range of data
if min(yData)<0 || max(yData)>1  
     % Attempt to normalise data to range 0 to 1
    yData = yData/(mean([min(yData), max(yData)])*2);
end

% Perform fit
P.fit.coeffs = glmfit(xAxis, [yData, weights], 'binomial','link','logit');

% Create a new xAxis with higher resolution

fineX = linspace(min(xAxis),max(xAxis),numel(xAxis)*50);
%  Generate curve from fit
curve = glmval(P.fit.coeffs, fineX, 'logit');
P.fit.curve = [fineX', curve];

P.threshold = (log(targets./(1-targets))-P.fit.coeffs(1))/P.fit.coeffs(2);