function P = initexp(P)
%UNTITLED3 Summary of this function goes here
% initialize trial structure for the current specific part of the experiment
    if P.paradigm == "staircase"
        P.face = repmat(1:P.nfaces,[1 ceil(P.design.nStaircase/length(1:P.nfaces))]);
        P.face = P.face(1:P.design.nStaircase);
        rng shuffle
        perm = randperm(P.design.nStaircase);
        P.face = P.face(perm);
        
    elseif P.paradigm == "Psychometric"
        deltaAmp = P.threshAmp / 10;
        P.amps = [0 (P.threshAmp - 5*deltaAmp):deltaAmp:(P.threshAmp + 3*deltaAmp)];
        % shuffle
        [x,y] = meshgrid(1:P.nfaces, P.amps);
        P.face = repmat(x(:).',[1 P.nfacesrep]);
        P.stimamp = repmat(y(:).',[1 P.nfacesrep]);
        P.trials = length(P.stimamp);          

        % be sure not to have too many trials
        P.stimamp = P.stimamp(1:P.design.nPerTot);
        P.face = P.face(1:P.design.nPerTot);
        % permute
        rng shuffle
        perm = randperm(P.design.nPerTot);
        P.stimamp = P.stimamp(perm);
        P.face = P.face(perm);
    
    elseif ismember(P.paradigm, ["main" "training"])
        nPerTot = P.design.nblocs * P.design.n / 3;
        P.face = repmat(1:P.nfaces,[1 ceil(nPerTot/length(1:P.nfaces))]);
        P.face = P.face(1:nPerTot);
        rng shuffle
        perm = randperm(nPerTot);
        P.face = P.face(perm);

    else
        clear all
        error('Unknown paradigm');
        
    end
end

