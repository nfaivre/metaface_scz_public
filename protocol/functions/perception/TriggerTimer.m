function TriggerTimer(mTimer,mEvent,trigger)
global handle
trigDuty = 0.1;
[nwritten, when, errmsg, prewritetime, postwritetime, lastchecktime] = IOPort('Write', handle, uint8(trigger));
WaitSecs(trigDuty);
[nwritten, when, errmsg, prewritetime, postwritetime, lastchecktime] = IOPort('Write', handle, uint8(0));
stop(mTimer);
end
