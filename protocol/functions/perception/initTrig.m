try
    IOPort('CloseAll');
catch
end

if strcmp(P.triggers,'serial')
    %_______________________________
    % Open SerialPort
    for i=1:3
        try
            %[handle, errmsg] = IOPort('OpenSerialPort', P.triggerport,sprintf('BaudRate=%d',P.BaudRate));
            [handle, errmsg] = IOPort('OpenSerialPort', P.triggerport,sprintf('BaudRate=%d, parity=%s,StopBits=1',P.baudrate,'none'));

            break
        catch
            fprintf('Failed to open serial port, retrying...\n');
            pause(5);
        end
    end
end