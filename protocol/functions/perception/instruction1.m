function edfName = instruction1(screen,P,trigQ1, trigM1, trigR1)

global win 
global DUMMY_EYELINK EL

edfName  = ['s' num2str(P.suj.number) '.edf'];
Screen('TextSize', win, 24);% Set text size

% Welcome message
instr = 'TACHE 3 \n\nCet exercice consiste à détecter des visages dans du brouillard.\n\nCliquez pour lancer un essai de démonstration.';
Screen('TextSize', win, 24);
DrawFormattedText( win, instr,  'center', 'center', WhiteIndex(win));
Screen('Flip',win);
HideCursor();
WaitForMouseRelease();
Screen('Flip', win, screen.colbackground);  % Clear screen to background color 
%% training eyelink startup sequence: from initialize to drift-correction
if EL
    el = EyelinkStartupSequence(win, edfName, screen.colbackground, 255, DUMMY_EYELINK);
    status = myEyelink( 'StartRecording',DUMMY_EYELINK);


    if ~DUMMY_EYELINK
        eye_used = Eyelink('eyeavailable'); % get eye that's tracked
        % 0 for left, 1 for right and 2 for both
        if eye_used == 2 % when both eyes are tracked, use right eye
            eye_used = 1;
        end
    else
        eye_used = -1;
    end
end

%% Start with one trial of perception
    P = initexp(P);
    i = 1; b = 1; trialcount = 1; session = 1;
    fprintf('\n-----------------------\n');
    % init variables

    P = inittrial(P,i,trialcount,b);

    % draw fix cross
    Screen('DrawText', win, '+',  P.midX, P.midY, 0);
    Screen('Flip',win);
    WaitSecs(P.tITI);

    % draw stimulus
    [P, m, text_m] = drawstim(win, P,i,b,session,trialcount);

    % first-order response
    [P, quit] = firstorder_session1(win, P, 1, b, m, text_m);

    RestrictKeysForKbCheck([]);
    PsychHID('KbQueueFlush', P.index);
    PsychHID('KbQueueStop', P.index);

    % Flip the stimulus       
    Screen('FillRect', win, screen.colbackground);  % Clear screen to background color 
    WaitSecs(P.design.ISI);

    % Start message
    startMessage = sprintf('Fin de la démonstration.\n\nVous allez maintenant réaliser %i essais d''entrainement. \n\nCliquez pour commencer l''entrainement.',P.design.nStaircase);
    DrawFormattedText(win, startMessage, 'center', 'center', WhiteIndex(win));
    Screen('Flip', win); % Update the display to show the instruction text
    HideCursor();
    WaitForMouseRelease();
    Screen('Flip', win, screen.colbackground);  % Clear screen to background color 
end