path_stim = '../stims/stimsTraining/';
stimN = 20;
stimN = int2str(stimN);

screen.id                  = max(Screen('Screens'));  % main screen 
screen.colbackground       = 128; % color of the background
screen.refreshrate         = Screen('FrameRate', screen.id); % in Hz
screen.refreshtime         = 1000/screen.refreshrate; % in ms
screen.size = get(0,'ScreenSize');
[win, screen.rect] = Screen('OpenWindow', screen.id, screen.colbackground, [screen.size(3)/2 1 screen.size(3) screen.size(4)], 32,2);
screen.center           = [screen.rect(3)/2 screen.rect(4)/2];
midX                    = screen.center(1);
midY                    = screen.center(2);
screen.res              = Screen('Resolution',win);
screen.width            = screen.res.width; % in pixel units
screen.height           = screen.res.height; % in pixel units
P.design.baseRect = [0 0 0.6*screen.height  0.6*screen.height]; % Make a base Rect
P.design.centeredRect = CenterRectOnPointd(P.design.baseRect, midX, midY);
theRect = [0 0 0.23*screen.width 0.23*screen.width];
P.design.dstRects = zeros(4, 1);
P.design.dstRects(:, 1) = CenterRectOnPointd(theRect, midX, midY);

% faceIm=double(imread(['./fille/SHINEd_f1.tif']));
faceIm=imread(strcat([path_stim, 'SHINEd_f',stimN,'.tif']));
face = imread(strcat([path_stim, 'SHINEd_f',stimN,'.tif']));
templateIm=imread(strcat([path_stim,'template_f',stimN,'.tif']));
faceIm = imresize(faceIm,0.75);
templateIm=imresize(templateIm,0.75);

stim = face;
contour = ones(size(stim));
Y = scramble(face,1); % matrice de bruit
Y = 255*(Y - min(Y(:)))./(max(Y(:) - min(Y(:))));
Y = Y.*contour;

% color blue or red
blue(:,:,1)= zeros(1024,1024);
blue(:,:,2)= zeros(1024,1024);
blue(:,:,3)= Y;  % blue

% adjust luminosity
 blue(:,:,2) = 60 * ones (1024,1024);

blue1 = blue(:,:,1);
blue2 = blue(:,:,2);
blue3 = blue(:,:,3);

bigmask=zeros(1024,1024);
smallface=zeros(1024,1024);
bigmask(129:896,129:896)= templateIm;
smallface(129:896,129:896)= faceIm;
visagemask=find(bigmask==255);

k=1; % 
if k==1 % couleur
blue1(visagemask)=smallface(visagemask);
blue2(visagemask)=smallface(visagemask);
blue3(visagemask)=smallface(visagemask);

blue(:,:,1) = blue1;
blue(:,:,2) = blue2;
blue(:,:,3) = blue3;
tex = Screen('MakeTexture', win, blue);
else % noir et blanc
Y(visagemask)=smallface(visagemask);
tex = Screen('MakeTexture', win, Y);
end

Screen('DrawTexture',win,tex, [], P.design.centeredRect);
Screen('Flip',win);

KbWait([],2);
close;

Screen('CloseAll');

