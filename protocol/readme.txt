Run the experimental paradigm with Psychtoolbox in Matlab.

1) Parameter settings:

Inside the script 'config', to be found in the 'protocol' directory:

	Protocol parameters :
	   - Set the number of blocks          
	   - Set the number of trials per block (should be a multiple of 9)
	   - Set the number of trials for the visual task staircase l.156
	FYI, 10 blocks of 30 trials take approximately one hour.

2) Directories

	- Stimuli : You should download stims.zip here https://osf.io/vjrke/ and unzip it to have a 'stims' directory at the same level as 'protocol' directory.

3) Launching the experiment

	command: launchme()
	Enter Subject ID (enter a number) , Age, Sex, Hand

4) Instructions for the task
	
	You will find the instructions of the task in the file 'metaface_instructions.docx'.

5) Display settings : 

	If the stimuli are not correctly displayed (e.g. misalignment), 
	please check that your display settings match the parameter settings illustrated in the 'Display_settings.docx' file.

6) Aborting the experiment prematurely

	In case the participant wants to give up the experiment, press 'ESC' when you reach a confidence judgment task.
	It will save the behavioral data in a file named 'dataAborted_...'.
	N.B.: You can't abort the experiment before the first trial of the first block, i.e. you can't abort during the instructions nor during the training phase. 


