function launchme()

Screen('Preference', 'SkipSyncTests', 1);
%% Experimental task for metacognition evaluation
%% preliminary stuff
close all;
clc;

% addpath(genpath('C:\toolbox\Psychtoolbox')); % NF: add PTB to the path, remove upon sending
addpath('./functions');
addpath('./functions/eyelink functions');
addpath('./functions/io64');
addpath('./functions/perception');
addpath('../stims/stimsP');

path_stimsM = '../stims/stimsM/';

%% RS initiating trigger and eyelink
isSendTrigger = 0; % change to 1 if you want to send triggers!
EL = 0;            % shut down all the calls to EyeLink toolbox when put to 0

if EL
    if DUMMY_EYELINK==1
        Eyelink('InitializeDummy');
    end
end

global win 
global P
tic;
commandwindow
KbName('UnifyKeyNames'); % Make sure keyboard mapping is the same on all supported OS

[P, win, screen] = config();

%% RS add a path to io64
if isSendTrigger
    addpath('./io64/');
    config_io;
end

pp_address = 888;

%% Sanity checks
% Do dummy calls to GetSecs, WaitSecs, KbCheck to make sure
% they are loaded and ready when we need them - without delays
% in the wrong moment:
KbCheck;
WaitSecs(0.1);
GetSecs;

% Set priority for script execution to realtime priority:
priorityLevel=MaxPriority(win);
Priority(priorityLevel);

%% experiment

% Embed core of code in try ... catch statement. If anything goes wrong
% inside the 'try' block (Matlab error), the 'catch' block is executed to
% clean up, save results, close the onscreen window etc.

try
    if P.suj.session == 1
        P = session1(P,screen,path_stimsM);
    else
        P = session2(P,screen,path_stimsM); 
    end
       
    %% main eyelink sequence end
    if EL
        EyelinkEndSequence(edfName, P.suj.number, DUMMY_EYELINK);
    end
    sca;
    ShowCursor;
    fclose('all');
    Priority(0);
    Screen('CloseAll');
    IOPort('CloseAll');
    
catch
    % catch error: This is executed in case something goes wrong in the
    % 'try' part due to programming error etc.:
    
    % Do same cleanup as at the end of a regular session...
    sca;
    ShowCursor;
    fclose('all');
    Priority(0);
    
    % Output the error message that describes the error:
    psychrethrow(psychlasterror);
end % try ... catch %
