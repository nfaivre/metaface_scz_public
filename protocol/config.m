function [P, win, screen] =  config()
%addpath(genpath('/home/lpnc/Documents/MATLAB/Psychtoolbox'))
addpath functions

% set keyboard for answer keys
P.keyboard = 'french'; % 'german', 'swiss' or 'french'

%% Triggers 
% Trigger type ('none', 'serial' or 'dummy')
P.triggers = 'dummy';
% Trigger port (e.g. /dev/ttyACM0 or /dev/ttyUSB0)
P.triggerport = '/dev/ttyACM0';
% Trigger baud rate (typically 9600 for MicroMed)
P.baudrate = 115200;%9600; 
% whether to send a trigger for every frame
P.trigframe = 0;

P.design.colorMask = 200;
% P.design.preTask = 1;

%set paths
P.savepath_tmp = '../data/tmp/';
P.savepath = '../data/';
P.savebloc = '../data/blocrecover/';
if ~exist(P.savepath,'dir')
    mkdir(P.savepath);
end

%% PARADIGM

P.design.RTmax = 6; % (in seconds)

% Create and register a buzz sound for out-of-time Responses:
% pitch = 25;
% env = 300;
% buzz(pitch, env)

% Load buzz sound for out-of-time Responses:
buzz_sound = 'buzz_sound.wav';
[y, Fs] = audioread(['functions/' buzz_sound]);
P.buzz.y = y;
P.buzz.Fs = Fs;

%1) Memory
it = 3; %Number of interleaved tasks (Recollection, familiarity, discrimination)
P.design.nfaces = 4; %Set the number of displayed stimuli per trial
    %% Session 1
P.design.nMemblocs = 3;   %  minimum 3, otherwise initrial.m won't be able to compute perception signal to match memory perf
P.design.nMemtrials = 10; % 40 (should be a multiple of 10) total number of memory trials (famil + recol) per block
P.design.nStaircase = 11; %>10    number of stimuli for the staircase procedure
P.design.nPerblocs = 3;   %  3   number of consecutive blocks for detection task (session1)
P.design.nPertrials = 10; % 10*4*2 number of trials per block (10 signal amplitudes * 4 stims * 2 repetitions)
P.design.nPerTot = P.design.nPerblocs * P.design.nPertrials; % number of perception trials (multiple of 40) 

    %% Session 2
P.design.nblocs = 10;  % 10 blocks of trials 
ntrials         = 30;  % 30 trials per bloc (should be a multiple of 15)
P.design.n = floor(ntrials/it)*it; % n is a multiple of 3   

    %%
%2) Detection
P.facelist = dir('../stims/stimsP/newstim*_blur.tif');
P.contourlist = dir('../stims/stimsP/newstim*.tif');
P.nfaces = length(P.facelist);
% The number of repetition of each face. 
% This setting overrides P.design.nPertrials: set to <=0 to use P.design.nPertrials per block
% but then there's no guarantee to have the same number of trials per face

P.nfacesrep = ceil(P.design.nPerTot / 40);
% P.nfacesrep = 1;

% how to mask: 'contour' puts a black contour around everything
% 'blur' uses a gaussian blur filter only on the faces
% 'none' doesn't do any masking
P.masking = 'none'; % 'contour' or 'blur'

% '01' for no/yes, '01+' for no/one/one+
P.answertype = '01';

% intensity values for the psychometric curve (only for psychometric)
% Initial amplitude of stimulus
P.initialamp = 0.12;

% percentage of catch trials (only for staircase, not for psycho)
P.catchperc = 0;

% standard deviation in x and y for the Gaussian blur mask (when P.masking = 'blur')
P.gauss_std = [250,350]; % old: [180 300];

% Percentage of the initial amplitude of the stimulus for adaptive
% staircase
P.stepsize = 0.01;

% Screen size [pos posY width height], set to [] for full screen
P.screenrect = [];%[100 100 1500 1200]

% whether to show a circle on an edge of the screen to sync with photodiode
P.photod = 1;
P.photodpos = [50,-50,50,50];
if P.triggers == 'dummy'
    P.photod = 0;
end

%% Keyboard: 
P.index = []; % 13 use GetKeyboardIndices!!!! 
KbName('UnifyKeyNames');
PsychHID('KbQueueCreate', P.index);%keys

P.design.yesResp = 'y'; % 'yes' response via key 's'
P.design.noResp  = 'n'; % 'no' response via key 'q'
P.rightside = 1;

% allow quit
P.allowquit = 1;
P.design.quitkey = KbName('ESCAPE');
% allow pause
P.allowpause = 0;
P.design.pausekey = KbName('p');
% select first or second cell to display (instructions on screen)
P.keydispsel = 2;
if strcmp(P.keyboard,'swiss')
   P.keyno      = {'a'};
   P.keyyes     = {'s'};
elseif strcmp(P.keyboard,'french')
   P.keyno      = {'q'};
   P.keyyes     = {'s'};
elseif strcmp(P.keyboard,'german')
   P.keyno      = {'a'};
   P.keyyes     = {'s'};
else
   error(['Unknown keyboard: ' P.keyboard]); 
end

%% GET PARTICIPANT'S INFOS
dlg = inputdlg({'Group (p/c)','Subject #','Age','Sexe (h/f)'},'Input');
P.suj.group             = dlg{1};
P.suj.number            = str2num(dlg{2});
P.suj.age               = str2num(dlg{3});
P.suj.sex               = dlg{4};
P.suj.startdate         = clock; % save the date & time

% If a file already exists for this participant, then he will go through
% session2
filenames = struct2cell(dir('../data/'));  
if any(contains(filenames(1,:),['suj' num2str(P.suj.number) '_session1_all']))
    P.suj.session = 2;
else
    P.suj.session = 1;
end

% attribute std values if missing
if isempty(P.suj.group),P.suj.group='c';end
if isempty(P.suj.number), P.suj.number=99; end
if isempty(P.suj.age),    P.suj.age=99;    end
if isempty(P.suj.sex),    P.suj.sex='x';   end
if isempty(P.suj.session),P.suj.session=1; end

%perception settings
rng('default');
subnum = P.suj.number;
P.subnum = subnum;
%% PREPARE STIMS & DESIGN
screen.id               = max(Screen('Screens'));  % main screen (consider changing if we have several screens)
screen.colbackground    = 128; % color of the background
screen.refreshrate      = Screen('FrameRate', screen.id); % in Hz
screen.refreshtime      = 1000/screen.refreshrate; % in ms

% PTB screen managment
[win, screen.rect]      = Screen('OpenWindow', screen.id, screen.colbackground, [], 32,2);
HideCursor();
screen.center           = [screen.rect(3)/2 screen.rect(4)/2]; % Screen center position (Mac screen coordinates)
P.depth                 = WhiteIndex(win);
screen.ppi              = get(0,'ScreenPixelsPerInch');
screen.dotPitch         = 25.4 ./ screen.ppi; % mm per pixels
screen.viewingDistance  = 573; % viewing distance in mm
screen.res              = Screen('Resolution',win);
screen.width            = screen.res.width; % in pixel units
screen.height           = screen.res.height; % in pixel units
screen.framedur         = Screen('GetFlipInterval', win); % sec / frame

HideCursor(); % Hide the mouse cursor
% Screen('TextSize', win, 26);% Set text size
Screen('Preference', 'DefaultFontSize',26);
Screen('TextFont', win)

%% TIMING 

P.design.ISI = 0.5; % Inter-stimulus interval (in secs)
P.design.stimDuration = 0.4; % (in secs)
% frequency of new masks in Hz (noise update)
P.fmasks = 1;
% Fixed time to wait between trials
P.tITI = 0.5;
% Random time to wait between trials in secs
P.tITIrand  = 0.25;
% Fixed time to wait between stim and delayed) answer in secs
P.tdelay = 0.5;
% Random time to wait between stim and (delayed) answer in secs
P.tdelayrand  = 0.5;
% Duration of a trial (i.e. masks) in secs
P.ttrial = 0.5;
% Initial duration of a trial without stim in secs
P.tinit = 0;
% Duration after P.tinit where there could be a stim in secs
P.tstim = 0.5;
% Duration of the choice selection
P.tfb = 0.1;

%% Stimuli & design of perception task
% Query the frame duration
P.fscreen = 1/Screen('GetFlipInterval', win);

P.nframestot = round(P.ttrial*P.fscreen);
P.nframesinit = round(P.tinit*P.fscreen);
P.nframesstim = round(P.tstim*P.fscreen);
P.nframesmask = round(P.fscreen/P.fmasks);
P.nmaskstot = round(P.nframestot/P.nframesmask);
P.nmasksinit = round(P.nframesinit/P.nframesmask);
P.nmasksstim = round(P.nframesstim/P.nframesmask);

% Get the centre coordinate of the window
[P.midX, P.midY] = RectCenter(screen.rect);

% Fixation cross in px
P.linewidth = 2;
P.fixcrossdim = 5;

xCoords = [-P.fixcrossdim P.fixcrossdim 0 0];
yCoords = [0 0 -P.fixcrossdim P.fixcrossdim];
P.allCoords = [xCoords; yCoords];

% Set up alpha-blending for smooth (anti-a liased) lines
Screen('BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
fprintf('Starting with %d frames total (init:%d, stim:%d, mask:%d\n',P.nframestot,P.nframesinit,P.nframesstim,P.nframesmask);
fprintf('Total: %d masks, init:%d, stim:%d\n',P.nmaskstot,P.nmasksinit,P.nmasksstim);

k = ceil(screen.rect(4)/5); % 20
x = ceil(screen.rect(4)/15);
% P.design.clickRange = [P.midX-x, P.midY-x,
%                        P.midX+x, P.midY+x];
% P.design.clickRangeNo = [0.25*P.midX - k, P.design.Ymin,
%                          0.25*P.midX + k, P.design.Ymax];
% P.design.clickRangeYes= [1.75*P.midX - k, P.design.Ymin,
%                          1.75*P.midX + k, P.design.Ymax];
P.design.clickReset = CenterRectOnPoint([0 0 x x],P.midX,P.midY);
respNo = CenterRectOnPoint( [0 0 k k], 0.25*P.midX, 0.5*P.midY);
respYes = CenterRectOnPoint([0 0 k k], 1.75*P.midX, 0.5*P.midY);
P.design.clickRange = [respNo; respYes].';
P.design.Ymin = 0.5*P.midY-k/2; %50
P.design.Ymax = 0.5*P.midY+k/2; %40
P.design.XtextNo  = 0.22*P.midX;
P.design.XtextYes = 1.72*P.midX;
P.design.Ytext    = 0.52*P.midY;
% Create rectangle for on-line trials :
P.design.baseRect = [0 0 0.6*screen.height  0.6*screen.height]; % Make a base Rect
P.design.centeredRect = CenterRectOnPoint(P.design.baseRect, P.midX, P.midY);
P.design.rectcolor = 200;

%Result display
P.design.bigRect1 = CenterRectOnPoint([0 0 0.5*screen.width 0.5*screen.height], P.midX, P.midY)';
P.design.bigRect2 = CenterRectOnPoint([0 0 0.5*screen.width 0.4*screen.height], P.midX, P.midY)';
P.design.bigRect3 = CenterRectOnPoint([0 0 0.5*screen.width 0.6*screen.height], P.midX, P.midY)';

%% Trigger values
P.TRIGGER_FRAME     = 1;
P.TRIGGER_BLOCK     = 2;
P.TRIGGER_TRIALSTART= 4;
P.TRIGGER_TRIALEND  = 5;
P.TRIGGER_STIMSTART = 8;
P.TRIGGER_STIMEND   = 9;
P.TRIGGER_RESPSTART = 16;
P.TRIGGER_NO        = 32;
P.TRIGGER_YES       = 33;
P.TRIGGER_YESPLUS   = 34;
P.TRIGGER_CONF      = 64;
P.TRIGGER_PAUSE     = 128;

%Memory trials
% trigstartabs = 11; % experiment start
% trigstart   = 101; % trial onset: goes from 101 to 101 + P.design.trialperblock (101:180)
% trigpair    = 201;
% trigQ1      = 12; % stim onset
% trigM1      = 13; % M1 mouse first move
% trigR1      = 14; % R1 provided
% trigQ2      = 15; % Q2 onset
% trigM2      = 16; % M2 mouse first move during VAS
% trigR2      = 17; % R2 provided
