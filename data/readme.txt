columns in data.csv:

place: place of testing, "Versailles" or "Grenoble"
suj : ID of subject in the form "gXX_p", where: 
      g is the group: "p" for patient, "c" for healthy control, 
      XX is the subject number,  
      p is the place of testing: "v" for Versailles, "g" for Grenoble
group : group factor, "p" for patient, "c" for healthy control
task  : task factor with 3 modalities: "Perception", "Familiarity", "Recollection"
trial : trial number for each participant, from 1 to 300.
stimamp : Numerical variable. Stimulus contrast for visual detection trials. 
bloc : bloc number, from 1 to 10.
resp : type 1 response, "y" for yes, "n" for no, "none" for no response given (due to time limit of 6 sec)
correctAns : type 1 correct response : y" for yes, "n" for no,
ac   : accuracy. 1 for correct, 0 for Incorrect
SDT  : SDT categories depending on resp and correctAns: "hit", "miss", "fa", "crej" or "none" when resp == "none"
rt   : type 1 response times, in sec. Bounded, from 0 to 6 sec.
confidence : continuous confidence from a full confidence scale from 0 to 1, 
             where 0 means "sure incorrect",
                   0.5 means "unsure",
                   1 means "sure correct"
sigLevel : Signal level factor, 5 modalities from 0 (catch trials) to 4. 1 is the most difficult level, 4 is the easiest.
           The meaning of sigLevel depends on the task.
           In the memory trials (familiarity and recollection), sigLevel increases as the lag between the target and the test item decreases. 
           In perceptual trials, sigLevel corresponds to stimulus amplitudes mapped from 1 to 4 to match with memory performance.
z_conf : z-score(confidence) for each individual
sig : centered variable sigLevel (> 0), values -1.5, -0.5, +0.5, +1.5 
binconf : quantiles of confidence (6 bins)
logrt: logtransformed response times
binconf2: z_conf splitted in 7 categories
acc : trial accuracy -> 0 or 1
predb : predictions of accuracy of bayesian mixed-effects model
