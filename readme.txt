Rouy et al., 2023. Confidence in visual detection, familiarity and recollection judgements is preserved in schizophrenia spectrum disorder.

Preregistration, scripts, and materials:
https://osf.io/vjrke
https://gitlab.com/nfaivre/metaface_scz_public

